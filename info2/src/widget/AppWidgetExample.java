package widget;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import getinfoPage.Activity_about;

import com.example.hellochart.R;
import com.example.informationPage.Activity_MealsPage;
import com.loopj.android.http.JsonHttpResponseHandler;

import API_Funtion.ClassAPI;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.sax.StartElementListener;
import android.util.Log;
import android.widget.RemoteViews;

public class AppWidgetExample extends AppWidgetProvider
{
	/** Called when the activity is first created. */
	public static final String ACTION_TEXT_CHANGED = "yourpackage.TEXT_CHANGED";
	int food_cal;
	int sport_cal;
	int MaxValue=0;
	ClassAPI API_Funtion = new ClassAPI(null);
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
	{
		final int IDs = appWidgetIds.length;

		for (int i = 0; i < IDs; i++)
		{
			int appWidgetId = appWidgetIds[i];
			updateAppWidget(context, appWidgetManager, appWidgetId);
		}
	}
	

	@Override
	public void onReceive(Context context, Intent intent)
	{
		super.onReceive(context, intent);
		if (intent.getAction().equals(ACTION_TEXT_CHANGED)) {
	        // handle intent here
	        food_cal = intent.getIntExtra("foodcal", 0);
	       sport_cal = intent.getIntExtra("sportcal", 0);
	        MaxValue = intent.getIntExtra("max", 0);
	        Log.e("-------------", ""+MaxValue);
	        //Log.e("**************", ""+MaxValue);
	        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context.getApplicationContext());
	        ComponentName thisWidget = new ComponentName(context.getApplicationContext(), AppWidgetExample.class);
	        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
	        if (appWidgetIds != null && appWidgetIds.length > 0) {
	            onUpdate(context, appWidgetManager, appWidgetIds);
	        }
	    }
	}


	private void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId)
	{
		// TODO Auto-generated method stub
		Intent intent = new Intent(context, Activity_about.class);
	    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
		
		RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget);
		views.setTextViewText(R.id.textView_food_cal, ""+food_cal);
		views.setTextViewText(R.id.textView_sport_cal, ""+sport_cal);
		views.setProgressBar(R.id.progressBar_food, MaxValue, food_cal, false);
		views.setProgressBar(R.id.progressBar_sport, MaxValue, sport_cal, false);
		views.setOnClickPendingIntent(R.id.layout1, pendingIntent);
		//views.setTextViewText(R.id.HelloTextView01, text);

		appWidgetManager.updateAppWidget(appWidgetId, views);

	}
}
