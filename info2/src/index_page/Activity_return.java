package index_page;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.calendarPage.Activity_CalendarPage;
import com.example.hellochart.R;
import com.example.informationPage.MainActivity;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.navdrawer.SimpleSideDrawer;

import API_Funtion.ClassAPI;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
@SuppressLint("NewApi")
public class Activity_return extends Activity
{
	private SimpleSideDrawer mNav;
	String meals_null_breakfast, meals_null_lunch, meals_null_dinner;
	private TextView mTextView_last_sport;
	private ImageView mImageView_mission;
	private ImageView mImageView_breakfast;
	private ImageView mImageView_lunch;
	private ImageView mImageView_dinner;
	int mission_flag = 0;
	int foodflag = 0;
	private String[] drawer_menu;
	public TypedArray drawer_icon;
	private ListView mListView_Left_Layout;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_index_page);
		ClassAPI API_Funtion_1 = new ClassAPI(Activity_return.this);
		mImageView_breakfast = (ImageView) findViewById(R.id.imageView_gugu);
		mImageView_lunch = (ImageView) findViewById(R.id.imageView_sun);
		mImageView_dinner = (ImageView) findViewById(R.id.imageView_moon);
		mTextView_last_sport = (TextView) findViewById(R.id.textView_last_sport);
		mImageView_mission = (ImageView) findViewById(R.id.imageView_mission);
		mImageView_breakfast.setImageAlpha(20);
		mImageView_lunch.setImageAlpha(20);
		mImageView_dinner.setImageAlpha(20);
		drawer_menu = this.getResources().getStringArray(R.array.drawer_menu);
		drawer_icon = this.getResources().obtainTypedArray(R.array.drawer_icons);
		initActionBar();
		initDrawer();
		try
		{
			API_Funtion_1.mission_show(88122841, gettoken(), API_Funtion_1.GetTimestamp(), new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					try
					{
						for(int i =0;i<5;i++)
						{
							if (response.getJSONArray("task").getJSONObject(i).getInt("data") == 1)
							{
								mission_flag++;
							}
						}
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (mission_flag == 1)
					{
						mImageView_mission.setImageResource(R.drawable.mission1);
					}
					else if (mission_flag == 2)
					{
						mImageView_mission.setImageResource(R.drawable.mission2);
					}
					else if (mission_flag == 3)
					{
						mImageView_mission.setImageResource(R.drawable.mission3);
					}
					else if (mission_flag == 4)
					{
						mImageView_mission.setImageResource(R.drawable.mission4);
					}
					else if (mission_flag == 5)
					{
						mImageView_mission.setImageResource(R.drawable.mission5);
					}
					super.onSuccess(response);
				}
			});
			//-------------------------------//
			API_Funtion_1.getlastsport(88122841, gettoken(), new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					try
					{
						mTextView_last_sport.setText(response.getString("msg"));
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					super.onSuccess(response);
				}
			});
			// ----------------------------------------//
			API_Funtion_1.food_show(88122841, gettoken(), API_Funtion_1.GetTimestamp(), new JsonHttpResponseHandler()
			{
				@SuppressLint("NewApi")
				public void onSuccess(JSONObject response)
				{
					Log.e("api1", "ok");
					try
					{
						// Log.e("*************", "qq1");
						meals_null_breakfast = response.get("breakfast").toString();
						meals_null_lunch = response.get("lunch").toString();
						meals_null_dinner = response.get("dinner").toString();
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (!meals_null_breakfast.equals("null"))
					{
						// Log.e("*************", "qq");
						foodflag++;
						mImageView_breakfast.setImageAlpha(255);
					}
					if (!meals_null_lunch.equals("null"))
					{
						// mImageView_lunch.setImageResource(drawable.checkbox_on_background);
						foodflag++;
						mImageView_lunch.setImageAlpha(255);
					}
					if (!meals_null_dinner.equals("null"))
					{
						// mImageView_dinner.setImageResource(drawable.checkbox_on_background);
						foodflag++;
						mImageView_dinner.setImageAlpha(255);
					}
					super.onSuccess(response);
				}

				@Override
				public void onFailure(Throwable e, JSONObject errorResponse)
				{
					Log.e("api1", "error");
					super.onFailure(e, errorResponse);
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable e)
				{
					Log.e("1", ""+statusCode);
					Log.e("2", ""+headers.toString());
					Log.e("3", ""+responseBody);
					Log.e("4", ""+e.toString());
					super.onFailure(statusCode, headers, responseBody, e);
				}
				
			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void initActionBar()
	{
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#ef3a5f8a"));
		ActionBar bar = getActionBar();
		bar.setHomeButtonEnabled(true);
		bar.setIcon(R.drawable.ic_drawer);
		bar.setBackgroundDrawable(actionbar_color);
	}
	
	private void initDrawer()
	{
		ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(Activity_return.this, R.layout.menu_list, R.id.txtItem1, drawer_menu)
		{
			@Override
			public View getView(int position, View convertView, ViewGroup parent)
			{
				LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = mInflater.inflate(R.layout.menu_list, null);

				ImageView icon = (ImageView) convertView.findViewById(R.id.img_icon);
				icon.setImageDrawable(drawer_icon.getDrawable(position));
				return super.getView(position, convertView, parent);
			}
		};
		mNav = new SimpleSideDrawer(this);
		mNav.setLeftBehindContentView(R.layout.activity_behind_left_simple);
		mListView_Left_Layout = (ListView) mNav.findViewById(R.id.listView1);
		mListView_Left_Layout.setAdapter(adapter1);
		mListView_Left_Layout.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
			{
				if(arg2 == 0)
				{
					Intent intent = new Intent();
					intent.setClass(Activity_return.this, Activity_CalendarPage.class);
					startActivity(intent);
				}
			}
		});
	}
	private String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = Activity_return.this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}
	
}
