package index_page;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import mission.Activity_Mission;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import sportPage.Sportpage;

import API_Funtion.ClassAPI;
import android.R.drawable;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.System;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.calendarPage.Activity_CalendarPage;
import com.example.hellochart.R;
import com.example.informationPage.Activity_MealsPage;
import com.example.informationPage.Activity_InformationPage;
import com.example.informationPage.MainActivity;
import com.loopj.android.http.JsonHttpResponseHandler;

import delay_enjoy.Activity_delay_enjoy;

@SuppressLint("NewApi")
public class Fragment_index_page extends Fragment
{
	String meals_null_breakfast, meals_null_lunch, meals_null_dinner;
	private Button mButton_mychart;
	private Button mButton_diary_food;
	private Button mButton_week_weight;
	private Button mButton_delay_enjoy;
	private Button mButton_diary_sport;
	private Button mButton_mission;
	private Button mButton_do_action;
	private ImageView mImageView_breakfast;
	private ImageView mImageView_lunch;
	private ImageView mImageView_dinner;
	private ImageView mButton_Dialog_cancel;
	private Button mButton_Dialog_addweight_enter;
	private EditText mEditText_Dialog_weight;
	private EditText mEditText_Dialog_waistline;
	private EditText mEditText_Dialog_height;
	private ClassAPI API_Funtion = new ClassAPI(getActivity());
	private TextView mTextView_last_sport;
	private ImageView mImageView_mission;
	int mission_flag = 0;
	int foodflag = 0;

	
	
	@Override
	public void onAttach(Activity activity)
	{
		food_show();
		lastsport();
		mission_image();
		super.onAttach(activity);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.activity_index_page, container, false);
		mButton_mychart = (Button) view.findViewById(R.id.button_myweight);
		mButton_diary_food = (Button) view.findViewById(R.id.btn_select_lunch);
		mButton_week_weight = (Button) view.findViewById(R.id.button3);
		mButton_delay_enjoy = (Button) view.findViewById(R.id.btn_select_other);
		mButton_diary_sport = (Button) view.findViewById(R.id.button5);
		mButton_mission = (Button) view.findViewById(R.id.button6);
		mButton_do_action = (Button) view.findViewById(R.id.button7);
		mImageView_breakfast = (ImageView) view.findViewById(R.id.imageView_gugu);
		mImageView_lunch = (ImageView) view.findViewById(R.id.imageView_sun);
		mImageView_dinner = (ImageView) view.findViewById(R.id.imageView_moon);
		mTextView_last_sport = (TextView) view.findViewById(R.id.textView_last_sport);
		mImageView_mission = (ImageView) view.findViewById(R.id.imageView_mission);
		// Log.e("*************", ""+gettoken());
		// Log.e("*************", ""+API_Funtion.GetTimestamp());
		mImageView_breakfast.setImageAlpha(20);
		mImageView_lunch.setImageAlpha(20);
		mImageView_dinner.setImageAlpha(20);
		Log.e("***************", "onCreateView");
		
		//API_Funtion = new ClassAPI(getActivity());
		
		onclick();
		//launchRingDialog(null);
		return view;
		//return initView(inflater, container);
	}

	

	@Override
	public void onResume()
	{
		Log.e("***************", "onResume");
		super.onResume();
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@SuppressLint("NewApi")
	private View initView(LayoutInflater inflater, ViewGroup container)
	{
		View view = inflater.inflate(R.layout.activity_index_page, container, false);
		mButton_mychart = (Button) view.findViewById(R.id.button_myweight);
		mButton_diary_food = (Button) view.findViewById(R.id.btn_select_lunch);
		mButton_week_weight = (Button) view.findViewById(R.id.button3);
		mButton_delay_enjoy = (Button) view.findViewById(R.id.btn_select_other);
		mButton_diary_sport = (Button) view.findViewById(R.id.button5);
		mButton_mission = (Button) view.findViewById(R.id.button6);
		mButton_do_action = (Button) view.findViewById(R.id.button7);
		mImageView_breakfast = (ImageView) view.findViewById(R.id.imageView_gugu);
		mImageView_lunch = (ImageView) view.findViewById(R.id.imageView_sun);
		mImageView_dinner = (ImageView) view.findViewById(R.id.imageView_moon);
		mTextView_last_sport = (TextView) view.findViewById(R.id.textView_last_sport);
		mImageView_mission = (ImageView) view.findViewById(R.id.imageView_mission);
		// Log.e("*************", ""+gettoken());
		// Log.e("*************", ""+API_Funtion.GetTimestamp());
		mImageView_breakfast.setImageAlpha(20);
		mImageView_lunch.setImageAlpha(20);
		mImageView_dinner.setImageAlpha(20);
		
		API_Funtion = new ClassAPI(getActivity());
		onclick();
		//launchRingDialog(null);
		return view;
	}
	
	

	private void onclick()
	{
		mButton_mychart.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent();
				intent.setClass(getActivity(), Activity_InformationPage.class);
				startActivity(intent);
				//getActivity().finish();
			}
		});

		mButton_diary_food.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent();
				intent.setClass(getActivity(), Activity_MealsPage.class);
				startActivity(intent);
				getActivity().finish();
			}
		});

		if (getweekday().equals("Wednesday"))
		{
			mButton_week_weight.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					// TODO Auto-generated method stub
					final Dialog dialog = new Dialog(getActivity(), R.style.dialog);
					View myView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_add_week_weight, null);
					dialog.setContentView(myView);
					mButton_Dialog_cancel = (ImageView) myView.findViewById(R.id.dialog_cancle);
					mButton_Dialog_addweight_enter = (Button) myView.findViewById(R.id.dialog_add);
					mEditText_Dialog_weight = (EditText) myView.findViewById(R.id.editText_user_weight);
					mEditText_Dialog_height = (EditText) myView.findViewById(R.id.EditText_height_1);
					mEditText_Dialog_waistline = (EditText) myView.findViewById(R.id.EditText_waistline_1);

					try
					{
						API_Funtion.weight_day_show(88122841, gettoken(), new JsonHttpResponseHandler()
						{
							@Override
							public void onSuccess(JSONObject response)
							{
								try
								{
									mEditText_Dialog_waistline.setText(response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 1).getString("waistline"));
									mEditText_Dialog_height.setText(response.getString("user_height_last"));
								}
								catch (JSONException e1)
								{
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}

						});
					}
					catch (UnsupportedEncodingException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					mButton_Dialog_cancel.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							dialog.cancel();

						}
					});

					mButton_Dialog_addweight_enter.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							try
							{
								if (mEditText_Dialog_weight.getText().toString() == null || Double.valueOf(mEditText_Dialog_weight.getText().toString()) == 0 || mEditText_Dialog_waistline.getText().toString() == null || Double.valueOf(mEditText_Dialog_waistline.getText().toString()) == 0 || mEditText_Dialog_height.getText().toString() == null || Double.valueOf(mEditText_Dialog_height.getText().toString()) == 0)
								{
									Toast.makeText(getActivity(), "請輸入合理資訊!", Toast.LENGTH_SHORT).show();
								}
								else
								{
									try
									{
										API_Funtion.weight_day(88122841, gettoken(), Double.valueOf(mEditText_Dialog_weight.getText().toString()), Double.valueOf(mEditText_Dialog_waistline.getText().toString()), Double.valueOf(mEditText_Dialog_height.getText().toString()), new JsonHttpResponseHandler()
										{

											@Override
											public void onSuccess(JSONObject response)
											{
												Toast.makeText(getActivity(), "輸入成功!", Toast.LENGTH_LONG).show();
												// Fragment fragment = new
												// Fragment_InformationPage();
												// android.app.FragmentManager
												// fragmentManager =
												// getFragmentManager();
												// fragmentManager.beginTransaction().replace(R.id.content_frame,
												// fragment).commit();
												super.onSuccess(response);
											}

										});
									}
									catch (UnsupportedEncodingException e)
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									catch (JSONException e)
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									dialog.cancel();
								}
							}
							catch (Exception e)
							{
								Toast.makeText(getActivity(), "請輸入完整資訊!", Toast.LENGTH_SHORT).show();
							}

						}
					});

					dialog.show();
				}
			});
		}
		else
		{
			mButton_week_weight.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					Toast.makeText(getActivity(), "請於週六輸入資料!", Toast.LENGTH_LONG).show();

				}
			});
		}

		mButton_delay_enjoy.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(getActivity(), Activity_delay_enjoy.class);
				startActivity(intent);
				getActivity().finish();
			}
		});

		mButton_diary_sport.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(getActivity(), Sportpage.class);
				startActivity(intent);
				getActivity().finish();
			}
		});

		mButton_mission.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(getActivity(), Activity_Mission.class);
				Bundle bundle = new Bundle();
				bundle.putInt("foodflag", foodflag);
				intent.putExtras(bundle);
				startActivity(intent);
				getActivity().finish();
			}
		});

		mButton_do_action.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub

			}
		});
	}

	private String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = getActivity().openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}

	private String getweekday()
	{
		String weekDay;
		SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);

		Calendar calendar = Calendar.getInstance();
		weekDay = dayFormat.format(calendar.getTime());
		return weekDay;
	}

	private void food_show()
	{
		try
		{
			// ----------------------------------------//
			API_Funtion.food_show(88122841, gettoken(), API_Funtion.GetTimestamp(), new JsonHttpResponseHandler()
			{
				@SuppressLint("NewApi")
				@Override
				public void onSuccess(JSONObject response)
				{
					Log.e("api1", "ok");
					try
					{
						// Log.e("*************", "qq1");
						meals_null_breakfast = response.get("breakfast").toString();
						meals_null_lunch = response.get("lunch").toString();
						meals_null_dinner = response.get("dinner").toString();
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (!meals_null_breakfast.equals("null"))
					{
						// Log.e("*************", "qq");
						foodflag++;
						mImageView_breakfast.setImageAlpha(255);
					}
					if (!meals_null_lunch.equals("null"))
					{
						// mImageView_lunch.setImageResource(drawable.checkbox_on_background);
						foodflag++;
						mImageView_lunch.setImageAlpha(255);
					}
					if (!meals_null_dinner.equals("null"))
					{
						// mImageView_dinner.setImageResource(drawable.checkbox_on_background);
						foodflag++;
						mImageView_dinner.setImageAlpha(255);
					}
					super.onSuccess(response);
				}

				@Override
				public void onFailure(Throwable e, JSONObject errorResponse)
				{
					Log.e("api1", "error");
					super.onFailure(e, errorResponse);
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable e)
				{
					Log.e("1", ""+statusCode);
					Log.e("2", ""+headers.toString());
					Log.e("3", ""+responseBody);
					Log.e("4", ""+e.toString());
					super.onFailure(statusCode, headers, responseBody, e);
				}
				
			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void lastsport()
	{
		try
		{
			API_Funtion.getlastsport(88122841, gettoken(), new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					try
					{
						mTextView_last_sport.setText(response.getString("msg"));
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					super.onSuccess(response);
				}
			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.e("api2", "ok");
	}

	private void mission_image()
	{
		try
		{
			API_Funtion.mission_show(88122841, gettoken(), API_Funtion.GetTimestamp(), new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					try
					{
						if (response.getJSONArray("task").getJSONObject(0).getInt("data") == 1)
						{
							mission_flag++;
							Log.e("tag", "" + mission_flag);
						}
						if (response.getJSONArray("task").getJSONObject(1).getInt("data") == 1)
						{
							mission_flag++;
							Log.e("tag", "" + mission_flag);
						}
						if (response.getJSONArray("task").getJSONObject(2).getInt("data") == 1)
						{
							mission_flag++;
							Log.e("tag", "" + mission_flag);
						}
						if (response.getJSONArray("task").getJSONObject(3).getInt("data") == 1)
						{
							mission_flag++;
							Log.e("tag", "" + mission_flag);
						}
						if (response.getJSONArray("task").getJSONObject(4).getInt("data") == 1)
						{
							mission_flag++;
							Log.e("tag", "" + mission_flag);
						}

						if (mission_flag == 1)
						{
							mImageView_mission.setImageResource(R.drawable.mission1);
						}
						else if (mission_flag == 2)
						{
							mImageView_mission.setImageResource(R.drawable.mission2);
						}
						else if (mission_flag == 3)
						{
							mImageView_mission.setImageResource(R.drawable.mission3);
						}
						else if (mission_flag == 4)
						{
							mImageView_mission.setImageResource(R.drawable.mission4);
						}
						else if (mission_flag == 5)
						{
							mImageView_mission.setImageResource(R.drawable.mission5);
						}
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					super.onSuccess(response);
				}
			});
			Log.e("api3", "ok");
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void launchRingDialog(View view)
	{
		final ProgressDialog ringProgressDialog = ProgressDialog.show(getActivity(), "Please wait ...", "Data Loading ...", true);
		ringProgressDialog.setCancelable(true);
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					Thread.sleep(2000);
				}
				catch (Exception e)
				{

				}
				ringProgressDialog.dismiss();
			}
		}).start();
	}
}
