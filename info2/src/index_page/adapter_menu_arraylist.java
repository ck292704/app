package index_page;

import com.example.hellochart.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class adapter_menu_arraylist extends ArrayAdapter<String>
{
	private ImageView mImageView_icon;
	public TypedArray drawer_icon;
	private Context index_context;

	public adapter_menu_arraylist(Context context, int resource, int textViewResourceId, String[] objects)
	{
		super(context, resource, textViewResourceId, objects);
		index_context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater mInflater = (LayoutInflater) index_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = mInflater.inflate(R.layout.menu_list, null);
		drawer_icon = index_context.getResources().obtainTypedArray(R.array.drawer_icons);
		mImageView_icon = (ImageView) convertView.findViewById(R.id.img_icon);
		mImageView_icon.setImageDrawable(drawer_icon.getDrawable(position));
		return super.getView(position, convertView, parent);
	}

}
