package index_page;

import getinfoPage.Activity_about;
import getinfoPage.LoginPage;
import healthytool.bmi_counter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import mission.Activity_Mission;
import news_page.Activity_news_page;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import sportPage.Sportpage;
import widget.AppWidgetExample;

import com.example.calendarPage.Activity_CalendarPage;
import com.example.hellochart.R;
import com.example.informationPage.Activity_InformationPage;
import com.example.informationPage.Activity_MealsPage;
import com.example.informationPage.MainActivity;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.navdrawer.SimpleSideDrawer;

import delay_enjoy.Activity_DelayEnjoy_v2;
import delay_enjoy.Activity_delay_enjoy;
import delay_enjoy.Activity_game_number;

import API_Funtion.ClassAPI;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class Activity_index_page extends Activity
{
	private SimpleSideDrawer mNav;
	ClassAPI API_Funtion = new ClassAPI(Activity_index_page.this);
	private String[] drawer_menu;
	public TypedArray drawer_icon;
	private ListView mListView_Left_Layout;
	int foodcal,sportcal,max;
	String meals_null_breakfast, meals_null_lunch, meals_null_dinner;
	private Button mButton_mychart;
	private Button mButton_diary_food;
	private Button mButton_week_weight;
	private Button mButton_delay_enjoy;
	private Button mButton_diary_sport;
	private Button mButton_mission;
	private Button mButton_news;
	private ImageView mImageView_breakfast;
	private ImageView mImageView_lunch;
	private ImageView mImageView_dinner;
	private ImageView mButton_Dialog_cancel;
	private ImageView mButton_Dialog_cancel_aboutus;
	private Button mButton_Dialog_addweight_enter;
	private EditText mEditText_Dialog_weight;
	private EditText mEditText_Dialog_waistline;
	private EditText mEditText_Dialog_height;
	private TextView mTextView_last_sport;
	private ImageView mImageView_mission;
	int mission_flag = 0;
	int foodflag = 0;
	Intent intent = new Intent(AppWidgetExample.ACTION_TEXT_CHANGED);

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_index_page);
		//launchRingDialog(null);
		drawer_menu = this.getResources().getStringArray(R.array.drawer_menu);
		drawer_icon = this.getResources().obtainTypedArray(R.array.drawer_icons);
		Log.e("token", gettoken());
		init();
		initActionBar();
		initDrawer();
		// Toast.makeText(Activity_index_page.this, ""+tmp_1,
		// Toast.LENGTH_LONG).show();
		// mission_image();
		food_show();
		// mission_image();
		// lastsport();
		onclick();
	}

	@Override
	protected void onResume()
	{
		// initDrawer();
		super.onResume();
	}

	@Override
	protected void onPause()
	{
		// initDrawer();
		super.onPause();
	}

	@Override
	protected void onStart()
	{
		// initDrawer();
		super.onStart();
	}

	private void init()
	{
		mButton_mychart = (Button) findViewById(R.id.button_myweight);
		mButton_diary_food = (Button) findViewById(R.id.btn_select_lunch);
		mButton_week_weight = (Button) findViewById(R.id.button3);
		mButton_delay_enjoy = (Button) findViewById(R.id.btn_select_other);
		mButton_diary_sport = (Button) findViewById(R.id.button5);
		mButton_mission = (Button) findViewById(R.id.button6);
		mButton_news = (Button) findViewById(R.id.button7);
		mImageView_breakfast = (ImageView) findViewById(R.id.imageView_gugu);
		mImageView_lunch = (ImageView) findViewById(R.id.imageView_sun);
		mImageView_dinner = (ImageView) findViewById(R.id.imageView_moon);
		mTextView_last_sport = (TextView) findViewById(R.id.textView_last_sport);
		mImageView_mission = (ImageView) findViewById(R.id.imageView_mission);
		mImageView_breakfast.setImageAlpha(20);
		mImageView_lunch.setImageAlpha(20);
		mImageView_dinner.setImageAlpha(20);
		
		
		// mission_image();
		// food_show();
		// lastsport();
		// food_show();
		// lastsport();
		// mission_image();
	}

	@SuppressLint("NewApi")
	private void initActionBar()
	{
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#ef3a5f8a"));
		ActionBar bar = getActionBar();
		bar.setHomeButtonEnabled(true);
		bar.setIcon(R.drawable.ic_drawer);
		bar.setBackgroundDrawable(actionbar_color);
	}

	private void initDrawer()
	{
		final LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		// ArrayAdapter<String> adapter1 = new
		// ArrayAdapter<String>(Activity_index_page.this, R.layout.menu_list,
		// R.id.txtItem1, drawer_menu)
		// {
		// @Override
		// public View getView(int position, View convertView, ViewGroup parent)
		// {
		// convertView = mInflater.inflate(R.layout.menu_list, null);
		//
		// ImageView icon = (ImageView) convertView.findViewById(R.id.img_icon);
		// icon.setImageDrawable(drawer_icon.getDrawable(position));
		// return super.getView(position, convertView, parent);
		// }
		// };
		adapter_menu_arraylist adapter1 = new adapter_menu_arraylist(Activity_index_page.this, R.layout.menu_list, R.id.txtItem1, drawer_menu);
		mNav = new SimpleSideDrawer(this);
		mNav.setLeftBehindContentView(R.layout.activity_behind_left_simple);
		mListView_Left_Layout = (ListView) mNav.findViewById(R.id.listView1);
		mListView_Left_Layout.setAdapter(adapter1);
		mListView_Left_Layout.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
			{
				if (arg2 == 0)
				{
					Intent intent = new Intent();
					intent.setClass(Activity_index_page.this, Activity_CalendarPage.class);
					startActivity(intent);
					finish();
				}
				else if (arg2 == 1)
				{
					Intent intent = new Intent();
					intent.setClass(Activity_index_page.this, bmi_counter.class);
					startActivity(intent);
					finish();
				}
				else if (arg2 == 2)
				{
					Intent intent = new Intent();
					intent.setClass(Activity_index_page.this, Activity_game_number.class);
					startActivity(intent);
					finish();
				}
				else if (arg2 == 3)
				{
					open_aboutus_dialog();
				}
				else if (arg2 == 4)
				{
					open_reset_dialog();
				}
				else if (arg2 == 5)
				{
					open_logout_dialog();
				}
			}
		});
	}

	private void onclick()
	{
		mButton_mychart.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent();
				intent.setClass(Activity_index_page.this, Activity_InformationPage.class);
				startActivity(intent);
				Activity_index_page.this.finish();
			}
		});

		mButton_diary_food.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent();
				intent.setClass(Activity_index_page.this, Activity_MealsPage.class);
				startActivity(intent);
				Activity_index_page.this.finish();
			}
		});

		if (getweekday().equals("Saturday"))
		//if (1!=0)
		{
			mButton_week_weight.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					// TODO Auto-generated method stub
					final Dialog dialog = new Dialog(Activity_index_page.this, R.style.dialog);
					View myView = LayoutInflater.from(Activity_index_page.this).inflate(R.layout.dialog_add_week_weight, null);
					dialog.setContentView(myView);
					mButton_Dialog_cancel = (ImageView) myView.findViewById(R.id.dialog_cancle);
					mButton_Dialog_addweight_enter = (Button) myView.findViewById(R.id.dialog_add);
					mEditText_Dialog_weight = (EditText) myView.findViewById(R.id.editText_user_weight);
					mEditText_Dialog_height = (EditText) myView.findViewById(R.id.EditText_height_1);
					mEditText_Dialog_waistline = (EditText) myView.findViewById(R.id.EditText_waistline_1);

					try
					{
						API_Funtion.weight_day_show(88122841, gettoken(), new JsonHttpResponseHandler()
						{
							@Override
							public void onSuccess(JSONObject response)
							{
								try
								{
									mEditText_Dialog_weight.setText(response.getString("user_weight_first"));
									mEditText_Dialog_waistline.setText(response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 1).getString("waistline"));
									mEditText_Dialog_height.setText(response.getString("user_height_last"));
								}
								catch (JSONException e1)
								{
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}

						});
					}
					catch (UnsupportedEncodingException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					mButton_Dialog_cancel.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							dialog.cancel();

						}
					});

					mButton_Dialog_addweight_enter.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							try
							{
								if (mEditText_Dialog_weight.getText().toString() == null || Double.valueOf(mEditText_Dialog_weight.getText().toString()) == 0 || mEditText_Dialog_waistline.getText().toString() == null || mEditText_Dialog_height.getText().toString() == null || Double.valueOf(mEditText_Dialog_height.getText().toString()) == 0)
								{
									Toast.makeText(Activity_index_page.this, "請輸入合理資訊!", Toast.LENGTH_SHORT).show();
								}
								else
								{
									try
									{
										API_Funtion.weight_day(88122841, gettoken(), Double.valueOf(mEditText_Dialog_weight.getText().toString()), Double.valueOf(mEditText_Dialog_waistline.getText().toString()), Double.valueOf(mEditText_Dialog_height.getText().toString()), new JsonHttpResponseHandler()
										{

											@Override
											public void onSuccess(JSONObject response)
											{
												Toast.makeText(Activity_index_page.this, "輸入成功!", Toast.LENGTH_LONG).show();
												// Fragment fragment = new
												// Fragment_InformationPage();
												// android.app.FragmentManager
												// fragmentManager =
												// getFragmentManager();
												// fragmentManager.beginTransaction().replace(R.id.content_frame,
												// fragment).commit();
												super.onSuccess(response);
											}

										});
									}
									catch (UnsupportedEncodingException e)
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									catch (JSONException e)
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									dialog.cancel();
								}
							}
							catch (Exception e)
							{
								Toast.makeText(Activity_index_page.this, "請輸入完整資訊!", Toast.LENGTH_SHORT).show();
							}

						}
					});

					dialog.show();
				}
			});
		}
		else
		{
			mButton_week_weight.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					Toast.makeText(Activity_index_page.this, "請於週六輸入資料!", Toast.LENGTH_LONG).show();

				}
			});
		}

		mButton_delay_enjoy.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(Activity_index_page.this, Activity_DelayEnjoy_v2.class);
				startActivity(intent);
				Activity_index_page.this.finish();
			}
		});

		mButton_diary_sport.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(Activity_index_page.this, Sportpage.class);
				startActivity(intent);
				Activity_index_page.this.finish();
			}
		});

		mButton_mission.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(Activity_index_page.this, Activity_Mission.class);
				Bundle bundle = new Bundle();
				bundle.putInt("foodflag", foodflag);
				intent.putExtras(bundle);
				startActivity(intent);
				Activity_index_page.this.finish();
			}
		});

		mButton_news.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent();
				intent.setClass(Activity_index_page.this, Activity_news_page.class);
				startActivity(intent);
				Activity_index_page.this.finish();
			}
		});
	}

	private void food_show()
	{
		ClassAPI API_Funtion_1 = new ClassAPI(Activity_index_page.this);
		try
		{
			final ProgressDialog ringProgressDialog = ProgressDialog.show(Activity_index_page.this, "Please wait ...", "Data Loading ...", true);
			ringProgressDialog.setCancelable(true);
			API_Funtion_1.food_show(88122841, gettoken(), API_Funtion_1.GetTimestamp(), new JsonHttpResponseHandler()
			{
				@SuppressLint("NewApi")
				public void onSuccess(JSONObject response)
				{
					Log.e("api1", "ok");
					try
					{
						// Log.e("*************", "qq1");
						foodcal = response.getInt("foodcal_all");
						meals_null_breakfast = response.get("breakfast").toString();
						meals_null_lunch = response.get("lunch").toString();
						meals_null_dinner = response.get("dinner").toString();
						max = response.getInt("user_cal");
						intent.putExtra("max", max);
						intent.putExtra("foodcal", foodcal);
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (!meals_null_breakfast.equals("null"))
					{
						// Log.e("*************", "qq");
						foodflag++;
						mImageView_breakfast.setImageAlpha(255);
					}
					if (!meals_null_lunch.equals("null"))
					{
						// mImageView_lunch.setImageResource(drawable.checkbox_on_background);
						foodflag++;
						mImageView_lunch.setImageAlpha(255);
					}
					if (!meals_null_dinner.equals("null"))
					{
						// mImageView_dinner.setImageResource(drawable.checkbox_on_background);
						foodflag++;
						mImageView_dinner.setImageAlpha(255);
					}
					if (!meals_null_breakfast.equals("null") && !meals_null_lunch.equals("null") && !meals_null_dinner.equals("null"))
					{
						try
						{
							API_Funtion.mission_finish(88122841, gettoken(), API_Funtion.GetTimestamp(), 0, new JsonHttpResponseHandler()
							{

								@Override
								public void onSuccess(JSONObject response)
								{
									Log.e("log", "mission1_okk!");
									super.onSuccess(response);
								}

							});
						}
						catch (UnsupportedEncodingException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						catch (JSONException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					super.onSuccess(response);
				}
			});
			// -------------------------------//
			API_Funtion_1.mission_show(88122841, gettoken(), API_Funtion_1.GetTimestamp(), new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					//Toast.makeText(Activity_index_page.this, "mission", Toast.LENGTH_LONG).show();
					try
					{
						for (int i = 0; i < 5; i++)
						{
							if (response.getJSONArray("task").getJSONObject(i).getInt("data") == 1)
							{
								mission_flag++;
								//Toast.makeText(Activity_index_page.this, "mission"+mission_flag, Toast.LENGTH_LONG).show();
							}
						}
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//Toast.makeText(Activity_index_page.this, "mission"+mission_flag, Toast.LENGTH_LONG).show();
					if (mission_flag == 1)
					{
						mImageView_mission.setImageResource(R.drawable.mission1);
					}
					else if (mission_flag == 2)
					{
						mImageView_mission.setImageResource(R.drawable.mission2);
					}
					else if (mission_flag == 3)
					{
						mImageView_mission.setImageResource(R.drawable.mission3);
					}
					else if (mission_flag == 4)
					{
						mImageView_mission.setImageResource(R.drawable.mission4);
					}
					else if (mission_flag == 5)
					{
						mImageView_mission.setImageResource(R.drawable.mission5);
					}
					super.onSuccess(response);
				}
			});
			// -------------------------------//
			API_Funtion_1.getlastsport(88122841, gettoken(), new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					try
					{
						sportcal = response.getInt("sport_cal");
						mTextView_last_sport.setText(response.getString("msg"));
						Intent intent = new Intent(AppWidgetExample.ACTION_TEXT_CHANGED);
						intent.putExtra("sportcal", sportcal);
						intent.putExtra("max", max);
						intent.putExtra("foodcal", foodcal);
						getApplicationContext().sendBroadcast(intent);
						ringProgressDialog.dismiss();
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					super.onSuccess(response);
				}
			});
			// ----------------------------------------//

		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void lastsport()
	{
		ClassAPI API_Funtion_2 = new ClassAPI(Activity_index_page.this);
		try
		{
			final ProgressDialog ringProgressDialog = ProgressDialog.show(Activity_index_page.this, "Please wait ...", "Data Loading ...", true);
			ringProgressDialog.setCancelable(true);
			API_Funtion_2.getlastsport(88122841, gettoken(), new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					try
					{
						mTextView_last_sport.setText(response.getString("msg"));
						ringProgressDialog.dismiss();
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					super.onSuccess(response);
				}
			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.e("api2", "ok");
	}

	private void mission_image()
	{
		ClassAPI API_Funtion_3 = new ClassAPI(Activity_index_page.this);
		try
		{
			API_Funtion_3.mission_show(88122841, gettoken(), API_Funtion_3.GetTimestamp(), new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					try
					{
						for (int i = 0; i < 5; i++)
						{
							if (response.getJSONArray("task").getJSONObject(i).getInt("data") == 1)
							{
								mission_flag++;
							}
						}
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (mission_flag == 1)
					{
						mImageView_mission.setImageResource(R.drawable.mission1);
					}
					else if (mission_flag == 2)
					{
						mImageView_mission.setImageResource(R.drawable.mission2);
					}
					else if (mission_flag == 3)
					{
						mImageView_mission.setImageResource(R.drawable.mission3);
					}
					else if (mission_flag == 4)
					{
						mImageView_mission.setImageResource(R.drawable.mission4);
					}
					else if (mission_flag == 5)
					{
						mImageView_mission.setImageResource(R.drawable.mission5);
					}
					super.onSuccess(response);
				}
			});
			// Log.e("api3", "ok");
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = Activity_index_page.this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}

	private String getweekday()
	{
		String weekDay;
		SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);

		Calendar calendar = Calendar.getInstance();
		weekDay = dayFormat.format(calendar.getTime());
		return weekDay;
	}

	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case android.R.id.home:
				mNav.toggleLeftDrawer();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event)
	{// 捕捉返回鍵
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			ConfirmExit();
		}
		return super.onKeyDown(keyCode, event);
	}

	public void ConfirmExit()
	{// 退出確認
		AlertDialog.Builder ad = new AlertDialog.Builder(this);
		ad.setTitle("離開");
		ad.setMessage("確定要離開?");
		ad.setNegativeButton("否", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int i)
			{
			}
		});
		ad.setPositiveButton("是", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int i)
			{
				finish();
			}
		});
		ad.show();
	}

	private void open_reset_dialog()
	{
		AlertDialog.Builder ad_1 = new AlertDialog.Builder(Activity_index_page.this);
		ad_1.setTitle("重設計畫");
		ad_1.setMessage("確定要重設計畫嗎?");
		ad_1.setNegativeButton("否", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int i)
			{
				dialog.cancel();
			}
		});
		ad_1.setPositiveButton("是", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int i)
			{
				try
				{
					API_Funtion.reset_project(88122841, gettoken(), new JsonHttpResponseHandler()
					{

						@Override
						public void onSuccess(JSONObject response)
						{
							Intent info_intent = new Intent();
							info_intent.setClass(Activity_index_page.this, Activity_about.class);
							// mToken = null;
							// getActivity().getDir(mTokenFileName,
							// Context.MODE_PRIVATE);
							// FileOutputStream writer = null;
							// try
							// {
							// writer =
							// getActivity().openFileOutput(mTokenFileName,
							// Context.MODE_PRIVATE);
							// writer.write(response.getString("session_token").getBytes());
							// writer.close();
							// }
							// catch (IOException e)
							// {
							// // TODO Auto-generated catch block
							// e.printStackTrace();
							// }
							// catch (JSONException e)
							// {
							// // TODO Auto-generated catch block
							// e.printStackTrace();
							// }
							clearApplicationData();
							startActivity(info_intent);
							Activity_index_page.this.finish();
							// Toast.makeText(getActivity(),
							// response.getString("msg"),
							// Toast.LENGTH_LONG).show();
							super.onSuccess(response);
						}
					});
				}
				catch (UnsupportedEncodingException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		ad_1.show();
	}

	private void open_aboutus_dialog()
	{
		final Dialog dialog_aboutus = new Dialog(Activity_index_page.this, R.style.dialog);
		View myView = LayoutInflater.from(Activity_index_page.this).inflate(R.layout.dialog_aboutus, null);
		dialog_aboutus.setContentView(myView);
		mButton_Dialog_cancel_aboutus = (ImageView) myView.findViewById(R.id.dialog_process_cancel);
		mButton_Dialog_cancel_aboutus.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				dialog_aboutus.cancel();			
			}
		});
		dialog_aboutus.show();
	}
	
	private void open_logout_dialog()
	{
		AlertDialog.Builder ad_2 = new AlertDialog.Builder(Activity_index_page.this);
		ad_2.setTitle("登出");
		ad_2.setMessage("確定要登出?");
		ad_2.setNegativeButton("否", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int i)
			{
				dialog.cancel();
			}
		});
		ad_2.setPositiveButton("是", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int i)
			{
				try
				{
					API_Funtion.logout(88122841, gettoken(), new JsonHttpResponseHandler()
					{

						@Override
						public void onSuccess(JSONObject response)
						{
							Intent info_intent = new Intent();
							info_intent.setClass(Activity_index_page.this, Activity_about.class);
							// mToken = null;
							// getActivity().getDir(mTokenFileName,
							// Context.MODE_PRIVATE);
							// FileOutputStream writer = null;
							// try
							// {
							// writer =
							// getActivity().openFileOutput(mTokenFileName,
							// Context.MODE_PRIVATE);
							// writer.write(response.getString("session_token").getBytes());
							// writer.close();
							// }
							// catch (IOException e)
							// {
							// // TODO Auto-generated catch block
							// e.printStackTrace();
							// }
							// catch (JSONException e)
							// {
							// // TODO Auto-generated catch block
							// e.printStackTrace();
							// }
							clearApplicationData();
							startActivity(info_intent);
							Activity_index_page.this.finish();
							// Toast.makeText(getActivity(),
							// response.getString("msg"),
							// Toast.LENGTH_LONG).show();
							super.onSuccess(response);
						}
					});
				}
				catch (UnsupportedEncodingException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		ad_2.show();
	}

	public void clearApplicationData()
	{
		File cache = this.getCacheDir();
		File appDir = new File(cache.getParent());
		if (appDir.exists())
		{
			String[] children = appDir.list();
			for (String s : children)
			{
				if (!s.equals("lib"))
				{
					deleteDir(new File(appDir, s));
					// Log.i("TAG",
					// "**************** File /data/data/APP_PACKAGE/" + s +
					// " DELETED *******************");
				}
			}
		}
	}

	public static boolean deleteDir(File dir)
	{
		if (dir != null && dir.isDirectory())
		{
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++)
			{
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success)
				{
					return false;
				}
			}
		}

		return dir.delete();
	}
	public void launchRingDialog(View view)
	{
		final ProgressDialog ringProgressDialog = ProgressDialog.show(Activity_index_page.this, "Please wait ...", "Data Loading ...", true);
		ringProgressDialog.setCancelable(true);
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				if(mTextView_last_sport.getTextSize()!=0)
				{
					ringProgressDialog.dismiss();
				}
			}
		}).start();
	}

}
