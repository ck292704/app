package sportPage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.zip.Inflater;

import mission.Activity_Mission;

import org.json.JSONException;
import org.json.JSONObject;
import com.example.hellochart.R;
import com.example.informationPage.MainActivity;
import com.loopj.android.http.JsonHttpResponseHandler;
import API_Funtion.ClassAPI;
import android.R.integer;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class ChoseSportMenuActivity extends Activity
{

	private ActionBar mActionBar; //定義actionbar
	
	private ListView mListViewSportSpice;
	private ArrayList<String> mArrayListSportName;
	private ArrayList<Integer> mArrayListConsumeCal;	
	private ArrayList<String> mArrayListCycleKcal;
	private ArrayList<Integer> mArrayListSportId;
	private ArrayList<Integer> mArrayListCount;
	private ArrayList<Integer> mArrayListUserSportId;
	private TextView mTextViewDialogShowCount;
	private ClassAPI mApi = new ClassAPI(ChoseSportMenuActivity.this);
	private SportSpiceAdapter mAdapter;
	private int mCount = 0;
	private int mCalorise;
	private Button mButtonDialogAdd;
	private Button mButtonDialogReduct;
	private TextView mTextViewDialogShowCycle;
	private Button mButtonDialogOk;
	private ClassAPI API_Funtion = new ClassAPI(ChoseSportMenuActivity.this);

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chose_sportmenu);
		
		mActionBar = getActionBar(); //取得actionbar
		mActionBar.setDisplayHomeAsUpEnabled(true); //開啟返回主頁
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#3a5f8a"));
		mActionBar.setBackgroundDrawable(actionbar_color);		
		mArrayListConsumeCal = new ArrayList<Integer>();
		mArrayListSportName = new ArrayList<String>();	
		mArrayListCycleKcal = new ArrayList<String>();
		mArrayListSportId = new ArrayList<Integer>();
		mArrayListUserSportId = new ArrayList<Integer>();
		mArrayListCount = new ArrayList<Integer>();
		mListViewSportSpice = (ListView) findViewById(R.id.listView_SportSpice);
		mAdapter = new SportSpiceAdapter(ChoseSportMenuActivity.this, mArrayListSportName, mArrayListConsumeCal, mArrayListCycleKcal);
		mListViewSportSpice.setAdapter(mAdapter);
		getData();
		mListViewSportSpice.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, final int position, long id)
			{
				// TODO Auto-generated method stub
				mCount=0;
				Dialog dialog =new Dialog(ChoseSportMenuActivity.this,R.style.MyDialog);
				dialog.setContentView(R.layout.dialog_sportcountv2);
				LinearLayout mLinearLayoutDialog=(LinearLayout)dialog.findViewById(R.id.LinearLayout__dialogsportcountv2);
				mLinearLayoutDialog.getLayoutParams().width=450;
				mButtonDialogAdd=(Button)dialog.findViewById(R.id.button_dialogsportcountv2_addcycle);
				mButtonDialogReduct=(Button)dialog.findViewById(R.id.button_dialogsportcountv2_reductcycle);
				mButtonDialogOk=(Button)dialog.findViewById(R.id.button_dialogsportcountv2_ok);
				mTextViewDialogShowCycle=(TextView)dialog.findViewById(R.id.textView_dialogsportcountv2_chosecycle);
				mButtonDialogAdd.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(mCount<20){
							mCount++;
							mTextViewDialogShowCycle.setText(""+mCount);
						}
					}
				});
				mButtonDialogReduct.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(mCount!=0){
							mCount--;
							mTextViewDialogShowCycle.setText(""+mCount);
						}
					}
				});
				mButtonDialogOk.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(mCount!=0){
							mCalorise=0;
							mCalorise+=mArrayListConsumeCal.get(position)*mCount;
							mArrayListCount.add(mCount);
							Bundle bundle=new Bundle();
							bundle.putInt("Calorise", mCalorise);
							bundle.putInt("Count", mCount);
							bundle.putInt("UserSportId",mArrayListSportId.get(position));
							bundle.putString("SportSpice",mArrayListSportName.get(position));
							Intent intent =new Intent();
							intent.putExtras(bundle);
							setResult(RESULT_OK,intent);
							ChoseSportMenuActivity.this.finish();
							
							try
							{
								API_Funtion.mission_finish(88122841, gettoken(), API_Funtion.GetTimestamp(), 1, new JsonHttpResponseHandler()
								{

									@Override
									public void onSuccess(JSONObject response)
									{
										Log.e("log", "mission2_okk!");
										super.onSuccess(response);
									}
									
								});
							}
							catch (UnsupportedEncodingException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							catch (JSONException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
						}
					}
				/*AlertDialog.Builder dialog = new AlertDialog.Builder(ChoseSportMenuActivity.this);
				dialog.setTitle("Chose SportCount");
				LayoutInflater inflater = ChoseSportMenuActivity.this.getLayoutInflater();
				View layout = inflater.inflate(R.layout.dialog_sportcount, null);
				dialog.setView(layout);
				mTextViewDialogShowCount = (TextView) layout.findViewById(R.id.textView_dialogsportcount_chosecycle);
				SeekBar seekbarcount = (SeekBar) layout.findViewById(R.id.seekBar_dialogsportcount_chose);
				seekbarcount.setMax(20);
				seekbarcount.setOnSeekBarChangeListener(new OnSeekBarChangeListener()
				{
					@Override
					public void onStopTrackingTouch(SeekBar seekBar)
					{
						// TODO Auto-generated method stub

					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar)
					{
						// TODO Auto-generated method stub

					}

					@Override
					public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
					{
						// TODO Auto-generated method stub

						// ---------8/4---------
						mTextViewDialogShowCount.setText(+progress + "  Cycle");
						// ---------8/4---------

						mCount = progress;
						mArrayListCount.add(progress);
					}
				});
				/*dialog.setNegativeButton("Ok", new OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						// TODO Auto-generated method stub
						if (mCount != 0)
						{
							mCalorise = 0;
							mCalorise += mArrayListConsumeCal.get(position) * mCount;
							
							Bundle bundle = new Bundle();
							bundle.putInt("Calorise", mCalorise);
							bundle.putInt("Count", mCount);
							bundle.putInt("UserSportId", mArrayListSportId.get(position));
							bundle.putString("SportSpice", mArrayListSportName.get(position));						
							Intent intent = new Intent();
							intent.putExtras(bundle);
							setResult(RESULT_OK, intent);
							ChoseSportMenuActivity.this.finish();
						}
					}					
				});*/
				});
				dialog.show();
			}
		});

	}

	private void getData()
	{
		try
		{

			mApi.sportlist(88122841, gettoken(), new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					// TODO Auto-generated method stub
					super.onSuccess(response);
					try
					{
						for (int i = 0; i < response.getJSONArray("sportlist").length(); i++)
						{

							mArrayListSportName.add(response.getJSONArray("sportlist").getJSONObject(i).getString("sport_name"));
							mArrayListConsumeCal.add(response.getJSONArray("sportlist").getJSONObject(i).getInt("sport_cal"));
							mArrayListSportId.add(response.getJSONArray("sportlist").getJSONObject(i).getInt("sport_id"));						
							mArrayListCycleKcal.add(response.getJSONArray("sportlist").getJSONObject(i).getString("sport_portion"));
							mAdapter.addItem(mAdapter.getCount() + 1);
						}
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = ChoseSportMenuActivity.this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{    
	   switch (item.getItemId()) 
	   {        
	      case android.R.id.home:            
	         Intent intent = new Intent(this, Sportpage.class);            
	         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
	         startActivity(intent);          
	         this.finish();
	         return true;        
	      default:            
	         return super.onOptionsItemSelected(item);    
	   }
	}

}
