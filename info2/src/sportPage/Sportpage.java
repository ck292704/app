package sportPage;

import index_page.Activity_index_page;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import widget.AppWidgetExample;

import com.example.hellochart.R;
import com.example.informationPage.Activity_MealsPage;
import com.example.informationPage.MainActivity;
import com.loopj.android.http.JsonHttpResponseHandler;

import API_Funtion.ClassAPI;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class Sportpage extends Activity
{

	private static final int ACTIVITY_REPORY = 1000;
	private Button mButtonAdd;
	private ListView mListViewSportDate;
	//
	
	private ActionBar mActionBar; //定義actionbar
	
	private ClassAPI API_Funtion = new ClassAPI(Sportpage.this);
	private ImageView mButton_Dialog_cancel;
	private ProgressBar mProcess_food;
	private ProgressBar mProcess_sport;
	private TextView mTextView_food;
	private TextView mTextView_sport;
	private CheckBox mCheckBox_skip;
	private int mPersonalworkValueMax, mPersonalworkValueMin_food, mPersonalworkValueMin_sport;
	int mSumCal_food = 0;
	int mSumCal_sport = 0;
	private int skip_tmp =0;
	
	public ArrayList<String> mArrayListSportName;
	public ArrayList<Integer> mArrayListUserSportId;
	public ArrayList<Integer> mArrayListConsumeCalorise;
	public ArrayList<Integer> mArrayListmCount;
	public ArrayList<Integer> mArrayListUserDownloadSportId;
	private ClassAPI mApI = new ClassAPI(Sportpage.this);
	//
	private SportInformationAdapter mAdapter;
	private int mCalorise;
	int mCaloriseTotal;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sports_main_page);
		
		mActionBar = getActionBar(); //取得actionbar
		mActionBar.setDisplayHomeAsUpEnabled(true); //開啟返回主頁
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#3a5f8a"));
		mActionBar.setBackgroundDrawable(actionbar_color);
		mButtonAdd = (Button) findViewById(R.id.button_add_sport);
		
		mListViewSportDate = (ListView) findViewById(R.id.listView_show_informatiom);
		// listviewinformation
		mArrayListConsumeCalorise = new ArrayList<Integer>();
		mArrayListSportName = new ArrayList<String>();
		mArrayListUserSportId = new ArrayList<Integer>();
		mArrayListmCount = new ArrayList<Integer>();
		mArrayListUserDownloadSportId = new ArrayList<Integer>();
		// Read sportinformation on server

		mAdapter = new SportInformationAdapter(Sportpage.this, mArrayListSportName, mArrayListConsumeCalorise);
		mListViewSportDate.setAdapter(mAdapter);
		
		getUserData();

		mArrayListUserSportId.addAll(mArrayListUserDownloadSportId);
		// mTextViewShowTotalCalorise.setText("Calories Consume:" +
		// mCaloriseTotal + " Kcal");
		mButtonAdd.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(Sportpage.this, ChoseSportMenuActivity.class);
				startActivityForResult(intent, ACTIVITY_REPORY);
			}
		});
	}

	private void getUserData()
	{
		try
		{
			final ProgressDialog ringProgressDialog = ProgressDialog.show(Sportpage.this, "Please wait ...", "Data Loading ...", true);
			ringProgressDialog.setCancelable(true);
			mApI.calendar_sport(88122841, gettoken(), mApI.GetTimestamp(), new JsonHttpResponseHandler()
			{

				@Override
				public void onSuccess(JSONObject response)
				{

					// TODO Auto-generated method stub
					super.onSuccess(response);
					try
					{
						for (int i = 0; i < response.getJSONArray("sport").length(); i++)
						{

							mArrayListSportName.add(response.getJSONArray("sport").getJSONObject(i).getString("sports_name"));
							mArrayListUserSportId.add(response.getJSONArray("sport").getJSONObject(i).getInt("sports_id"));
							mArrayListmCount.add(response.getJSONArray("sport").getJSONObject(i).getInt("sport_cycle"));
							int sport_cal, sport_cycle;
							sport_cal = response.getJSONArray("sport").getJSONObject(i).getInt("sports_cal");
							sport_cycle = response.getJSONArray("sport").getJSONObject(i).getInt("sport_cycle");
							mArrayListConsumeCalorise.add(sport_cal * sport_cycle);
							mCaloriseTotal += sport_cal * sport_cycle;
							mButtonAdd.setText("Add your sport\n\nCalories Consume:" + mCaloriseTotal + " Kcal");
							mAdapter.addItem(mAdapter.getCount() + 1);
						}
						// Toast.makeText(Sportpage.this,""+mArrayListUserSportId,Toast.LENGTH_LONG).show();

					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(getSkip()!="")
					{
						//Toast.makeText(Sportpage.this, getSkip(), Toast.LENGTH_LONG).show();
						skip_tmp = Integer.valueOf(getSkip());
					}
					if (skip_tmp == 0)
					{
						open_dialog(0);
						ringProgressDialog.dismiss();
					}
					else
					{
					}
				}

			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		super.onActivityResult(requestCode, resultCode, intent);
		if (resultCode == RESULT_OK)
		{
			if (requestCode == ACTIVITY_REPORY)
			{
				Bundle bundle = intent.getExtras();
				mArrayListUserSportId.add(bundle.getInt("UserSportId"));
				mArrayListmCount.add(bundle.getInt("Count"));
				mArrayListSportName.add(bundle.getString("SportSpice"));
				mCalorise = bundle.getInt("Calorise");
				mArrayListConsumeCalorise.add(mCalorise);
				mCaloriseTotal += mCalorise;
				mButtonAdd.setText("Add your sport\n\nCalories Consume:" + mCaloriseTotal + " Kcal");
				mAdapter.addItem(mAdapter.getCount() + 1);
				if(mCaloriseTotal>=300)
				{
					try
					{
						mApI.mission_finish(88122841, gettoken(), mApI.GetTimestamp(), 2, new JsonHttpResponseHandler()
						{

							@Override
							public void onSuccess(JSONObject response)
							{
								Log.e("mission", "mission3_ok");
								super.onSuccess(response);
							}
						});
					}
					catch (UnsupportedEncodingException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				updateData();
				// Toast.makeText(Sportpage.this,"ID:"+mArrayListUserSportId+"&Count:"+mArrayListmCount,Toast.LENGTH_LONG).show();
			}
		}
	};

	public void updateData()
	{
		try
		{
			mApI.sports_update(88122841, gettoken(), mArrayListUserSportId, mArrayListmCount, new JsonHttpResponseHandler()
			{

				@Override
				public void onSuccess(JSONObject response)
				{
					// TODO Auto-generated method stub
					super.onSuccess(response);
					// Toast.makeText(Sportpage.this,"Oh,yes",
					// Toast.LENGTH_LONG).show();
				}

			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = Sportpage.this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			Intent intent = new Intent();
			intent.setClass(Sportpage.this, Activity_index_page.class);
			startActivity(intent);
			this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{    
	   switch (item.getItemId()) 
	   {        
	      case android.R.id.home:            
	         Intent intent = new Intent(this, Activity_index_page.class);            
	         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
	         startActivity(intent);          
	         this.finish();
	         return true;        
	      default:            
	         return super.onOptionsItemSelected(item);    
	   }
	}
	private String getSkip() // 取得token
	{
		String fileName = "Skip_sport";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = Sportpage.this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}
	
	private void open_dialog(int tmp)
	{
		final Dialog dialog_process = new Dialog(Sportpage.this, R.style.dialog);
		View myView = LayoutInflater.from(Sportpage.this).inflate(R.layout.dialog_food_sport_process, null);
		dialog_process.setContentView(myView);
		mButton_Dialog_cancel = (ImageView) myView.findViewById(R.id.dialog_process_cancel);
		mProcess_food = (ProgressBar) myView.findViewById(R.id.progressBar_food);
		mProcess_sport =(ProgressBar) myView.findViewById(R.id.progressBar_sport);
		mTextView_food =(TextView) myView.findViewById(R.id.textView_food_cal);
		mTextView_sport =(TextView) myView.findViewById(R.id.textView_sport_cal);
		mCheckBox_skip = (CheckBox) myView.findViewById(R.id.checkBox_dialog_food_sport);
		mButton_Dialog_cancel.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				dialog_process.cancel();			
			}
		});
		if(tmp==0)
		{
			dialog_process.show();
		}
		else
		{
		}
		
		
		try
		{
			API_Funtion.food_show(88122841, gettoken(), API_Funtion.GetTimestamp(), new JsonHttpResponseHandler()
			{

				@Override
				public void onSuccess(JSONObject response)
				{
					try
					{
						mSumCal_food = response.getInt("foodcal_all");
						mPersonalworkValueMax = response.getInt("user_cal");
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// // 個人工作量最大值
					mPersonalworkValueMin_food = mSumCal_food; // 個人累積飲食卡路里

					mTextView_food.setText("" + mPersonalworkValueMin_food);
					mTextView_sport.setText("" + mCaloriseTotal);
					
					mProcess_food.setMax(mPersonalworkValueMax); // 設定最大值
					mProcess_sport.setMax(mPersonalworkValueMax);
					mProcess_food.incrementProgressBy(mPersonalworkValueMin_food);// 遞增進度調
					mProcess_sport.incrementSecondaryProgressBy(mCaloriseTotal);
//					Intent intent = new Intent(AppWidgetExample.ACTION_TEXT_CHANGED);
//					intent.putExtra("sportcal", mCaloriseTotal);
//					intent.putExtra("max", mPersonalworkValueMax);
//					intent.putExtra("foodcal", mPersonalworkValueMin_food);
//					getApplicationContext().sendBroadcast(intent);
					// Toast.makeText(getActivity(), "123",
					// Toast.LENGTH_LONG).show();
					super.onSuccess(response);
				}
			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		mCheckBox_skip.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				if (mCheckBox_skip.isChecked())
				{
					String fileName_1 = "Skip_sport";
					getDir(fileName_1, Context.MODE_PRIVATE);
					try
					{
						FileOutputStream writer_1 = openFileOutput(fileName_1, Context.MODE_PRIVATE);
						writer_1.write(String.valueOf(1).getBytes());
						writer_1.close();
					}
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					String fileName_1 = "Skip_sport";
					getDir(fileName_1, Context.MODE_PRIVATE);
					try
					{
						FileOutputStream writer_1 = openFileOutput(fileName_1, Context.MODE_PRIVATE);
						writer_1.write(String.valueOf(0).getBytes());
						writer_1.close();
					}
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		});
		
	}
	
}
