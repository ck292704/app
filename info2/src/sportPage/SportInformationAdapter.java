package sportPage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.hellochart.R;
import com.loopj.android.http.JsonHttpResponseHandler;

import API_Funtion.ClassAPI;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView.FindListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SportInformationAdapter extends BaseAdapter{

	private ArrayList<Integer> mArraylist;
	
	private TextView mTextViewSportspice;
	private TextView mTextViewSportCal;
	private Button mButtonAdd;
	private ArrayList<String> mArrayListSportName;
	private ArrayList<Integer> mArrayListConsumeCalorise;
	private ListView mListViewSportInf;
	private Button mButtonDelet;
	private Sportpage sportpage;	
	String mSportSpice, mSportTime;
	LayoutInflater inflater;
	Context mContext;
	public SportInformationAdapter(Sportpage context,ArrayList<String> sportSpiewlist,ArrayList<Integer> callist){
		mArrayListSportName=sportSpiewlist;
		mArrayListConsumeCalorise=callist;
		mContext=context;
		this.sportpage=context;
		
		inflater=LayoutInflater.from(context);
		mArraylist=new ArrayList<Integer>();
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mArraylist.size();
		
	}
	public void addItem(int position){
		mArraylist.add(position);
		this.notifyDataSetChanged();
	}
	public void removeItem(int position){
	    if(!mArraylist.isEmpty()){
	    	mArraylist.remove(position);
	        this.notifyDataSetChanged();
	        
	    }
	}
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		convertView=inflater.inflate(R.layout.adapter_sportinformation_row,null);
		mTextViewSportspice=(TextView)convertView.findViewById(R.id.textView_sportinfmation_sportname);
		mTextViewSportCal=(TextView)convertView.findViewById(R.id.textView_sportinfmation_consume);
		mButtonDelet=(Button)convertView.findViewById(R.id.button_sportinfmation_delete);
		mTextViewSportspice.setText(""+mArrayListSportName.get(position));
		mTextViewSportCal.setText(""+mArrayListConsumeCalorise.get(position)+" Kcal");
		mButtonDelet.setOnClickListener(new mButtonDeletitem(this.sportpage,position));
	
		return convertView;
	}
	class mButtonDeletitem implements OnClickListener{
		private int postion;
		private Sportpage Sportpage;
		private ClassAPI mApi=new ClassAPI(mContext);
		mButtonDeletitem(Sportpage context, int pos) {
		        this.Sportpage = context;
		        postion = pos;
		    }

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			this.Sportpage.mCaloriseTotal-=mArrayListConsumeCalorise.get(postion);
			//Toast.makeText(mainActivity, ""+mCalTotal, Toast.LENGTH_LONG).show();
			
			//Delete ListViewInformation
			mArrayListSportName.remove(postion);
			this.Sportpage.mArrayListUserSportId.remove(postion);
			this.Sportpage.mArrayListmCount.remove(postion);
			mArrayListConsumeCalorise.remove(postion);
			try {
				mApi.sports_update(88122841, Sportpage.gettoken(),Sportpage.mArrayListUserSportId,Sportpage.mArrayListmCount, new JsonHttpResponseHandler(){
					
					@Override
					public void onSuccess(JSONObject response) {
						// TODO Auto-generated method stub
						Log.e(null, "Ok");
						super.onSuccess(response);
						//Toast.makeText(Sportpage.this,"Oh,yes", Toast.LENGTH_LONG).show();
					}
					
				});
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mButtonAdd = (Button)Sportpage.findViewById(R.id.button_add_sport);
			mButtonAdd.setText("Add your sport\n\nCalories Consume:" + sportpage.mCaloriseTotal + " Kcal");
			removeItem(getCount()-1);
			
		}
		
	}
}
