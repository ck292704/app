package sportPage;

import java.util.ArrayList;

import com.example.hellochart.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView.FindListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SportSpiceAdapter extends BaseAdapter{

	private ArrayList<Integer> mArraylist;
	private ImageView mImageViewSportIcon;
	private TextView mTextViewSportspice;
	private TextView mTextViewConsumeCal;
	//---------8/4---------
	private TextView mTextViewCycleKcal;
	//---------8/4---------
	//private ArrayList<String> mArrayListCycleKcal;
	private ArrayList<String> mArrayListSportname;
	private ArrayList<Integer> mArrayListConsumeCal;
	private ArrayList<String> mArrayListCycleKcal;
	private ChoseSportMenuActivity  chosesportmenuactivity;
	Context mContext;
	LayoutInflater inflater;
	
	//---------8/4---------
	public SportSpiceAdapter(ChoseSportMenuActivity context,ArrayList<String> sportSpiewlist,
			ArrayList<Integer> concallist,ArrayList<String> cyclekcal){
	//---------8/4---------	
		
		mArrayListSportname=sportSpiewlist;
		mArrayListConsumeCal=concallist;
		
		//---------8/4---------
		mArrayListCycleKcal=cyclekcal;
		//---------8/4---------
		mArraylist=new ArrayList<Integer>();
		this.chosesportmenuactivity=context;
		inflater=LayoutInflater.from(context);	
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mArraylist.size();
		
		
		
	}
	public void addItem(int position){
		mArraylist.add(position);
		this.notifyDataSetChanged();
	}
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		convertView=inflater.inflate(R.layout.adapter_sportspice_row, null);
		mTextViewConsumeCal=(TextView)convertView.findViewById(R.id.textView_sportspice_row_Consume);
		mTextViewSportspice=(TextView)convertView.findViewById(R.id.textView_sportspice_row_Sportname);
		mTextViewCycleKcal=(TextView)convertView.findViewById(R.id.textView_sportspice_row_cyclecal);
		mTextViewSportspice.setText(""+mArrayListSportname.get(position));
		mTextViewConsumeCal.setText(""+mArrayListConsumeCal.get(position));
		//---------8/4---------
		mTextViewCycleKcal.setText(""+mArrayListCycleKcal.get(position)+"/1cycle");
		//---------8/4---------
		return convertView;
	}
}