package API_Funtion;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import android.content.Context;
import android.media.MediaCodecList;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

public class ClassAPI
{
	public static AsyncHttpClient mClient = new AsyncHttpClient();
	public String mBaseUrl = "http://www.healthrecord.lomrt.com/cjcu";
	public Context mContext = null;
	
	public ClassAPI(Context cont){
		mContext = cont;
	}
	
	
	public void login(int apikey, String fb_token,JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException
	{
		JSONObject mJSONParams = new JSONObject();
		JSONObject mJSONuserdata = new JSONObject();

		mJSONParams.put("apikey", apikey);
		mJSONParams.put("userdata", mJSONuserdata.put("user_fb_token", fb_token));
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/auth/login/", entity, "application/json", handler);
	}

	public void user_sign(int apikey, String fb_token, String sex, int age, Double weight, Double height, String name, String work_intensity, int user_cal, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException
	{
		JSONObject mJSONParams = new JSONObject();
		JSONObject mJSONuserdata = new JSONObject();

		mJSONParams.put("apikey", apikey);
		mJSONParams.put("userdata", mJSONuserdata.put("user_fb_token", fb_token));
		mJSONParams.put("userdata", mJSONuserdata.put("user_sex", sex));
		mJSONParams.put("userdata", mJSONuserdata.put("user_age", age));
		mJSONParams.put("userdata", mJSONuserdata.put("user_weight", weight));
		mJSONParams.put("userdata", mJSONuserdata.put("user_height", height));
		mJSONParams.put("userdata", mJSONuserdata.put("user_name", name));
		mJSONParams.put("userdata", mJSONuserdata.put("user_work_intensity", work_intensity));
		mJSONParams.put("userdata", mJSONuserdata.put("user_cal", user_cal));
		
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/auth/user_sign/", entity, "application/json", handler);
	}

	public void logout(int apikey, String fb_token, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException
	{
		JSONObject mJSONParams = new JSONObject();
		JSONObject mJSONuserdata = new JSONObject();

		mJSONParams.put("apikey", apikey);
		mJSONParams.put("userdata", mJSONuserdata.put("user_fb_token", fb_token));
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/auth/logout/", entity, "application/json", handler);
	}
	
	public void reset_project(int apikey, String session_token, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException
	{
		JSONObject mJSONParams = new JSONObject();
		//JSONObject mJSONuserdata = new JSONObject();

		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", session_token);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/auth/reset/", entity, "application/json", handler);
	}

	public void weight_day(int apikey, String session_token, Double weight,double waistline,double height, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException  //K
	{
		JSONObject mJSONParams = new JSONObject();
		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", session_token);
		mJSONParams.put("weight", weight);
		mJSONParams.put("waistline", waistline);
		mJSONParams.put("height", height);
//		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
//		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		StringEntity entity = new StringEntity(mJSONParams.toString());
		Log.e("test", mJSONParams.toString());

		mClient.post(mContext, mBaseUrl+"/DataForSet/weight_day/", entity, "application/json", handler);

	}

	public void weight_day_show(int apikey, String session_token, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException  //D
	{
		JSONObject mJSONParams = new JSONObject();
		
		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", session_token);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/DataForShow/weight_day_show/", entity, "application/json", handler);
		
//		mClient.post(mContext, BaseUrl+"/DataForShow/weight_day_show/", entity, "application/json", new JsonHttpResponseHandler()
//		{
//
//			@Override
//			public void onSuccess(JSONObject response)
//			{
//				try
//				{
//					JSONArray array = response.getJSONArray("user_weight");
//					StringBuilder stringBuilder_1 = new StringBuilder();
//					StringBuilder stringBuilder_2 = new StringBuilder();
//					for (int i = 0; i < array.length(); i++)
//					{
//						stringBuilder_1.append(array.getJSONObject(i).getString("weight"));
//						stringBuilder_2.append(array.getJSONObject(i).getString("date"));
//					}
//
//					Toast.makeText(mContext, "" + stringBuilder_1.toString() + "  " + stringBuilder_2.toString(), Toast.LENGTH_LONG).show();
//					Log.e("ohmy", response.toString());
//				}
//				catch (JSONException e)
//				{
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				super.onSuccess(response);
//			}
//
//		});
	}
	
	public void food_show(int apikey, String session_token, int timestamp, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException  //E
	{
		JSONObject mJSONParams = new JSONObject();
		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", session_token);
		mJSONParams.put("date", timestamp);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/DataForShow/food/", entity, "application/json", handler);
	}
	
	public void food_partition(int apikey, String session_token, int partition, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException  //H
	{
		JSONObject mJSONParams = new JSONObject();
		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", session_token);
		mJSONParams.put("food_partition", partition);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/Foods/food_partition/", entity, "application/json", handler);
	}
	
	public void food_chose(int apikey, String session_token, String meal, ArrayList food_id_array, ArrayList food_portion_array, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException  //I
	{
		JSONObject mJSONParams = new JSONObject();
		JSONObject mJSONParams_2 = new JSONObject();
		JSONArray mJSONarray = new JSONArray();
		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", session_token);
		mJSONParams.put("meal", meal);
		for(int i = 0;i<food_id_array.size();i++)
		{
			mJSONParams_2.put("food_id", food_id_array.get(i));
			mJSONParams_2.put("food_portion", food_id_array.get(i));
			mJSONarray.put(mJSONParams_2);
		}
		
		mJSONParams.put("fooddata", mJSONarray);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/food_chose", entity, "application/json", handler);
	}
	
	public void food_update(int apikey, String session_token, String meal, JSONArray food_array, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException  //J
	{
		JSONObject mJSONParams = new JSONObject();
		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", session_token);
		mJSONParams.put("meal", meal);
		mJSONParams.put("fooddata", food_array);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/Foods/food_update/", entity, "application/json", handler);
	}
	
	public void event_day_add(int apikey, String session_token, String event, int date_timestamp, int time_timestamp, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException  //輸入資訊(備忘錄)
	{
		JSONObject mJSONParams = new JSONObject();
		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", session_token);
		mJSONParams.put("event_day", event);
		mJSONParams.put("date", date_timestamp);
		mJSONParams.put("time", time_timestamp);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/DataForSet/event_day/", entity, "application/json", handler);
	}
	
	public void event_day_show(int apikey, String session_token, int timestamp, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException  //L,給予日曆顯示資訊(備忘錄)
	{
		JSONObject mJSONParams = new JSONObject();
		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", session_token);
		mJSONParams.put("date", timestamp);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		Log.e("qq", mJSONParams.toString());
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/DataForShow/event_day_show/", entity, "application/json", handler);
	}
	
	public void calendar_sport(int apikey, String session_token, int timestamp, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException  //N,給予日曆顯示資訊(運動)
	{
		JSONObject mJSONParams = new JSONObject();
		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", session_token);
		mJSONParams.put("date", timestamp);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/DataForShow/sport/", entity, "application/json", handler);
	}
	
	public void sportlist(int apikey, String session_token, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException   //O,顯示運動清單
	{
		JSONObject mJSONParams = new JSONObject();
		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", session_token);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/Sports/sportlist/", entity, "application/json", handler);
	} 
	
	public void event_delete(int apikey, String session_token, String event, int date, int time, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException //Q,選擇運動內容
	{
		JSONObject mJSONParams_1 = new JSONObject();
		//JSONObject mJSONParams_2 = new JSONObject();
		mJSONParams_1.put("apikey", apikey);
		mJSONParams_1.put("session_token", session_token);
		mJSONParams_1.put("event_day", event);
		mJSONParams_1.put("date", date);
		mJSONParams_1.put("time", time);
		byte[] bytes = mJSONParams_1.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams_1.toString());
		mClient.post(mContext, mBaseUrl+"/DataForSet/event_day_delete/", entity, "application/json", handler);
	}
	
	public void sports_update(int apikey, String session_token, ArrayList<Integer> id, ArrayList<Integer> cycle, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException //Q,修改運動內容
	{
		JSONObject mJSONParams_1 = new JSONObject();
		JSONObject mJSONParams_2 = new JSONObject();
		JSONArray mJSONarray = new JSONArray();
		for(int i = 0;i<id.size();i++)
		{
			JSONObject mJSONParams_3 = new JSONObject();
			mJSONParams_3.put("sports_id", id.get(i));
			mJSONParams_3.put("user_sport_cycle", cycle.get(i));
			mJSONarray.put(mJSONParams_3);
		}
		mJSONParams_1.put("apikey", apikey);
		mJSONParams_1.put("session_token", session_token);
		mJSONParams_1.put("sportdata", mJSONarray);

		byte[] bytes = mJSONParams_1.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams_1.toString());
		mClient.post(mContext, mBaseUrl+"/Sports/sport_update/", entity, "application/json", handler);
	}
	
	public void newslist(int apikey, String session_token, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException //顯示新聞清單
	{
		JSONObject mJSONParams = new JSONObject();
		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", session_token);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/newslist", entity, "application/json", handler);
	}
	
	public void enter_news(int apikey, String session_token, int news_id, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException //進入新聞
	{
		JSONObject mJSONParams = new JSONObject();
		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", session_token);
		mJSONParams.put("news_id", news_id);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/news", entity, "application/json", handler);
	}
	
	public void news_event(int apikey, String session_token, int news_id, Text event,JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException //S,留言
	{
		JSONObject mJSONParams = new JSONObject();
		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", session_token);
		mJSONParams.put("news_id", news_id);
		mJSONParams.put("event", event);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/news_event", entity, "application/json", handler);
	}
	
	public void search_food(int apikey, String food_name, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException //進入新聞
	{
		JSONObject mJSONParams = new JSONObject();
		mJSONParams.put("apikey", apikey);
		mJSONParams.put("food_name", food_name);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/Foods/food_find_name/", entity, "application/json", handler);
	}
	
	public void mission_finish(int apikey, String fb_token, int timestamp, int task_id,JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException
	{
		JSONObject mJSONParams = new JSONObject();

		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", fb_token);
		mJSONParams.put("date", timestamp);
		mJSONParams.put("task", task_id);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/task/task_set/", entity, "application/json", handler);
	}
	
	public void mission_show(int apikey, String fb_token, int timestamp, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException
	{
		JSONObject mJSONParams = new JSONObject();

		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", fb_token);
		mJSONParams.put("date", timestamp);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/task/task_show/", entity, "application/json", handler);
	}
	
	public void getlastsport(int apikey, String fb_token, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException
	{
		JSONObject mJSONParams = new JSONObject();

		mJSONParams.put("apikey", apikey);
		mJSONParams.put("session_token", fb_token);
		byte[] bytes = mJSONParams.toString().getBytes("utf-8");
		ByteArrayEntity entity = new ByteArrayEntity(bytes);
		//StringEntity entity = new StringEntity(mJSONParams.toString());
		mClient.post(mContext, mBaseUrl+"/Sports/last_sport/", entity, "application/json", handler);
	}
	
	public int GetTimestamp(){
		java.util.Date date= new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy"); //Timestamp 格式
		Timestamp tms = new Timestamp(date.getTime());  //取得目前時間
		int timestm  = Integer.parseInt(sdf.format(tms)); //轉成整數型態
		return timestm;
	}
}
