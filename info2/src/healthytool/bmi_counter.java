package healthytool;

import index_page.Activity_index_page;

import java.text.DecimalFormat;

import com.example.hellochart.R;
import com.example.informationPage.Activity_MealsPage;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class bmi_counter extends Activity {
	private Button btn_commit;
	private EditText edit_height;
	private EditText edit_weight;
	private TextView text_bmi, text_standard, text_dream;
	private ImageButton mBtn_man;
	private ImageButton mBtn_woman;
	private boolean sex_value = true;
	private double height,height2;
	private double weight;
	private double bmi_result = 0;
	private double standard_result = 0;
	private double dream_result1 = 0;
	private double dream_result2 = 0;
	private double dream_tmp;
	DecimalFormat nf = new DecimalFormat("0.00");

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bmi_counter);
		initActionBar();
		mBtn_man = (ImageButton)findViewById(R.id.img_btn_man);
		mBtn_woman = (ImageButton)findViewById(R.id.img_btn_woman);
		btn_commit = (Button)findViewById(R.id.commit);
		edit_height = (EditText)findViewById(R.id.edit_event);
		edit_weight = (EditText)findViewById(R.id.edit_weight);
		text_bmi = (TextView)findViewById(R.id.text_bmi);
		text_dream = (TextView)findViewById(R.id.text_dream_weight);
		text_standard = (TextView)findViewById(R.id.text_standard_weight);
		mBtn_woman.setImageAlpha(25);
		setImgBtnonclick();
		setcommit();
	}

	private void initActionBar()
	{
		ActionBar bar = getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#ef3a5f8a"));
		bar.setBackgroundDrawable(actionbar_color);
	}
	
	private void setcommit() {
		btn_commit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try {
					height2 = Double.parseDouble(edit_height.getText().toString());
					height = Double.parseDouble(edit_height.getText().toString())/100;
					weight = Double.parseDouble(edit_weight.getText().toString());
					bmi_result = weight / (height*height);
					
					if(sex_value==true){
						standard_result = (height2-80)*0.7;
						dream_tmp = standard_result*0.1;
						dream_result1 = standard_result-dream_tmp;
						dream_result2 = standard_result+dream_tmp;
						text_standard.setText(nf.format(standard_result)+"   KG");
						text_dream.setText(nf.format(dream_result1)+" ~ "+nf.format(dream_result2)+"   KG");
					}
					else{
						standard_result = (height2-70)*0.6;
						dream_tmp = standard_result*0.1;
						dream_result1 = standard_result-dream_tmp;
						dream_result2 = standard_result+dream_tmp;
						text_standard.setText(nf.format(standard_result)+"   KG");
						text_dream.setText(nf.format(dream_result1)+" ~ "+nf.format(dream_result2)+"   KG");
					}
					
					text_bmi.setText(nf.format(bmi_result));
				} catch (Exception obj) {
					Toast.makeText(bmi_counter.this, "請輸入身高,體重", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	private void setImgBtnonclick() {
		mBtn_man.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				sex_value = true;
				if(sex_value==true){
					mBtn_woman.setImageAlpha(25);
					mBtn_man.setImageAlpha(1000);
				}
			}
		});
		
		mBtn_woman.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				sex_value = false;
				if(sex_value==false){
					mBtn_man.setImageAlpha(25);
					mBtn_woman.setImageAlpha(1000);
				}
			}
		});
		
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			Intent intent = new Intent();
			intent.setClass(bmi_counter.this, Activity_index_page.class);
			startActivity(intent);
			this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{    
	   switch (item.getItemId()) 
	   {        
	      case android.R.id.home:            
	         Intent intent = new Intent(this, Activity_index_page.class);            
	         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
	         startActivity(intent);
	         bmi_counter.this.finish();
	         return true;        
	      default:            
	         return super.onOptionsItemSelected(item);    
	   }
	}

	

}
