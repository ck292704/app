package healthytool;

import java.text.DecimalFormat;

import com.example.hellochart.R;

import android.app.Fragment;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class delayEnjoy extends Fragment {
	private Button btn_start;
	private Button btn_reset;
	private TextView timer;
	private long min;
	private long sec;
	DecimalFormat nf = new DecimalFormat("00");
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return initView(inflater, container);
	}

	private View initView(LayoutInflater inflater, ViewGroup container) {
		View view = inflater.inflate(R.layout.delay_enjoy, container, false);
		timer = (TextView)view.findViewById(R.id.text_timer);
		btn_start = (Button)view.findViewById(R.id.btn_start);
		btn_reset = (Button)view.findViewById(R.id.btn_reset);
		
		final CountDownTimer mtimer =new CountDownTimer(600000, 1000){

			@Override
			public void onTick(long millisUntilFinished) {
				min = millisUntilFinished / 60000;
				sec = millisUntilFinished % 60000 /1000;
				timer.setText(nf.format(min)+" : "+nf.format(sec));
			}

			@Override
			public void onFinish() {
				timer.setText("成功完成,開動!");
			}	
			
		};
		
		btn_start.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				mtimer.start();
				btn_start.setClickable(false);
			}
		});
		
		btn_reset.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mtimer.cancel();
				btn_start.setClickable(true);
				timer.setText("請按開始鍵");
			}
		});
		
		
		return view;
	}
}
