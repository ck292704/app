package healthytool;

import java.util.ArrayList;

import com.example.calendarPage.Activity_CalendarPage;
import com.example.hellochart.R;
import com.example.informationPage.Activity_InformationPage;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
@SuppressLint("NewApi")
public class Fragment_HealthytoolPage extends Fragment{
	 TabManager mTabManager;

	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        mTabManager = new TabManager(getActivity(), getChildFragmentManager(),R.id.tab1);
	        
	    }

	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
	        View v = inflater.inflate(R.layout.fragment_healthytool, container, false);
	        TabHost host = mTabManager.handleCreateView(v);
	        host.setup();
	        mTabManager.addTab(host.newTabSpec("relax").setIndicator("延遲享受"),
	                delayEnjoy.class, null);
	        mTabManager.addTab(host.newTabSpec("counter").setIndicator("BMI計算器"),
	                bmi_counter.class, null);
	       //mTabManager.addTab(host.newTabSpec("XXXX").setIndicator("XXXX"),
	                //Fragment_InformationPage.class, null);
	       // mTabManager.addTab(host.newTabSpec("throttle").setIndicator("Throttle"),
	        //        LoaderThrottle.ThrottledLoaderListFragment.class, null);

	        return v;
	    }

	    @SuppressLint("NewApi")
		@Override
	    public void onViewStateRestored(Bundle savedInstanceState) {
	        super.onViewStateRestored(savedInstanceState);
	        mTabManager.handleViewStateRestored(savedInstanceState);
	    }

	    @Override
	    public void onDestroyView() {
	        super.onDestroyView();
	        mTabManager.handleDestroyView();
	    }

	    @Override
	    public void onSaveInstanceState(Bundle outState) {
	        super.onSaveInstanceState(outState);
	        mTabManager.handleSaveInstanceState(outState);
	    }

	    @SuppressLint("NewApi")
		public static class TabManager implements TabHost.OnTabChangeListener {
	        private final Context mContext;
	        private final android.app.FragmentManager mManager;
	        private final int mContainerId;
	        private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();
	        private TabHost mTabHost;
	        private TabInfo mLastTab;
	        private boolean mInitialized;
	        private String mCurrentTabTag;

	        static final class TabInfo {
	            private final String tag;
	            private final Class<?> clss;
	            private final Bundle args;
	            private Fragment fragment;

	            TabInfo(String _tag, Class<?> _class, Bundle _args) {
	                tag = _tag;
	                clss = _class;
	                args = _args;
	            }
	        }

	        static class DummyTabFactory implements TabHost.TabContentFactory {
	            private final Context mContext;

	            public DummyTabFactory(Context context) {
	                mContext = context;
	            }

	            @Override
	            public View createTabContent(String tag) {
	                View v = new View(mContext);
	                v.setMinimumWidth(0);
	                v.setMinimumHeight(0);
	                return v;
	            }
	        }

	        public TabManager(Context context, android.app.FragmentManager fragmentManager, int containerId) {
	            mContext = context;
	            mManager = fragmentManager;
	            mContainerId = containerId;
	        }

	        public TabHost handleCreateView(View root) {
	            if (mTabHost != null) {
	                throw new IllegalStateException("TabHost already set");
	            }
	            mTabHost = (TabHost)root.findViewById(android.R.id.tabhost);
	            mTabHost.setup();
	            mTabHost.setOnTabChangedListener(this);
	            return mTabHost;
	        }

	        public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args) {
	            tabSpec.setContent(new DummyTabFactory(mContext));
	            String tag = tabSpec.getTag();
	            TabInfo info = new TabInfo(tag, clss, args);
	            mTabs.add(info);
	            mTabHost.addTab(tabSpec);
	        }

	        @SuppressLint("NewApi")
			public void handleViewStateRestored(Bundle savedInstanceState) {
	            if (savedInstanceState != null) {
	                mCurrentTabTag = savedInstanceState.getString("tab");
	            }
	            mTabHost.setCurrentTabByTag(mCurrentTabTag);

	            String currentTab = mTabHost.getCurrentTabTag();

	            // Go through all tabs and make sure their fragments match
	            // the correct state.
	            android.app.FragmentTransaction ft = null;
	            for (int i=0; i<mTabs.size(); i++) {
	                TabInfo tab = mTabs.get(i);
	                tab.fragment = mManager.findFragmentByTag(tab.tag);
	                if (tab.fragment != null && !tab.fragment.isDetached()) {
	                    if (tab.tag.equals(currentTab)) {
	                        // The fragment for this tab is already there and
	                        // active, and it is what we really want to have
	                        // as the current tab.  Nothing to do.
	                        mLastTab = tab;
	                    } else {
	                        // This fragment was restored in the active state,
	                        // but is not the current tab.  Deactivate it.
	                        if (ft == null) {
	                            ft = mManager.beginTransaction();
	                        }
	                        ft.detach(tab.fragment);
	                    }
	                }
	            }

	            // We are now ready to go.  Make sure we are switched to the
	            // correct tab.
	            mInitialized = true;
	            ft = doTabChanged(currentTab, ft);
	            if (ft != null) {
	                ft.commit();
	                mManager.executePendingTransactions();
	            }
	        }

	        public void handleDestroyView() {
	            mCurrentTabTag = mTabHost.getCurrentTabTag();
	            mTabHost = null;
	            mTabs.clear();
	            mInitialized = false;
	        }

	        public void handleSaveInstanceState(Bundle outState) {
	            outState.putString("tab", mTabHost != null
	                    ? mTabHost.getCurrentTabTag() : mCurrentTabTag);
	        }

	        @Override
	        public void onTabChanged(String tabId) {
	            if (!mInitialized) {
	                return;
	            }
	            android.app.FragmentTransaction ft = doTabChanged(tabId, null);
	            if (ft != null) {
	                ft.commit();
	            }
	        }

	        @SuppressLint("NewApi")
			private android.app.FragmentTransaction doTabChanged(String tabId, android.app.FragmentTransaction ft) {
	            TabInfo newTab = null;
	            for (int i=0; i<mTabs.size(); i++) {
	                TabInfo tab = mTabs.get(i);
	                if (tab.tag.equals(tabId)) {
	                    newTab = tab;
	                }
	            }
	            if (newTab == null) {
	                throw new IllegalStateException("No tab known for tag " + tabId);
	            }
	            if (mLastTab != newTab) {
	                if (ft == null) {
	                    ft = mManager.beginTransaction();
	                }
	                if (mLastTab != null) {
	                    if (mLastTab.fragment != null) {
	                        ft.detach(mLastTab.fragment);
	                    }
	                }
	                if (newTab != null) {
	                    if (newTab.fragment == null) {
	                        newTab.fragment = Fragment.instantiate(mContext,
	                                newTab.clss.getName(), newTab.args);
	                        ft.add(mContainerId, newTab.fragment, newTab.tag);
	                    } else {
	                        ft.attach(newTab.fragment);
	                    }
	                }

	                mLastTab = newTab;
	            }
	            return ft;
	        }
	    }
	}
