package reset_project_page;

import getinfoPage.LoginPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.hellochart.R;
import com.example.informationPage.MainActivity;
import com.loopj.android.http.JsonHttpResponseHandler;

import API_Funtion.ClassAPI;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class Fragment_ResetPage extends Fragment
{
	private ClassAPI API_Funtion = new ClassAPI(getActivity());

	private Intent info_intent = new Intent();

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return initView(inflater, container);
	}

	private View initView(LayoutInflater inflater, ViewGroup container)
	{
		View view = inflater.inflate(R.layout.fragment_logout, container, false);


		open_dialog();
		return view;
	}

	private void open_dialog()
	{
		AlertDialog.Builder ad = new AlertDialog.Builder(getActivity());
		ad.setTitle("重設計畫");
		ad.setMessage("確定要重設計畫嗎?");
		ad.setNegativeButton("否", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int i)
			{
				Intent intent = new Intent();
				intent.setClass(getActivity(), MainActivity.class);
				startActivity(intent);
			}
		});
		ad.setPositiveButton("是", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int i)
			{
				try
				{
					API_Funtion.reset_project(88122841, gettoken(), new JsonHttpResponseHandler()
					{

						@Override
						public void onSuccess(JSONObject response)
						{
							info_intent.setClass(getActivity(), LoginPage.class);
//							mToken = null;
//							getActivity().getDir(mTokenFileName, Context.MODE_PRIVATE);
//							FileOutputStream writer = null;
//							try
//							{
//								writer = getActivity().openFileOutput(mTokenFileName, Context.MODE_PRIVATE);
//								writer.write(response.getString("session_token").getBytes());
//								writer.close();
//							}
//							catch (IOException e)
//							{
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//							catch (JSONException e)
//							{
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
							clearApplicationData();
							startActivity(info_intent);
							getActivity().finish();
							// Toast.makeText(getActivity(),
							// response.getString("msg"),
							// Toast.LENGTH_LONG).show();
							super.onSuccess(response);
						}
					});
				}
				catch (UnsupportedEncodingException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		ad.show();
	}

	public void clearApplicationData()
	{
		File cache = getActivity().getCacheDir();
		File appDir = new File(cache.getParent());
		if (appDir.exists())
		{
			String[] children = appDir.list();
			for (String s : children)
			{
				if (!s.equals("lib"))
				{
					deleteDir(new File(appDir, s));
					// Log.i("TAG",
					// "**************** File /data/data/APP_PACKAGE/" + s +
					// " DELETED *******************");
				}
			}
		}
	}

	public static boolean deleteDir(File dir)
	{
		if (dir != null && dir.isDirectory())
		{
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++)
			{
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success)
				{
					return false;
				}
			}
		}

		return dir.delete();
	}

	private String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = getActivity().openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}
}
