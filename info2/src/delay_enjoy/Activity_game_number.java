package delay_enjoy;

import android.R.integer;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import index_page.Activity_index_page;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.example.hellochart.R;
import com.example.informationPage.MainActivity;

public class Activity_game_number extends Activity
{
	private Chronometer timer;
	private ImageButton mImageButton_start;
	/** Called when the activity is first created. */
	private GridView gridView;
	private int[] image = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14, 15, 16, 17, 18, 19,20, 21, 22, 23,24, 25 };
	// private String[] imgText = { "cat", "flower", "hippo", "monkey",
	// "mushroom", "panda", "rabbit",
	// "raccoon","123","123","123","123","123","123","123","123","123","123","123","123","123","123","123","123","123"
	// };

	private Integer[] imgtext = getRandomId(25);
	private ArrayList<Integer> marray = new ArrayList<Integer>();

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_number);
		
		final ActionBar bar = getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#3a5f8a"));
		bar.setBackgroundDrawable(actionbar_color);
		
		timer = (Chronometer)findViewById(R.id.chronometer1);
		mImageButton_start = (ImageButton)findViewById(R.id.imageButton1);
		gridView = (GridView) findViewById(R.id.mygridview);
		gridView.setNumColumns(5);
		gridView.setEnabled(false);
		
		mImageButton_start.setOnClickListener(new OnClickListener()
		{
			
			@SuppressLint("NewApi")
			@Override
			public void onClick(View v)
			{
				bar.setDisplayHomeAsUpEnabled(false);
				bar.setHomeButtonEnabled(false);
				timer.setBase(SystemClock.elapsedRealtime());
				timer.start();
				List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();
				for (int i = 0; i < image.length; i++)
				{
					Map<String, Object> item = new HashMap<String, Object>();
					item.put("image", image[i]);
					item.put("text", imgtext[i]);
					items.add(item);
				}
				mImageButton_start.setVisibility(View.INVISIBLE);
				gridView.setEnabled(true);
				SimpleAdapter adapter1 = new SimpleAdapter(Activity_game_number.this, items, R.layout.grid_item, new String[] { "text" }, new int[] { R.id.text });
				gridView.setAdapter(adapter1);
			}
		});
		
		for (int i = 0; i < imgtext.length; i++)
		{
			marray.add(i + 1);
		}

		List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < image.length; i++)
		{
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("image", image[i]);
			item.put("text", imgtext[i]);
			items.add(item);
		}
		final SimpleAdapter adapter = new SimpleAdapter(this, items, R.layout.grid_item, new String[] { "image" }, new int[] { R.id.text });
		
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(new OnItemClickListener()
		{

			@SuppressLint("NewApi")
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				// Toast.makeText(getApplicationContext(), "" + position,
				// Toast.LENGTH_SHORT).show();
				// Toast.makeText(getApplicationContext(), "Your choice is " +
				// imgtext[position], Toast.LENGTH_SHORT).show();

				if (marray.get(0) == imgtext[position])
				{
					TextView txt = (TextView) view.findViewById(R.id.text);
					txt.setTextColor(Color.BLUE);
					marray.remove(0);
//					imgtext[position] = null;
//					List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();
//					for (int i = 0; i < image.length; i++)
//					{
//						Map<String, Object> item = new HashMap<String, Object>();
//						item.put("image", image[i]);
//						item.put("text", imgtext[i]);
//						items.add(item);
//					}
//					SimpleAdapter adapter1 = new SimpleAdapter(getApplicationContext(), items, R.layout.grid_item, new String[] { "text" }, new int[] { R.id.text });
//					gridView = (GridView) findViewById(R.id.mygridview);
//					gridView.setNumColumns(5);
//					gridView.setAdapter(adapter1);

				}
				if (marray.size() == 0)
				{
					for (int i = 0; i < imgtext.length; i++)
					{
						marray.add(i + 1);
					}
					gridView.setEnabled(false);
					mImageButton_start.setVisibility(View.VISIBLE);
					timer.stop();
					gridView.setAdapter(adapter);
					Toast.makeText(getApplicationContext(), "Very Good! 花了 "+timer.getText().toString()+" 完成", Toast.LENGTH_LONG).show();
					bar.setDisplayHomeAsUpEnabled(true);
					bar.setHomeButtonEnabled(true);
//					 Thread thread = new Thread(){
//					 @Override
//					 public void run(){
//					 try {
//					 synchronized(this){
//					 wait(3000);
//					 finish();
//					 }
//					 }
//					 catch(InterruptedException ex){
//					 }
//					
//					 // TODO
//					 }
//					 };
//					
//					 thread.start();
				}

			}

		});
	}
	
	  
	public static Integer[] getRandomId(int n)
	{
		Integer[] arryRandom = new Integer[n];
		for (int i = 0; i < n; i++)
			arryRandom[i] = i + 1;
		List list = Arrays.asList(arryRandom);
		Collections.shuffle(list);
		return arryRandom;
	}
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			onBackPressed();
		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	public void onBackPressed() {
	    // Do nothing
	    return;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{    
	   switch (item.getItemId()) 
	   {        
	      case android.R.id.home:
	    	  Intent intent = new Intent();
	    	  intent.setClass(Activity_game_number.this, Activity_index_page.class);
	    	  startActivity(intent);
	         Activity_game_number.this.finish();
	         return true;        
	      default:            
	         return super.onOptionsItemSelected(item);    
	   }
	}
}
