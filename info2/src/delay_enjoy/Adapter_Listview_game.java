package delay_enjoy;

import com.example.hellochart.R;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Adapter_Listview_game extends BaseAdapter
{
	private TextView mTextView_title;
	private TextView mTextView_about;
	private ImageView mButton_close;
	private LayoutInflater myInflater;
	private String[] names = new String[] { "指數字", "健康小問題", "卡路里估算" };
	Context contect;

	public Adapter_Listview_game(Context c)
	{
		contect = c;
		myInflater = LayoutInflater.from(c);
	}

	@Override
	public int getCount()
	{
		// TODO Auto-generated method stub
		return names.length;
	}

	@Override
	public Object getItem(int position)
	{
		// TODO Auto-generated method stub
		return names[position];
	}

	@Override
	public long getItemId(int position)
	{
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		// TODO Auto-generated method stub
		convertView = myInflater.inflate(R.layout.listview_game, null);

		// ImageView logo = (ImageView) convertView.findViewById(R.id.imglogo);
		TextView name = (TextView) convertView.findViewById(R.id.textView_list_game);
		ImageView about = (ImageView) convertView.findViewById(R.id.imageView_about);
		// TextView list = (TextView) convertView.findViewById(R.id.txtengname);

		// logo.setImageResource(logos[position]);
		name.setText(names[position]);
		// list.setText(lists[position]);
		if (position == 0)
		{
			about.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					final Dialog dialog = new Dialog(contect, R.style.dialog);
					View myView = LayoutInflater.from(contect).inflate(R.layout.dialog_game_about, null);
					dialog.setContentView(myView);
					mButton_close = (ImageView)myView.findViewById(R.id.dialog_process_cancel);
					//mTextView_title = (TextView)myView.findViewById(R.id.textView_game_title);
					//mTextView_about = (TextView)myView.findViewById(R.id.textView_game_about);
					
					mTextView_title.setText("指數字");
					mTextView_about.setText("請從版面上從 1 指到 25 !\n※此遊戲能訓練集中力");
					mButton_close.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							dialog.cancel();

						}
					});
					dialog.show();
				}
			});
		}
		else if (position == 1)
		{
			about.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					final Dialog dialog = new Dialog(contect, R.style.dialog);
					View myView = LayoutInflater.from(contect).inflate(R.layout.dialog_game_about, null);
					dialog.setContentView(myView);
					mButton_close = (ImageView)myView.findViewById(R.id.dialog_process_cancel);
					//mTextView_title = (TextView)myView.findViewById(R.id.textView_game_title);
					//mTextView_about = (TextView)myView.findViewById(R.id.textView_game_about);
					
					mTextView_title.setText("健康小問題");
					mTextView_about.setText("回答健康相關問題!\n※此遊戲能加強認知");
					mButton_close.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							dialog.cancel();

						}
					});
					dialog.show();
				}
			});
		}
		else if (position == 2)
		{
			about.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					final Dialog dialog = new Dialog(contect, R.style.dialog);
					View myView = LayoutInflater.from(contect).inflate(R.layout.dialog_game_about, null);
					dialog.setContentView(myView);
					mButton_close = (ImageView)myView.findViewById(R.id.dialog_process_cancel);
					//mTextView_title = (TextView)myView.findViewById(R.id.textView_game_title);
					//mTextView_about = (TextView)myView.findViewById(R.id.textView_game_about);
					
					mTextView_title.setText("卡路里估算");
					mTextView_about.setText("挑選出熱量最低的食物 !\n※此遊戲能強化認知");
					mButton_close.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							dialog.cancel();

						}
					});
					dialog.show();

				}
			});
		}

		return convertView;
	}

}
