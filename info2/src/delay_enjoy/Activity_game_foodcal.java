package delay_enjoy;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.example.hellochart.R;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable.Callback;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Activity_game_foodcal extends Activity
{
	private int tmp = 0;
	private long min;
	private long sec;
	DecimalFormat nf = new DecimalFormat("00");
	private TextView timer;
	private TextView process;
	private TextView answer;
	private TextView txt_top;
	private TextView txt_down;
	private ImageView top;
	private ImageView down;
	private ArrayList<Integer> array = new ArrayList<Integer>();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_foodcal);

		ActionBar bar = getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#3a5f8a"));
		bar.setBackgroundDrawable(actionbar_color);

		timer = (TextView) findViewById(R.id.textView_foodgame_time);
		process = (TextView) findViewById(R.id.textView_foodgame_process);
		answer = (TextView) findViewById(R.id.textView_foodgame_bingo);
		txt_top = (TextView) findViewById(R.id.textView_top);
		txt_down = (TextView) findViewById(R.id.textView_down);
		top = (ImageView) findViewById(R.id.imageView_top);
		down = (ImageView) findViewById(R.id.imageView_down);

		for (int i = 1; i <= 5; i++)
		{
			array.add(i);
		}
		final CountDownTimer mtimer = new CountDownTimer(20000, 1000)
		{
			@Override
			public void onTick(long millisUntilFinished)
			{
				min = millisUntilFinished / 60000;
				sec = millisUntilFinished % 60000 / 1000;
				timer.setText(nf.format(min) + " : " + nf.format(sec));
			}

			@SuppressLint("NewApi")
			@Override
			public void onFinish()
			{
				if (array.get(0) == 1)
				{
					process.setText("2/5");
					top.setImageResource(R.drawable.doublebeef);
					down.setImageResource(R.drawable.bancowji);
					txt_top.setText(" 雙層牛肉吉士堡");
					txt_down.setText(" 板烤雞腿堡");
					array.remove(0);
					start();
				}
				else if (array.get(0) == 2)
				{
					process.setText("3/5");
					top.setImageResource(R.drawable.milktea);
					down.setImageResource(R.drawable.blacktea);
					txt_top.setText(" 奶茶");
					txt_down.setText(" 紅茶");
					array.remove(0);
					start();
				}
				else if (array.get(0) == 3)
				{
					process.setText("4/5");
					top.setImageResource(R.drawable.fishegg);
					down.setImageResource(R.drawable.miegg);
					txt_top.setText(" 鮪魚蛋餅");
					txt_down.setText(" 玉米蛋餅");
					array.remove(0);
					start();
				}
				else if (array.get(0) == 4)
				{
					process.setText("5/5");
					top.setImageResource(R.drawable.frise);
					down.setImageResource(R.drawable.chicken_six);
					txt_top.setText(" 中包薯條");
					txt_down.setText(" 六塊小雞塊");
					array.remove(0);
					start();
				}
				else if (array.get(0) == 5)
				{
					process.setText("完成");
					timer.setText("00 : 00");
					array.remove(0);
					top.setEnabled(false);
					down.setEnabled(false);
					opendialog(tmp);
				}
			}
		};
		mtimer.start();

		top.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (array.get(0) == 1)
				{
					tmp++;
					answer.setText("" + tmp);
					process.setText("2/5");
					top.setImageResource(R.drawable.doublebeef);
					down.setImageResource(R.drawable.bancowji);
					txt_top.setText(" 雙層牛肉吉士堡");
					txt_down.setText(" 板烤雞腿堡");
					mtimer.cancel();
					mtimer.start();
					array.remove(0);
				}
				else if (array.get(0) == 2)
				{
					process.setText("3/5");
					top.setImageResource(R.drawable.milktea);
					down.setImageResource(R.drawable.blacktea);
					txt_top.setText(" 奶茶");
					txt_down.setText(" 紅茶");
					mtimer.cancel();
					mtimer.start();
					array.remove(0);
				}
				else if (array.get(0) == 3)
				{
					process.setText("4/5");
					top.setImageResource(R.drawable.fishegg);
					down.setImageResource(R.drawable.miegg);
					txt_top.setText(" 鮪魚蛋餅");
					txt_down.setText(" 玉米蛋餅");
					mtimer.cancel();
					mtimer.start();
					array.remove(0);
				}
				else if (array.get(0) == 4)
				{
					process.setText("5/5");
					top.setImageResource(R.drawable.frise);
					down.setImageResource(R.drawable.chicken_six);
					txt_top.setText(" 中包薯條");
					txt_down.setText(" 六塊小雞塊");
					mtimer.cancel();
					mtimer.start();
					array.remove(0);
				}
				else if (array.get(0) == 5)
				{
					tmp++;
					process.setText("完成");
					answer.setText("" + tmp);
					mtimer.cancel();
					timer.setText("00 : 00");
					array.remove(0);
					top.setEnabled(false);
					down.setEnabled(false);
					opendialog(tmp);
				}
			}
		});

		down.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (array.get(0) == 1)
				{
					// tmp++;
					// answer.setText(""+tmp);
					process.setText("2/5");
					top.setImageResource(R.drawable.doublebeef);
					down.setImageResource(R.drawable.bancowji);
					txt_top.setText(" 雙層牛肉吉士堡");
					txt_down.setText(" 板烤雞腿堡");
					mtimer.cancel();
					mtimer.start();
					array.remove(0);
				}
				else if (array.get(0) == 2)
				{
					tmp++;
					answer.setText("" + tmp);
					process.setText("3/5");
					top.setImageResource(R.drawable.milktea);
					down.setImageResource(R.drawable.blacktea);
					txt_top.setText(" 奶茶");
					txt_down.setText(" 紅茶");
					mtimer.cancel();
					mtimer.start();
					array.remove(0);
				}
				else if (array.get(0) == 3)
				{
					tmp++;
					answer.setText("" + tmp);
					process.setText("4/5");
					top.setImageResource(R.drawable.fishegg);
					down.setImageResource(R.drawable.miegg);
					txt_top.setText(" 鮪魚蛋餅");
					txt_down.setText(" 玉米蛋餅");
					mtimer.cancel();
					mtimer.start();
					array.remove(0);
				}
				else if (array.get(0) == 4)
				{
					tmp++;
					answer.setText("" + tmp);
					process.setText("5/5");
					top.setImageResource(R.drawable.frise);
					down.setImageResource(R.drawable.chicken_six);
					txt_top.setText(" 中包薯條");
					txt_down.setText(" 六塊小雞塊");
					mtimer.cancel();
					mtimer.start();
					array.remove(0);
				}
				else if (array.get(0) == 5)
				{
					// tmp++;
					// process.setText("完成");
					answer.setText("" + tmp);
					mtimer.cancel();
					timer.setText("00 : 00");
					array.remove(0);
					top.setEnabled(false);
					down.setEnabled(false);
					opendialog(tmp);
				}
			}
		});
	}
	
	private void opendialog(int num)
	{
		final Dialog dialog = new Dialog(Activity_game_foodcal.this, R.style.dialog);
		View myView = LayoutInflater.from(this).inflate(R.layout.dialog_game_question_final, null);
		dialog.setContentView(myView);
		ImageView btn_close = (ImageView)myView.findViewById(R.id.dialog_cancle_game_quese);
		Button btn_re = (Button)myView.findViewById(R.id.button_regame);
		Button btn_back = (Button)myView.findViewById(R.id.button_reback);
		TextView bingonum = (TextView)myView.findViewById(R.id.textView_game_bingo_num);
		bingonum.setText("總共答對 "+num+" 題!");
		
		btn_close.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				dialog.cancel();
				
			}
		});
		
		btn_re.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent();
				intent.setClass(Activity_game_foodcal.this, Activity_game_foodcal.class);
				startActivity(intent);
				Activity_game_foodcal.this.finish();
			}
		});
		
		btn_back.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				Activity_game_foodcal.this.finish();
			}
		});
		dialog.show();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			onBackPressed();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onBackPressed()
	{
		// Do nothing
		return;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case android.R.id.home:
				Intent intent = new Intent(this, Activity_delay_enjoy.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				Activity_game_foodcal.this.finish();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
