package delay_enjoy;

import index_page.Fragment_index_page;

import java.text.DecimalFormat;

import com.example.hellochart.R;
import com.example.informationPage.Activity_MealsPage;
import com.example.informationPage.MainActivity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class Activity_delay_enjoy extends Activity
{
	private ListView mListView_game;
	private Button btn_start;
	private Button btn_reset;
	private TextView timer;
	private long min;
	private long sec;
	DecimalFormat nf = new DecimalFormat("00");
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.delay_enjoy);
		final ActionBar bar = getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#df3a5f8a"));
		bar.setBackgroundDrawable(actionbar_color);
		timer = (TextView)findViewById(R.id.text_timer);
		btn_start = (Button)findViewById(R.id.btn_start);
		btn_reset = (Button)findViewById(R.id.btn_reset);
		mListView_game = (ListView)findViewById(R.id.listView_game);
		
		Adapter_Listview_game adapter = new Adapter_Listview_game(Activity_delay_enjoy.this);
		mListView_game.setAdapter(adapter);
		mListView_game.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				if(position==0)
				{
					Intent intent = new Intent();
					intent.setClass(Activity_delay_enjoy.this, Activity_game_number.class);
					startActivity(intent);
				}
				else if(position==1)
				{
					Intent intent = new Intent();
					intent.setClass(Activity_delay_enjoy.this, Activity_game_question.class);
					startActivity(intent);
				}
				else if(position==2)
				{
					Intent intent = new Intent();
					intent.setClass(Activity_delay_enjoy.this, Activity_game_foodcal.class);
					startActivity(intent);
				}
			}
		});
		
		final CountDownTimer mtimer =new CountDownTimer(180000, 1000){

			@Override
			public void onTick(long millisUntilFinished) {
				min = millisUntilFinished / 60000;
				sec = millisUntilFinished % 60000 /1000;
				timer.setText(nf.format(min)+" : "+nf.format(sec));
			}

			@SuppressLint("NewApi")
			@Override
			public void onFinish() {
				timer.setText("成功完成,開動!");
				bar.setDisplayHomeAsUpEnabled(true);
				bar.setHomeButtonEnabled(true);
				btn_start.setClickable(true);
				showNotificaltion();		
			}	
			
		};
		
		btn_start.setOnClickListener(new OnClickListener() {
			
			@SuppressLint("NewApi")
			public void onClick(View v) {
				mtimer.start();
				btn_start.setClickable(false);
				bar.setDisplayHomeAsUpEnabled(false);
				bar.setHomeButtonEnabled(false);
			}
		});
		
		btn_reset.setOnClickListener(new OnClickListener() {
			
			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				mtimer.cancel();
				btn_start.setClickable(true);
				bar.setDisplayHomeAsUpEnabled(true);
				bar.setHomeButtonEnabled(true);
				timer.setText("請按開始鍵");
			}
		});
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			onBackPressed();
		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	public void onBackPressed() {
	    // Do nothing
	    return;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{    
	   switch (item.getItemId()) 
	   {        
	      case android.R.id.home:            
	         Intent intent = new Intent(this, MainActivity.class);            
	         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
	         startActivity(intent);
	         Activity_delay_enjoy.this.finish();
	         return true;        
	      default:            
	         return super.onOptionsItemSelected(item);    
	   }
	}
	
	@SuppressLint("NewApi")
	protected void showNotificaltion(){
		NotificationManager noti = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		//Notification msg = new Notification(R.drawable.ic_launcher,"aaa",System.currentTimeMillis());
		
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,Activity_delay_enjoy.class), PendingIntent.FLAG_UPDATE_CURRENT);
		Notification.Builder msg = new Notification.Builder(this)
		.setTicker("延遲享受  Time's up!")
		.setContentTitle("恭喜完成延遲享受")
		.setContentText("Very Good!")
		.setSmallIcon(R.drawable.ic_launcher)
		.setContentIntent(contentIntent);
		noti.notify(0,msg.build());
		
		//msg.setLatestEventInfo(MainActivity.this, "123", "321", contentIntent);
		//noti.notify(0,msg);
	}
	
}
