package delay_enjoy;

import java.util.ArrayList;

import com.example.hellochart.R;
import com.example.informationPage.MainActivity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class Activity_game_question extends Activity
{
	private TextView mTextView_question;
	private TextView mTextView_process;
	private TextView mTextView_bingo;
	private Button mButton_true;
	private Button mButton_false;

	private int tmp = 0;

	private ArrayList<Integer> process = new ArrayList<Integer>();
	private ArrayList<Boolean> answer = new ArrayList<Boolean>();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_question);
		
		ActionBar bar = getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#3a5f8a"));
		bar.setBackgroundDrawable(actionbar_color);
		
		mTextView_question = (TextView) findViewById(R.id.textView_question);
		mTextView_process = (TextView) findViewById(R.id.textView_process);
		mTextView_bingo = (TextView) findViewById(R.id.textView_bingo);
		mButton_true = (Button) findViewById(R.id.button_true);
		mButton_false = (Button) findViewById(R.id.button_false);
		for (int i = 1; i <= 10; i++)
		{
			process.add(i);
		}
		mButton_true.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (process.get(0) == 1)
				{
					tmp++;
					mTextView_question.setText(R.string.q2);
					mTextView_process.setText("2/10");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
				}
				else if(process.get(0)==2)
				{
					tmp++;
					mTextView_question.setText(R.string.q3);
					mTextView_process.setText("3/10");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
				}
				else if(process.get(0)==3)
				{
					mTextView_question.setText(R.string.q4);
					mTextView_process.setText("4/10");
					process.remove(0);
				}
				else if(process.get(0)==4)
				{
					mTextView_question.setText(R.string.q5);
					mTextView_process.setText("5/10");
					process.remove(0);
				}
				else if(process.get(0)==5)
				{
					tmp++;
					mTextView_question.setText(R.string.q6);
					mTextView_process.setText("6/10");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
				}
				else if(process.get(0)==6)
				{
					tmp++;
					mTextView_question.setText(R.string.q7);
					mTextView_process.setText("7/10");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
				}
				else if(process.get(0)==7)
				{
					tmp++;
					mTextView_question.setText(R.string.q8);
					mTextView_process.setText("8/10");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
				}
				else if(process.get(0)==8)
				{
					tmp++;
					mTextView_question.setText(R.string.q9);
					mTextView_process.setText("9/10");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
				}
				else if(process.get(0)==9)
				{
					tmp++;
					mTextView_question.setText(R.string.q10);
					mTextView_process.setText("10/10");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
				}
				else if(process.get(0)==10)
				{
					tmp++;
					mTextView_question.setText("恭喜全部作答完畢");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
					mButton_true.setEnabled(false);
					mButton_false.setEnabled(false);
					opendialog(tmp);
				}
			}
		});
		
		mButton_false.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if (process.get(0) == 1)
				{
					mTextView_question.setText(R.string.q2);
					mTextView_process.setText("2/10");
					process.remove(0);
				}
				else if(process.get(0)==2)
				{
					mTextView_question.setText(R.string.q3);
					mTextView_process.setText("3/10");
					process.remove(0);
				}
				else if(process.get(0)==3)
				{
					tmp++;
					mTextView_question.setText(R.string.q4);
					mTextView_process.setText("4/10");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
				}
				else if(process.get(0)==4)
				{
					tmp++;
					mTextView_question.setText(R.string.q5);
					mTextView_process.setText("5/10");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
				}
				else if(process.get(0)==5)
				{
					//tmp++;
					mTextView_question.setText(R.string.q6);
					mTextView_process.setText("6/10");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
				}
				else if(process.get(0)==6)
				{
					//tmp++;
					mTextView_question.setText(R.string.q7);
					mTextView_process.setText("7/10");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
				}
				else if(process.get(0)==7)
				{
					//tmp++;
					mTextView_question.setText(R.string.q8);
					mTextView_process.setText("8/10");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
				}
				else if(process.get(0)==8)
				{
					//tmp++;
					mTextView_question.setText(R.string.q9);
					mTextView_process.setText("9/10");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
				}
				else if(process.get(0)==9)
				{
					//tmp++;
					mTextView_question.setText(R.string.q10);
					mTextView_process.setText("10/10");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
				}
				else if(process.get(0)==10)
				{
					//tmp++;
					mTextView_question.setText("恭喜全部作答完畢");
					mTextView_bingo.setText(""+tmp);
					process.remove(0);
					mButton_true.setEnabled(false);
					mButton_false.setEnabled(false);
					opendialog(tmp);
				}
			}
		});

	}
	
	private void opendialog(int num)
	{
		final Dialog dialog = new Dialog(Activity_game_question.this, R.style.dialog);
		View myView = LayoutInflater.from(this).inflate(R.layout.dialog_game_question_final, null);
		dialog.setContentView(myView);
		ImageView btn_close = (ImageView)myView.findViewById(R.id.dialog_cancle_game_quese);
		Button btn_re = (Button)myView.findViewById(R.id.button_regame);
		Button btn_back = (Button)myView.findViewById(R.id.button_reback);
		TextView bingonum = (TextView)myView.findViewById(R.id.textView_game_bingo_num);
		bingonum.setText("總共答對 "+num+" 題!");
		
		btn_close.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				dialog.cancel();
				
			}
		});
		
		btn_re.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent();
				intent.setClass(Activity_game_question.this, Activity_game_question.class);
				startActivity(intent);
				Activity_game_question.this.finish();
			}
		});
		
		btn_back.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				Activity_game_question.this.finish();
			}
		});
		dialog.show();
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			onBackPressed();
		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	public void onBackPressed() {
	    // Do nothing
	    return;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{    
	   switch (item.getItemId()) 
	   {        
	      case android.R.id.home:            
	         Intent intent = new Intent(this, Activity_delay_enjoy.class);            
	         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
	         startActivity(intent);
	         Activity_game_question.this.finish();
	         return true;        
	      default:            
	         return super.onOptionsItemSelected(item);    
	   }
	}

}
