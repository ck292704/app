package com.example.informationPage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

import org.json.JSONException;
import org.json.JSONObject;

import mission.Activity_Mission;
import index_page.Activity_index_page;

import com.example.hellochart.R;
import com.example.informationPage.Activity_MealsPage;
import com.loopj.android.http.JsonHttpResponseHandler;

import API_Funtion.ClassAPI;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewSwitcher.ViewFactory;

public class Activity_DelayEnjoy_selectfood extends Activity
{
	public String mMeal_number = null; // 哪一餐的代號
	private ClassAPI API_Funtion = new ClassAPI(Activity_DelayEnjoy_selectfood.this);
	int[] mpic = { R.drawable.ifit_pic1, R.drawable.ifit_pic2n, R.drawable.ifit_pic3, R.drawable.ifit_pic4 };
	private ImageSwitcher mImageSwitcherShowPic;
	// add image
	private Button mButtonChangePic;
	int pic = 0;
	int index;
	int timecount = 0;
	DecimalFormat nf = new DecimalFormat("00");
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_delayenjoy_v2);
		final ActionBar bar = getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#ef3a5f8a"));
		bar.setBackgroundDrawable(actionbar_color);
		getbunlde();
		mImageSwitcherShowPic = (ImageSwitcher) findViewById(R.id.imageSwitcher1);
		mButtonChangePic = (Button) findViewById(R.id.btn_yes);
		mImageSwitcherShowPic.setFactory(new ViewFactory()
		{
			@Override
			public View makeView()
			{
				// TODO Auto-generated method stub
				return new ImageView(Activity_DelayEnjoy_selectfood.this);
			}

		});
		mImageSwitcherShowPic.setImageResource(mpic[index]);

		mButtonChangePic.setOnClickListener(new OnClickListener()
		{

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				bar.setDisplayHomeAsUpEnabled(false);
				bar.setHomeButtonEnabled(false);
				mButtonChangePic.setEnabled(false);
				new CountDownTimer(300000, 1000)
				{

					@Override
					public void onTick(long millisUntilFinished)
					{
						// TODO Auto-generated method stub
						timecount++;
						mButtonChangePic.setText(""+nf.format(millisUntilFinished/60000)+"："+nf.format(millisUntilFinished%60000/1000));
						if (timecount % 10 == 5)
						{
							Log.e("++++", "OK");
							index++;
							if (index >= mpic.length)
							{
								index = 0;
							}
							mImageSwitcherShowPic.setImageResource(mpic[index]);
						}
					}

					@Override
					public void onFinish()
					{
						// TODO Auto-generated method stub
						mButtonChangePic.setText("You can enjoy your food");
						//bar.setDisplayHomeAsUpEnabled(true);
						//bar.setHomeButtonEnabled(true);
						try
						{
							API_Funtion.mission_finish(88122841, gettoken(), API_Funtion.GetTimestamp(), 3, new JsonHttpResponseHandler()
							{

								@Override
								public void onSuccess(JSONObject response)
								{
									super.onSuccess(response);
								}

							});
						}
						catch (UnsupportedEncodingException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						catch (JSONException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Bundle bundle = new Bundle();
						Intent intent = new Intent();
						intent.setClass(Activity_DelayEnjoy_selectfood.this, Activity_selectFoodPage.class);
						bundle.putString("meal", mMeal_number);
						intent.putExtras(bundle);
						startActivity(intent);
						finish();
					}
				}.start();
			}
		});
		mImageSwitcherShowPic.setInAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left));
		mImageSwitcherShowPic.setOutAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right));

	}
	
	private void getbunlde()
	{
		Bundle bundle = this.getIntent().getExtras();
		mMeal_number = bundle.getString("meal");
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
//			Intent intent = new Intent();
//			intent.setClass(Activity_DelayEnjoy_selectfood.this, Activity_MealsPage.class);
//			startActivity(intent);
//			finish();
			onBackPressed();
		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	public void onBackPressed() {
	    // Do nothing
	    return;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case android.R.id.home:
				Intent intent = new Intent(this, Activity_MealsPage.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				Activity_DelayEnjoy_selectfood.this.finish();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = Activity_DelayEnjoy_selectfood.this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}

}
