package com.example.informationPage;

import index_page.Activity_index_page;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectOutputStream.PutField;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sportPage.Sportpage;

import com.example.hellochart.R;
import com.example.informationPage.TableAdapter.TableCell;
import com.example.informationPage.TableAdapter.TableRow;
import com.loopj.android.http.JsonHttpResponseHandler;

import API_Funtion.ClassAPI;
import android.R.integer;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.View.OnClickListener;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AbsListView.OnScrollListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TabHost.TabSpec;

public class Activity_selectFoodPage extends Activity
{
	public String mMeal_number = null; // 哪一餐的代號
	public ArrayList<Integer> mList_tmp = new ArrayList<Integer>();
	public int mCalValue = 0;
	private ClassAPI API_Funtion = new ClassAPI(Activity_selectFoodPage.this);
	private TextView mCal_Value;
	private TextView mFoodname;
	private Button mButton_portion_0;
	private Button mButton_portion_1;
	private Button mButton_portion_2;
	private Button mButton_portion_3;
	private Button mButton_portion_4;
	private Button mButton_portion_5;
	private Button mButton_portion_6;
	private Button mButton_portion_7;
	private Button mButton_portion_8;
	private Button mButton_portion_9;
	private Button mButton_portion_10;
	private Button mButton_portion_11;
	private Button mButton_portion_12;
	private Button mButton_portion_13;
	private Button mButton_portion_14;
	private Button mButton_portion_15;
	private Button mButton_portion_16;
	private Button mButton_portion_17;
	private Button mButton_portion_18;
	private Button mButton_portion_19;
	private Button mButton_portion_20;
	private Button mButton_portion_21;
	private Button mButton_portion_22;
	private Button mButton_portion_23;
	private Button mButton_portion_24;
	private Button mButton_portion_25;
	private Button mButton_portion_26;
	public QuickReturnListView mLv;
	public ListView mList_addfood;
	public ArrayAdapter<String> mAdapter;
	public ArrayList<String> mItems;
	public ArrayList<Integer> mItems_id = new ArrayList<Integer>();
	public ArrayList<Integer> mItems_portion = new ArrayList<Integer>();
	public HashMap<String, String> map1 = new HashMap<String, String>();
	public ArrayList<HashMap<String, String>> mList0 = new ArrayList<HashMap<String, String>>();

	private int mResultValue = 1;
	private Button mBtn_add;
	private Button mBtn_sub;
	private EditText mEdit_value;

	private LinearLayout mQuickReturnView;
	private int mQuickReturnHeight;
	private static final int STATE_ONSCREEN = 0;
	private static final int STATE_OFFSCREEN = 1;
	private static final int STATE_RETURNING = 2;
	private int mState = STATE_ONSCREEN;
	private int mScrollY;
	private int mMinRawY = 0;
	private TranslateAnimation anim;
	
	private ArrayList<String> array_name = new ArrayList<String>();
	private ArrayList<String> array_id = new ArrayList<String>();
	private ArrayList<String> array_cal = new ArrayList<String>();
	// public String[] food_id;
	// public String[] name;
	// public String[] count;
	// public String[] cal;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_food_page);
		//launchRingDialog();
		ActionBar bar = getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#3a5f8a"));
		bar.setBackgroundDrawable(actionbar_color);
		getbunlde(); // 取得是哪一餐的代號
		init();
		
		// --------// 進入抓取當天所選取的食物烈表
				try
				{
					API_Funtion.food_show(88122841, gettoken(), API_Funtion.GetTimestamp(), new JsonHttpResponseHandler()
					{

						@Override
						public void onSuccess(JSONObject response)
						{

							if (mMeal_number.equals("0"))
							{
								try
								{
									Log.e("*******breakfast", "break");
									for (int i = 0; i < response.getJSONArray("breakfast").length(); i++)
									{
										int foodid = response.getJSONArray("breakfast").getJSONObject(i).getInt("food_id");
										String foodname = response.getJSONArray("breakfast").getJSONObject(i).getString("food_name");
										int food_portion = response.getJSONArray("breakfast").getJSONObject(i).getInt("meal_portion");
										int food_cal = response.getJSONArray("breakfast").getJSONObject(i).getInt("food_cal");
										mItems_id.add(foodid);
										mItems_portion.add(food_portion);
										mList_tmp.add(food_cal * food_portion);
										mItems.add(foodname + "   " + food_portion + "份" + "   " + food_portion * food_cal + "卡");
										mCalValue += food_portion * food_cal;
										Log.e("test", "" + mCalValue);
									}
									// mAdapter = new
									// ArrayAdapter(Activity_selectFoodPage.this,
									// android.R.layout.simple_list_item_1, mItems);
									if(mCalValue!=0)
									{
										mList_addfood.setOnItemClickListener(new Itemdelete());
									}
									mCal_Value.setText("卡路里總和：" + mCalValue);
									mList_addfood.setAdapter(mAdapter);

								}
								catch (JSONException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							else if (mMeal_number.equals("1"))
							{
								try
								{
									for (int i = 0; i < response.getJSONArray("lunch").length(); i++)
									{
										int foodid = response.getJSONArray("lunch").getJSONObject(i).getInt("food_id");
										String foodname = response.getJSONArray("lunch").getJSONObject(i).getString("food_name");
										int food_portion = response.getJSONArray("lunch").getJSONObject(i).getInt("meal_portion");
										int food_cal = response.getJSONArray("lunch").getJSONObject(i).getInt("food_cal");
										mItems_id.add(foodid);
										mItems_portion.add(food_portion);
										mList_tmp.add(food_cal * food_portion);
										mItems.add(foodname + "   " + food_portion + "份" + "   " + food_portion * food_cal + "卡");
										mCalValue += food_portion * food_cal;
										Log.e("test", "" + mCalValue);
									}
									// mAdapter = new
									// ArrayAdapter(Activity_selectFoodPage.this,
									// android.R.layout.simple_list_item_1, mItems);
									if(mCalValue!=0)
									{
										mList_addfood.setOnItemClickListener(new Itemdelete());
									}
									mCal_Value.setText("卡路里總和：" + mCalValue);
									mList_addfood.setAdapter(mAdapter);

								}
								catch (JSONException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							else if (mMeal_number.equals("2"))
							{
								try
								{
									for (int i = 0; i < response.getJSONArray("dinner").length(); i++)
									{
										int foodid = response.getJSONArray("dinner").getJSONObject(i).getInt("food_id");
										String foodname = response.getJSONArray("dinner").getJSONObject(i).getString("food_name");
										int food_portion = response.getJSONArray("dinner").getJSONObject(i).getInt("meal_portion");
										int food_cal = response.getJSONArray("dinner").getJSONObject(i).getInt("food_cal");
										mItems_id.add(foodid);
										mItems_portion.add(food_portion);
										mList_tmp.add(food_cal * food_portion);
										mItems.add(foodname + "   " + food_portion + "份" + "   " + food_portion * food_cal + "卡");
										mCalValue += food_portion * food_cal;
										Log.e("test", "" + mCalValue);
									}
									// mAdapter = new
									// ArrayAdapter(Activity_selectFoodPage.this,
									// android.R.layout.simple_list_item_1, mItems);
									if(mCalValue!=0)
									{
										mList_addfood.setOnItemClickListener(new Itemdelete());
									}
									mCal_Value.setText("卡路里總和：" + mCalValue);
									mList_addfood.setAdapter(mAdapter);

								}
								catch (JSONException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							else
							{
								try
								{
									for (int i = 0; i < response.getJSONArray("other").length(); i++)
									{
										int foodid = response.getJSONArray("other").getJSONObject(i).getInt("food_id");
										String foodname = response.getJSONArray("other").getJSONObject(i).getString("food_name");
										int food_portion = response.getJSONArray("other").getJSONObject(i).getInt("meal_portion");
										int food_cal = response.getJSONArray("other").getJSONObject(i).getInt("food_cal");
										mItems_id.add(foodid);
										mItems_portion.add(food_portion);
										mList_tmp.add(food_cal * food_portion);
										mItems.add(foodname + "   " + food_portion + "份" + "   " + food_portion * food_cal + "卡");
										mCalValue += food_portion * food_cal;
										Log.e("test", "" + mCalValue);
									}
									// mAdapter = new
									// ArrayAdapter(Activity_selectFoodPage.this,
									// android.R.layout.simple_list_item_1, mItems);
									if(mCalValue!=0)
									{
										mList_addfood.setOnItemClickListener(new Itemdelete());
									}
									mCal_Value.setText("卡路里總和：" + mCalValue);
									mList_addfood.setAdapter(mAdapter);

								}
								catch (JSONException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							super.onSuccess(response);
						}
					});
				}
				catch (UnsupportedEncodingException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// --------//
		onclickListener();
		tableList(0);
		//launchRingDialog();

		mButton_portion_0.setBackgroundResource(R.drawable.shape3);
		//glob();
	}

	private void getbunlde()
	{
		Bundle bundle = this.getIntent().getExtras();
		mMeal_number = bundle.getString("meal");
	}

	private void tableList(int partion)
	{
		final ProgressDialog ringProgressDialog = ProgressDialog.show(Activity_selectFoodPage.this, "Please wait ...", "Data Loading ...", true);
		ringProgressDialog.setCancelable(true);
		final ArrayList<TableRow> table = new ArrayList<TableRow>();
		final TableCell[] titles = new TableCell[3];// 每行3個單元
		final int width = this.getWindowManager().getDefaultDisplay().getWidth() / titles.length;
		try
		{
			API_Funtion.food_partition(88122841, gettoken(), partion, new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					// Toast.makeText(Activity_selectFoodPage.this, "qq",
					// Toast.LENGTH_LONG).show();
					try
					{
						String[] food_id = new String[response.getJSONArray("foodlist").length()];
						String[] name = new String[response.getJSONArray("foodlist").length()];
						String[] count = new String[response.getJSONArray("foodlist").length()];
						String[] cal = new String[response.getJSONArray("foodlist").length()];
						for (int i = 0; i < response.getJSONArray("foodlist").length(); i++)
						{
							food_id[i] = response.getJSONArray("foodlist").getJSONObject(i).getString("food_id");
							array_id.add(response.getJSONArray("foodlist").getJSONObject(i).getString("food_id"));
							name[i] = response.getJSONArray("foodlist").getJSONObject(i).getString("food_name");
							array_name.add(response.getJSONArray("foodlist").getJSONObject(i).getString("food_name"));
							count[i] = response.getJSONArray("foodlist").getJSONObject(i).getString("food_portion");
							cal[i] = response.getJSONArray("foodlist").getJSONObject(i).getString("food_cal");
							array_cal.add(response.getJSONArray("foodlist").getJSONObject(i).getString("food_cal"));

							map1.put("name" + i, name[i]);
							map1.put("count" + i, count[i]);
							map1.put("cal" + i, cal[i]);
							mList0.add(map1);
						}

						// 定義標題
						titles[0] = new TableCell("", width, 0, TableCell.STRING);
						titles[1] = new TableCell("", width, 0, TableCell.STRING);
						titles[2] = new TableCell("", width, 0, TableCell.STRING);
						table.add(new TableRow(titles));

						// 每行3個單元
						int j = 0;
						for (int i = 0; i < mList0.size(); i++)
						{
							TableCell[] cells = new TableCell[3];
							cells[0] = new TableCell("" + mList0.get(i).get("name" + i), titles[0].width, 100, TableCell.STRING);
							cells[1] = new TableCell("" + mList0.get(i).get("count" + i), titles[1].width, 100, TableCell.STRING);
							cells[2] = new TableCell("" + mList0.get(i).get("cal" + i), titles[2].width, 100, TableCell.STRING);
							table.add(new TableRow(cells));
						}
						TableAdapter tableAdapter = new TableAdapter(Activity_selectFoodPage.this, table);
						mLv.setAdapter(tableAdapter);
						glob();
						Log.e("*********test***********", "okok");
						// mLv.notifyAll();
						mLv.setOnItemClickListener(new ItemClickEvent(0));
						//mList_addfood.setOnItemClickListener(new Itemdelete());
						ringProgressDialog.dismiss();
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					super.onSuccess(response);
				}
			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void AddFoodItem(final int arg2, final int counts, int partion)
	{
		mCalValue += Integer.parseInt(array_cal.get(arg2 - 1)) * counts;
		mList_tmp.add(Integer.parseInt(array_cal.get(arg2 - 1)) * counts);
		mCal_Value.setText("卡路里總和：" + mCalValue);
		mItems.add(array_name.get(arg2 - 1) + "   " + counts + "份" + "   " + Integer.parseInt(array_cal.get(arg2 - 1)) * counts + "卡");
		mItems_id.add(Integer.parseInt(array_id.get(arg2 - 1)));
		mItems_portion.add(counts);
		mList_addfood.setAdapter(mAdapter);
	}

	public void AddFoodItem_1(final int arg2, final int counts, String search_name)
	{
		mCalValue += Integer.parseInt(array_cal.get(arg2 - 1)) * counts;
		mList_tmp.add(Integer.parseInt(array_cal.get(arg2 - 1)) * counts);
		mCal_Value.setText("卡路里總和：" + mCalValue);
		mItems.add(array_name.get(arg2 - 1) + "   " + counts + "份" + "   " + Integer.parseInt(array_cal.get(arg2 - 1)) * counts + "卡");
		mItems_id.add(Integer.parseInt(array_id.get(arg2 - 1)));
		mItems_portion.add(counts);
		mList_addfood.setAdapter(mAdapter);
	}

	class Itemdelete implements AdapterView.OnItemClickListener
	{

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3)
		{
			// Toast.makeText(Activity_selectFoodPage.this, "" +
			// String.valueOf(arg2), 500).show();
			AlertDialog.Builder dialog_delete = new AlertDialog.Builder(Activity_selectFoodPage.this);
			dialog_delete.setTitle("刪除項目");
			dialog_delete.setMessage("是否刪除此項目?");
			dialog_delete.setPositiveButton("返回", new DialogInterface.OnClickListener()
			{

				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					dialog.cancel();
				}
			});
			dialog_delete.setNegativeButton("刪除", new DialogInterface.OnClickListener()
			{

				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					mCalValue -= mList_tmp.get(arg2);
					if(mCalValue!=0)
					{
						mList_tmp.remove(arg2);
						mCal_Value.setText("卡路里總和：" + mCalValue);
						mItems.remove(arg2);
						mItems_id.remove(arg2);
						mItems_portion.remove(arg2);
						mList_addfood.setAdapter(mAdapter);
					}
					else
					{
						mList_tmp.remove(arg2);
						mCal_Value.setText("卡路里總和：" + mCalValue);
						mItems.remove(arg2);
						mItems_id.remove(arg2);
						mItems_portion.remove(arg2);
						String[] array1 = new String[] { "暫無資料","","","","" };
						ArrayAdapter<String> adapter1 = new ArrayAdapter(Activity_selectFoodPage.this, android.R.layout.simple_list_item_1, array1);
						mList_addfood.setAdapter(adapter1);
						mList_addfood.setOnItemClickListener(null);
					}
					
				}
			});

			dialog_delete.show();

		}

	}

	class ItemClickEvent implements AdapterView.OnItemClickListener
	{
		private int part;

		public ItemClickEvent(int partion)
		{
			part = partion;
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3)
		{
			// Toast.makeText(Activity_selectFoodPage.this, "" +
			// String.valueOf(arg2), 500).show();

			if (arg2 != 0)
			{
				AlertDialog.Builder dialog = new AlertDialog.Builder(Activity_selectFoodPage.this);
				dialog.setTitle("請輸入份量");
				LayoutInflater inflater = Activity_selectFoodPage.this.getLayoutInflater();
				final View layout = inflater.inflate(R.layout.dialog_selectfood, null);
				dialog.setView(layout);
				mBtn_add = (Button) layout.findViewById(R.id.btn_select_dinner);
				mBtn_sub = (Button) layout.findViewById(R.id.btn_select_breakfast);
				mEdit_value = (EditText) layout.findViewById(R.id.edit_value);
				mEdit_value.setEnabled(false);
				dialog.setPositiveButton("加入", new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						AddFoodItem(arg2, Integer.parseInt(mEdit_value.getText().toString()), part);
						mList_addfood.setOnItemClickListener(new Itemdelete());
					}
				});
				dialog.setNegativeButton("返回", new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.cancel();
					}
				});
				dialog.show();
				mFoodname = (TextView) layout.findViewById(R.id.text_foodname);
				mFoodname.setText(array_name.get(arg2 - 1)); // 取得dialog食物名稱
//				try
//				{
//					API_Funtion.food_partition(88122841, gettoken(), part, new JsonHttpResponseHandler()
//					{
//
//						@Override
//						public void onSuccess(JSONObject response)
//						{
//							try
//							{
//								String[] name = new String[response.getJSONArray("foodlist").length()];
//								for (int i = 0; i < response.getJSONArray("foodlist").length(); i++)
//								{
//									name[i] = response.getJSONArray("foodlist").getJSONObject(i).getString("food_name");
//								}
//								mFoodname = (TextView) layout.findViewById(R.id.text_foodname);
//								mFoodname.setText(name[arg2 - 1]); // 取得dialog食物名稱
//							}
//							catch (JSONException e)
//							{
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//							super.onSuccess(response);
//						}
//
//					});
//				}
//				catch (UnsupportedEncodingException e)
//				{
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				catch (JSONException e)
//				{
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}

				mBtn_add.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						mResultValue = Integer.parseInt(mEdit_value.getText().toString());
						mResultValue += 1;
						mEdit_value.setText("" + mResultValue);
						if (mResultValue > 99)
						{
							mResultValue = 99;
							mEdit_value.setText("" + mResultValue);
						}
					}
				});

				mBtn_sub.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						mResultValue = Integer.parseInt(mEdit_value.getText().toString());
						mResultValue -= 1;
						mEdit_value.setText("" + mResultValue);
						if (mResultValue < 1)
						{
							mResultValue = 1;
							mEdit_value.setText("" + mResultValue);
						}
					}
				});

			}
		}

	}

	class ItemClickEvent_1 implements AdapterView.OnItemClickListener
	{
		private String search_name;

		public ItemClickEvent_1(String foodname)
		{
			search_name = foodname;
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3)
		{
			// Toast.makeText(Activity_selectFoodPage.this, "" +
			// String.valueOf(arg2), 500).show();

			if (arg2 != 0)
			{
				AlertDialog.Builder dialog = new AlertDialog.Builder(Activity_selectFoodPage.this);
				dialog.setTitle("請輸入份量");
				LayoutInflater inflater = Activity_selectFoodPage.this.getLayoutInflater();
				final View layout = inflater.inflate(R.layout.dialog_selectfood, null);
				dialog.setView(layout);
				mBtn_add = (Button) layout.findViewById(R.id.btn_select_dinner);
				mBtn_sub = (Button) layout.findViewById(R.id.btn_select_breakfast);
				mEdit_value = (EditText) layout.findViewById(R.id.edit_value);
				mEdit_value.setEnabled(false);
				dialog.setPositiveButton("加入", new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						AddFoodItem_1(arg2, Integer.parseInt(mEdit_value.getText().toString()), search_name);
						mList_addfood.setOnItemClickListener(new Itemdelete());
					}
				});
				dialog.setNegativeButton("返回", new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.cancel();
					}
				});
				dialog.show();
				mFoodname = (TextView) layout.findViewById(R.id.text_foodname);
				mFoodname.setText(array_name.get(arg2 - 1)); // 取得dialog食物名稱
//				try
//				{
//					API_Funtion.search_food(88122841, search_name, new JsonHttpResponseHandler()
//					{
//
//						@Override
//						public void onSuccess(JSONObject response)
//						{
//							try
//							{
//								String[] name = new String[response.getJSONArray("foodlist").length()];
//								for (int i = 0; i < response.getJSONArray("foodlist").length(); i++)
//								{
//									name[i] = response.getJSONArray("foodlist").getJSONObject(i).getString("food_name");
//								}
//								mFoodname = (TextView) layout.findViewById(R.id.text_foodname);
//								mFoodname.setText(name[arg2 - 1]); // 取得dialog食物名稱
//							}
//							catch (JSONException e)
//							{
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//							super.onSuccess(response);
//						}
//
//					});
//				}
//				catch (UnsupportedEncodingException e)
//				{
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				catch (JSONException e)
//				{
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}

				mBtn_add.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						mResultValue = Integer.parseInt(mEdit_value.getText().toString());
						mResultValue += 1;
						mEdit_value.setText("" + mResultValue);
						if (mResultValue > 99)
						{
							mResultValue = 99;
							mEdit_value.setText("" + mResultValue);
						}
					}
				});

				mBtn_sub.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						mResultValue = Integer.parseInt(mEdit_value.getText().toString());
						mResultValue -= 1;
						mEdit_value.setText("" + mResultValue);
						if (mResultValue < 1)
						{
							mResultValue = 1;
							mEdit_value.setText("" + mResultValue);
						}
					}
				});

			}
		}

	}

	@SuppressLint("NewApi")
	private void onclickListener()
	{
		// final ArrayList<TableRow> table = new ArrayList<TableRow>();
		// final TableCell[] titles = new TableCell[3];// 每行3個單元
		// final int width =
		// this.getWindowManager().getDefaultDisplay().getWidth() /
		// titles.length;
		mButton_portion_0.setOnClickListener(new OnClickListener()
		{

			public void onClick(View arg0)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape3);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);

				get_foodmenu(0);
			}

		});

		mButton_portion_1.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape3);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);

				get_foodmenu(1);

			}
		});

		mButton_portion_2.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape3);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);

				get_foodmenu(2);
			}
		});

		mButton_portion_3.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape3);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);

				get_foodmenu(3);

			}
		});

		mButton_portion_4.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape3);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(4);
			}
		});

		mButton_portion_5.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape3);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(5);
			}
		});

		mButton_portion_6.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape3);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(6);
			}
		});

		mButton_portion_7.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape3);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(7);
			}
		});

		mButton_portion_8.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape3);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(8);
			}
		});

		mButton_portion_9.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape3);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(9);
			}
		});

		mButton_portion_10.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape3);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(10);
			}
		});

		mButton_portion_11.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape3);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(11);
			}
		});

		mButton_portion_12.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape3);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(12);
			}
		});

		mButton_portion_13.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape3);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(13);
			}
		});

		mButton_portion_14.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape3);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(14);
			}
		});

		mButton_portion_15.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape3);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(15);
			}
		});

		mButton_portion_16.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape3);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(16);
			}
		});

		mButton_portion_17.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape3);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(17);
			}
		});

		mButton_portion_18.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape3);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(18);
			}
		});

		mButton_portion_19.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape3);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(19);
			}
		});

		mButton_portion_20.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape3);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(20);
			}
		});

		mButton_portion_21.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape3);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(21);
			}
		});

		mButton_portion_22.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape3);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(22);
			}
		});

		mButton_portion_23.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape3);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(23);
			}
		});

		mButton_portion_24.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape3);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(24);
			}
		});

		mButton_portion_25.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape3);
				mButton_portion_26.setBackgroundResource(R.drawable.shape2);
				get_foodmenu(25);
			}
		});

		mButton_portion_26.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mButton_portion_0.setBackgroundResource(R.drawable.shape2);
				mButton_portion_1.setBackgroundResource(R.drawable.shape2);
				mButton_portion_2.setBackgroundResource(R.drawable.shape2);
				mButton_portion_3.setBackgroundResource(R.drawable.shape2);
				mButton_portion_4.setBackgroundResource(R.drawable.shape2);
				mButton_portion_5.setBackgroundResource(R.drawable.shape2);
				mButton_portion_6.setBackgroundResource(R.drawable.shape2);
				mButton_portion_7.setBackgroundResource(R.drawable.shape2);
				mButton_portion_8.setBackgroundResource(R.drawable.shape2);
				mButton_portion_9.setBackgroundResource(R.drawable.shape2);
				mButton_portion_10.setBackgroundResource(R.drawable.shape2);
				mButton_portion_11.setBackgroundResource(R.drawable.shape2);
				mButton_portion_12.setBackgroundResource(R.drawable.shape2);
				mButton_portion_13.setBackgroundResource(R.drawable.shape2);
				mButton_portion_14.setBackgroundResource(R.drawable.shape2);
				mButton_portion_15.setBackgroundResource(R.drawable.shape2);
				mButton_portion_16.setBackgroundResource(R.drawable.shape2);
				mButton_portion_17.setBackgroundResource(R.drawable.shape2);
				mButton_portion_18.setBackgroundResource(R.drawable.shape2);
				mButton_portion_19.setBackgroundResource(R.drawable.shape2);
				mButton_portion_20.setBackgroundResource(R.drawable.shape2);
				mButton_portion_21.setBackgroundResource(R.drawable.shape2);
				mButton_portion_22.setBackgroundResource(R.drawable.shape2);
				mButton_portion_23.setBackgroundResource(R.drawable.shape2);
				mButton_portion_24.setBackgroundResource(R.drawable.shape2);
				mButton_portion_25.setBackgroundResource(R.drawable.shape2);
				mButton_portion_26.setBackgroundResource(R.drawable.shape3);
				get_foodmenu(26);
			}
		});
	}

	private void init()
	{
		mButton_portion_0 = (Button) findViewById(R.id.btn_0);
		mButton_portion_1 = (Button) findViewById(R.id.btn_1);
		mButton_portion_2 = (Button) findViewById(R.id.btn_2);
		mButton_portion_3 = (Button) findViewById(R.id.btn_3);
		mButton_portion_4 = (Button) findViewById(R.id.btn_4);
		mButton_portion_5 = (Button) findViewById(R.id.btn_5);
		mButton_portion_6 = (Button) findViewById(R.id.btn_6);
		mButton_portion_7 = (Button) findViewById(R.id.btn_7);
		mButton_portion_8 = (Button) findViewById(R.id.btn_8);
		mButton_portion_9 = (Button) findViewById(R.id.btn_9);
		mButton_portion_10 = (Button) findViewById(R.id.btn_10);
		mButton_portion_11 = (Button) findViewById(R.id.btn_11);
		mButton_portion_12 = (Button) findViewById(R.id.btn_12);
		mButton_portion_13 = (Button) findViewById(R.id.btn_13);
		mButton_portion_14 = (Button) findViewById(R.id.btn_14);
		mButton_portion_15 = (Button) findViewById(R.id.btn_15);
		mButton_portion_16 = (Button) findViewById(R.id.btn_16);
		mButton_portion_17 = (Button) findViewById(R.id.btn_17);
		mButton_portion_18 = (Button) findViewById(R.id.btn_18);
		mButton_portion_19 = (Button) findViewById(R.id.btn_19);
		mButton_portion_20 = (Button) findViewById(R.id.btn_20);
		mButton_portion_21 = (Button) findViewById(R.id.btn_21);
		mButton_portion_22 = (Button) findViewById(R.id.btn_22);
		mButton_portion_23 = (Button) findViewById(R.id.btn_23);
		mButton_portion_24 = (Button) findViewById(R.id.btn_24);
		mButton_portion_25 = (Button) findViewById(R.id.btn_25);
		mButton_portion_26 = (Button) findViewById(R.id.btn_26);

		mQuickReturnView = (LinearLayout) findViewById(R.id.quik_layout);
		mLv = (QuickReturnListView) findViewById(R.id.ListView_foodmenu);
		
		mCal_Value = (TextView) findViewById(R.id.footer);
		mList_addfood = (ListView) findViewById(R.id.listView_addfood);
		mItems = new ArrayList<String>();
		mAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, mItems);
		

		String[] array1 = new String[] { "資料讀取中..." };
		ArrayAdapter<String> adapter1 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, array1);
		mLv.setAdapter(adapter1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(0, 0, 1, "存檔").setShowAsAction(2);
		menu.add(0, 1, 0, "").setIcon(android.R.drawable.ic_menu_search).setShowAsAction(2);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case 0:
			{
				AlertDialog.Builder dialog_save = new AlertDialog.Builder(Activity_selectFoodPage.this);
				dialog_save.setTitle("存檔");
				dialog_save.setMessage("是否儲存目前資訊？");
				dialog_save.setNegativeButton("是", new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						JSONArray array = new JSONArray();
						for (int i = 0; i < mItems_id.size(); i++)
						{
							JSONObject object = new JSONObject();
							if (mMeal_number.equals("0"))
							{
								try
								{
									object.put("food_id", mItems_id.get(i));
									object.put("breakfast_food_portion", mItems_portion.get(i));
									array.put(i, object);
									Log.e("ww", array.toString());
								}
								catch (JSONException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							else if (mMeal_number.equals("1"))
							{
								try
								{
									object.put("food_id", mItems_id.get(i));
									object.put("lunch_food_portion", mItems_portion.get(i));
									array.put(object);
								}
								catch (JSONException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							else if (mMeal_number.equals("2"))
							{
								try
								{
									object.put("food_id", mItems_id.get(i));
									object.put("dinner_food_portion", mItems_portion.get(i));
									array.put(object);
								}
								catch (JSONException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							else
							{
								try
								{
									object.put("food_id", mItems_id.get(i));
									object.put("other_food_portion", mItems_portion.get(i));
									array.put(object);
								}
								catch (JSONException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

						}
						try
						{
							API_Funtion.food_update(88122841, gettoken(), mMeal_number, array, new JsonHttpResponseHandler()
							{

								@Override
								public void onSuccess(JSONObject response)
								{
									Toast.makeText(Activity_selectFoodPage.this, "成功輸入!", Toast.LENGTH_LONG).show();
									super.onSuccess(response);
								}
							});
						}
						catch (UnsupportedEncodingException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						catch (JSONException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Intent intent = new Intent();
						intent.setClass(Activity_selectFoodPage.this, Activity_MealsPage.class);
						startActivity(intent);
						Activity_selectFoodPage.this.finish();
					}
				});
				dialog_save.setPositiveButton("否", new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.cancel();
					}
				});
				dialog_save.show();
				break;
			}
			// ------------------------------------//

			case 1:
			{
				AlertDialog.Builder dialog_search = new AlertDialog.Builder(Activity_selectFoodPage.this);
				dialog_search.setTitle("搜尋食物");
				LayoutInflater inflater = Activity_selectFoodPage.this.getLayoutInflater();
				final View layout = inflater.inflate(R.layout.dialog_searchbar, null);
				final SearchView mSearch;
				mSearch = (SearchView) layout.findViewById(R.id.searchView1);
				dialog_search.setView(layout);
				mSearch.setIconifiedByDefault(false);
				// mSearch.setSubmitButtonEnabled(true);
				mSearch.setQueryHint("請輸入欲查詢食物名稱");
				dialog_search.setNegativeButton("搜尋", new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						//Toast.makeText(Activity_selectFoodPage.this, "" + mSearch.getQuery().toString(), Toast.LENGTH_SHORT).show();
						search_foodmenu(mSearch.getQuery().toString());
						dialog.cancel();
					}
				});
				dialog_search.setPositiveButton("返回", new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.cancel();
					}
				});
				dialog_search.show();
				break;
			}
			case android.R.id.home:            
				AlertDialog.Builder dialog_save = new AlertDialog.Builder(Activity_selectFoodPage.this);
				dialog_save.setTitle("存檔");
				dialog_save.setMessage("是否儲存目前資訊？");
				dialog_save.setNegativeButton("是", new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						JSONArray array = new JSONArray();
						for (int i = 0; i < mItems_id.size(); i++)
						{
							JSONObject object = new JSONObject();
							if (mMeal_number.equals("0"))
							{
								try
								{
									object.put("food_id", mItems_id.get(i));
									object.put("breakfast_food_portion", mItems_portion.get(i));
									array.put(i, object);
									Log.e("ww", array.toString());
								}
								catch (JSONException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							else if (mMeal_number.equals("1"))
							{
								try
								{
									object.put("food_id", mItems_id.get(i));
									object.put("lunch_food_portion", mItems_portion.get(i));
									array.put(object);
								}
								catch (JSONException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							else if (mMeal_number.equals("2"))
							{
								try
								{
									object.put("food_id", mItems_id.get(i));
									object.put("dinner_food_portion", mItems_portion.get(i));
									array.put(object);
								}
								catch (JSONException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							else
							{
								try
								{
									object.put("food_id", mItems_id.get(i));
									object.put("other_food_portion", mItems_portion.get(i));
									array.put(object);
								}
								catch (JSONException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

						}
						try
						{
							API_Funtion.food_update(88122841, gettoken(), mMeal_number, array, new JsonHttpResponseHandler()
							{

								@Override
								public void onSuccess(JSONObject response)
								{
									Toast.makeText(Activity_selectFoodPage.this, "成功輸入!", Toast.LENGTH_LONG).show();
									super.onSuccess(response);
								}
							});
						}
						catch (UnsupportedEncodingException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						catch (JSONException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Intent intent = new Intent();
						intent.setClass(Activity_selectFoodPage.this, Activity_MealsPage.class);
						startActivity(intent);
						Activity_selectFoodPage.this.finish();
					}
				});
				dialog_save.setPositiveButton("否", new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						Intent intent = new Intent(Activity_selectFoodPage.this, Activity_MealsPage.class);            
				        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
				        startActivity(intent);
				        Activity_selectFoodPage.this.finish();
					}
				});
				dialog_save.show();
		         return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}

	public void get_foodmenu(final int partion)
	{
		// ------//
		final ProgressDialog ringProgressDialog = ProgressDialog.show(Activity_selectFoodPage.this, "Please wait ...", "Data Loading ...", true);
		ringProgressDialog.setCancelable(true);
		try
		{
			API_Funtion.food_partition(88122841, gettoken(), partion, new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					ArrayList<TableRow> table = new ArrayList<TableRow>();
					TableCell[] titles = new TableCell[3];
					int width = Activity_selectFoodPage.this.getWindowManager().getDefaultDisplay().getWidth() / titles.length;
					table.clear();
					map1.clear();
					mList0.clear();
					array_name.clear();
					array_id.clear();
					array_cal.clear();
					// Toast.makeText(Activity_selectFoodPage.this, "0",
					// Toast.LENGTH_LONG).show();
					try
					{
						String[] food_id = new String[response.getJSONArray("foodlist").length()];
						String[] name = new String[response.getJSONArray("foodlist").length()];
						String[] count = new String[response.getJSONArray("foodlist").length()];
						String[] cal = new String[response.getJSONArray("foodlist").length()];
						for (int i = 0; i < response.getJSONArray("foodlist").length(); i++)
						{
							food_id[i] = response.getJSONArray("foodlist").getJSONObject(i).getString("food_id");
							array_id.add(response.getJSONArray("foodlist").getJSONObject(i).getString("food_id"));
							name[i] = response.getJSONArray("foodlist").getJSONObject(i).getString("food_name");
							array_name.add(response.getJSONArray("foodlist").getJSONObject(i).getString("food_name"));
							count[i] = response.getJSONArray("foodlist").getJSONObject(i).getString("food_portion");
							cal[i] = response.getJSONArray("foodlist").getJSONObject(i).getString("food_cal");
							array_cal.add(response.getJSONArray("foodlist").getJSONObject(i).getString("food_cal"));

							map1.put("name" + i, name[i]);
							map1.put("count" + i, count[i]);
							map1.put("cal" + i, cal[i]);
							mList0.add(map1);
						}

						// 定義標題
						titles[0] = new TableCell("", width, 0, TableCell.STRING);
						titles[1] = new TableCell("", width, 0, TableCell.STRING);
						titles[2] = new TableCell("", width, 0, TableCell.STRING);
						table.add(new TableRow(titles));

						// 每行3個單元
						int j = 0;
						for (int i = 0; i < mList0.size(); i++)
						{
							TableCell[] cells = new TableCell[3];
							cells[0] = new TableCell("" + mList0.get(i).get("name" + i), titles[0].width, 100, TableCell.STRING);
							cells[1] = new TableCell("" + mList0.get(i).get("count" + i), titles[1].width, 100, TableCell.STRING);
							cells[2] = new TableCell("" + mList0.get(i).get("cal" + i), titles[2].width, 100, TableCell.STRING);
							table.add(new TableRow(cells));
						}
						TableAdapter tableAdapter = new TableAdapter(Activity_selectFoodPage.this, table);
						mLv.setAdapter(tableAdapter);
						ringProgressDialog.dismiss();
						//glob();
						mLv.setOnItemClickListener(new ItemClickEvent(partion));
						mList_addfood.setOnItemClickListener(new Itemdelete());
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					super.onSuccess(response);
				}
			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// ------//
	}

	public void search_foodmenu(final String foodname)
	{
		// ------//
		final ProgressDialog ringProgressDialog = ProgressDialog.show(Activity_selectFoodPage.this, "Please wait ...", "Data Loading ...", true);
		ringProgressDialog.setCancelable(true);
		try
		{
			API_Funtion.search_food(88122841, foodname, new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					ArrayList<TableRow> table = new ArrayList<TableRow>();
					TableCell[] titles = new TableCell[3];
					int width = Activity_selectFoodPage.this.getWindowManager().getDefaultDisplay().getWidth() / titles.length;
					table.clear();
					map1.clear();
					mList0.clear();
					array_name.clear();
					array_id.clear();
					array_cal.clear();
					// Toast.makeText(Activity_selectFoodPage.this, "0",
					// Toast.LENGTH_LONG).show();
					try
					{
						String[] food_id = new String[response.getJSONArray("foodlist").length()];
						String[] name = new String[response.getJSONArray("foodlist").length()];
						String[] count = new String[response.getJSONArray("foodlist").length()];
						String[] cal = new String[response.getJSONArray("foodlist").length()];
						for (int i = 0; i < response.getJSONArray("foodlist").length(); i++)
						{
							food_id[i] = response.getJSONArray("foodlist").getJSONObject(i).getString("food_id");
							array_id.add(response.getJSONArray("foodlist").getJSONObject(i).getString("food_id"));
							name[i] = response.getJSONArray("foodlist").getJSONObject(i).getString("food_name");
							array_name.add(response.getJSONArray("foodlist").getJSONObject(i).getString("food_name"));
							count[i] = response.getJSONArray("foodlist").getJSONObject(i).getString("food_portion");
							cal[i] = response.getJSONArray("foodlist").getJSONObject(i).getString("food_cal");
							array_cal.add(response.getJSONArray("foodlist").getJSONObject(i).getString("food_cal"));

							map1.put("name" + i, name[i]);
							map1.put("count" + i, count[i]);
							map1.put("cal" + i, cal[i]);
							mList0.add(map1);
						}

						// 定義標題
						titles[0] = new TableCell("", width, 0, TableCell.STRING);
						titles[1] = new TableCell("", width, 0, TableCell.STRING);
						titles[2] = new TableCell("", width, 0, TableCell.STRING);
						table.add(new TableRow(titles));

						// 每行3個單元
						int j = 0;
						for (int i = 0; i < mList0.size(); i++)
						{
							TableCell[] cells = new TableCell[3];
							cells[0] = new TableCell("" + mList0.get(i).get("name" + i), titles[0].width, 100, TableCell.STRING);
							cells[1] = new TableCell("" + mList0.get(i).get("count" + i), titles[1].width, 100, TableCell.STRING);
							cells[2] = new TableCell("" + mList0.get(i).get("cal" + i), titles[2].width, 100, TableCell.STRING);
							table.add(new TableRow(cells));
						}
						TableAdapter tableAdapter = new TableAdapter(Activity_selectFoodPage.this, table);
						mLv.setAdapter(tableAdapter);
						ringProgressDialog.dismiss();
						//glob();
						mLv.setOnItemClickListener(new ItemClickEvent_1(foodname));
						mList_addfood.setOnItemClickListener(new Itemdelete());
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					super.onSuccess(response);
				}
			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// ------//
	}

	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{

			AlertDialog.Builder dialog_save = new AlertDialog.Builder(Activity_selectFoodPage.this);
			dialog_save.setTitle("存檔");
			dialog_save.setMessage("是否儲存目前資訊？");
			dialog_save.setNegativeButton("是", new DialogInterface.OnClickListener()
			{

				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					final ProgressDialog ringProgressDialog = ProgressDialog.show(Activity_selectFoodPage.this, "Please wait ...", "Data Loading ...", true);
					ringProgressDialog.setCancelable(true);
					JSONArray array = new JSONArray();
					for (int i = 0; i < mItems_id.size(); i++)
					{
						JSONObject object = new JSONObject();
						if (mMeal_number.equals("0"))
						{
							try
							{
								object.put("food_id", mItems_id.get(i));
								object.put("breakfast_food_portion", mItems_portion.get(i));
								array.put(i, object);
								Log.e("ww", array.toString());
							}
							catch (JSONException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						else if (mMeal_number.equals("1"))
						{
							try
							{
								object.put("food_id", mItems_id.get(i));
								object.put("lunch_food_portion", mItems_portion.get(i));
								array.put(object);
							}
							catch (JSONException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						else if (mMeal_number.equals("2"))
						{
							try
							{
								object.put("food_id", mItems_id.get(i));
								object.put("dinner_food_portion", mItems_portion.get(i));
								array.put(object);
							}
							catch (JSONException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						else
						{
							try
							{
								object.put("food_id", mItems_id.get(i));
								object.put("other_food_portion", mItems_portion.get(i));
								array.put(object);
							}
							catch (JSONException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					}
					try
					{
						API_Funtion.food_update(88122841, gettoken(), mMeal_number, array, new JsonHttpResponseHandler()
						{

							@Override
							public void onSuccess(JSONObject response)
							{
								Toast.makeText(Activity_selectFoodPage.this, "成功輸入!", Toast.LENGTH_LONG).show();
								super.onSuccess(response);
							}
						});
					}
					catch (UnsupportedEncodingException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Intent intent = new Intent();
					intent.setClass(Activity_selectFoodPage.this, Activity_MealsPage.class);
					ringProgressDialog.dismiss();
					startActivity(intent);
					Activity_selectFoodPage.this.finish();
				}
			});
			dialog_save.setPositiveButton("否", new DialogInterface.OnClickListener()
			{

				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					Intent intent = new Intent(Activity_selectFoodPage.this, Activity_MealsPage.class);            
			        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
			        startActivity(intent);
			        Activity_selectFoodPage.this.finish();
				}
			});
			dialog_save.show();
		}
		return super.onKeyDown(keyCode, event);
	}

	private void glob()
	{
		mLv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
		{
			@Override
			public void onGlobalLayout()
			{
				mQuickReturnHeight = mQuickReturnView.getHeight();
				mLv.computeScrollY();
			}
		});

		mLv.setOnScrollListener(new OnScrollListener()
		{
			@SuppressLint("NewApi")
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
			{

				mScrollY = 0;
				int translationY = 0;

				if (mLv.scrollYIsComputed())
				{
					mScrollY = mLv.getComputedScrollY();
				}

				int rawY = mScrollY;

				switch (mState)
				{
					case STATE_OFFSCREEN:
						if (rawY >= mMinRawY)
						{
							mMinRawY = rawY;
						}
						else
						{
							mState = STATE_RETURNING;
						}
						translationY = rawY;
						break;

					case STATE_ONSCREEN:
						if (rawY > mQuickReturnHeight)
						{
							mState = STATE_OFFSCREEN;
							mMinRawY = rawY;
						}
						translationY = rawY;
						break;

					case STATE_RETURNING:

						translationY = (rawY - mMinRawY) + mQuickReturnHeight;

						System.out.println(translationY);
						if (translationY < 0)
						{
							translationY = 0;
							mMinRawY = rawY + mQuickReturnHeight;
						}

						if (rawY == 0)
						{
							mState = STATE_ONSCREEN;
							translationY = 0;
						}

						if (translationY > mQuickReturnHeight)
						{
							mState = STATE_OFFSCREEN;
							mMinRawY = rawY;
						}
						break;
				}

				/** this can be used if the build is below honeycomb **/
				if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB)
				{
					anim = new TranslateAnimation(0, 0, translationY, translationY);
					anim.setFillAfter(true);
					anim.setDuration(0);
					mQuickReturnView.startAnimation(anim);
				}
				else
				{
					mQuickReturnView.setTranslationY(translationY);
				}

			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState)
			{
			}
		});
	}
	
	public void launchRingDialog(final ArrayList<TableRow> arraylist)
	{
		final ProgressDialog ringProgressDialog = ProgressDialog.show(Activity_selectFoodPage.this, "Please wait ...", "Data Loading ..."+arraylist.size(), true);
		ringProgressDialog.setCancelable(true);
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				if(arraylist.size()!=0)
				{
					ringProgressDialog.dismiss();
				}
			}
		}).start();
	}
}
