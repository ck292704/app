package com.example.informationPage;

import getinfoPage.Activity_about;
import getinfoPage.LoginPage;
import index_page.Activity_index_page;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import widget.AppWidgetExample;

import com.example.hellochart.R;
import com.loopj.android.http.JsonHttpResponseHandler;

import API_Funtion.ClassAPI;
import android.R.drawable;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class Activity_MealsPage extends Activity
{
	ArrayList<Integer> mArraylist_cal = new ArrayList<Integer>();
	ArrayList<Integer> mArraylist_portion = new ArrayList<Integer>();
	int mSumCal = 0;
	String meals_null_breakfast, meals_null_lunch, meals_null_dinner, meals_null_other;
	String breakfast_cal, lunch_cal, dinner_cal, other_cal;
	private ClassAPI API_Funtion = new ClassAPI(Activity_MealsPage.this);
	private ProgressBar mProgressbar;
	private int mPersonalworkValueMax, mPersonalworkValueMin;
	private TextView mText_ProgressValuMax, mText_ProgressValuMin;
	private LinearLayout breakfast;
	private LinearLayout lunch;
	private LinearLayout dinner;
	private LinearLayout other;

	private TextView mTextView_breakfast_cal;
	private TextView mTextView_lunch_cal;
	private TextView mTextView_dinner_cal;
	private TextView mTextView_other_cal;
	private Button mButton_yes;
	private Button mButton_no;
	private ImageView mButton_Dialog_cancel_news;
	private CheckBox mCheckBox_skip;
	private int skip_tmp = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_meals_page);
		// launchRingDialog(null);
		ActionBar bar = getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#ef3a5f8a"));
		bar.setBackgroundDrawable(actionbar_color);
		init();
		//Toast.makeText(Activity_MealsPage.this, ""+getSkip(), Toast.LENGTH_LONG).show();
		if(getSkip() != "")
		{
			skip_tmp = Integer.valueOf(getSkip());
		}
		if (skip_tmp == 0)
		{
			open_news_dialog();
		}
		onclickListner();
		
		
		try
		{
			// API_Funtion.weight_day_show(88122841, gettoken(), new
			// JsonHttpResponseHandler(){
			//
			// @Override
			// public void onSuccess(JSONObject response)
			// {
			// try
			// {
			// mPersonalworkValueMax = response.getInt("user_cal");
			// }
			// catch (JSONException e)
			// {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// super.onSuccess(response);
			// }
			//
			// });
			// ----------------------------------------//
			final ProgressDialog ringProgressDialog = ProgressDialog.show(Activity_MealsPage.this, "Please wait ...", "Data Loading ...", true);
			ringProgressDialog.setCancelable(true);
			API_Funtion.food_show(88122841, gettoken(), API_Funtion.GetTimestamp(), new JsonHttpResponseHandler()
			{

				@Override
				public void onSuccess(JSONObject response)
				{
					try
					{
						mSumCal = response.getInt("foodcal_all");
						meals_null_breakfast = response.get("breakfast").toString();
						meals_null_lunch = response.get("lunch").toString();
						meals_null_dinner = response.get("dinner").toString();
						meals_null_other = response.get("other").toString();
						breakfast_cal = response.getString("breakfast_cal");
						lunch_cal = response.getString("lunch_cal");
						dinner_cal = response.getString("dinner_cal");
						other_cal = response.getString("other_cal");
						mPersonalworkValueMax = response.getInt("user_cal");
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (!meals_null_breakfast.equals("null"))
					{
						mTextView_breakfast_cal.setText("卡路里量： " + breakfast_cal + " 大卡");
					}
					if (!meals_null_lunch.equals("null"))
					{
						mTextView_lunch_cal.setText("卡路里量： " + lunch_cal + " 大卡");
					}
					if (!meals_null_dinner.equals("null"))
					{
						mTextView_dinner_cal.setText("卡路里量： " + dinner_cal + " 大卡");
					}
					if (!meals_null_other.equals("null"))
					{
						mTextView_other_cal.setText("卡路里量： " + other_cal + " 大卡");
					}

					// mPersonalworkValueMax = Integer.parseInt(getneedcal());
					// // 個人工作量最大值
					mPersonalworkValueMin = mSumCal; // 個人累積卡路里

					mText_ProgressValuMax.setText("" + mPersonalworkValueMax);
					mText_ProgressValuMin.setText("" + mPersonalworkValueMin);

					mProgressbar.setMax(mPersonalworkValueMax); // 設定個人工作量最大值
					mProgressbar.incrementProgressBy(mPersonalworkValueMin); // 遞增進度調
					ringProgressDialog.dismiss();
					// Toast.makeText(getActivity(), "123",
					// Toast.LENGTH_LONG).show();
					super.onSuccess(response);
				}
			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Bundle bundle = this.getIntent().getExtras();
		// mPersonalworkValueMax = Integer.parseInt(bundle.getString("MAX"));
		// mPersonalworkValueMin = Integer.parseInt(bundle.getString("MIN"));
		// mProgressbar.setMax(mPersonalworkValueMax);
		// mProgressbar.incrementProgressBy(mPersonalworkValueMin);
		// mText_ProgressValuMax.setText(""+mPersonalworkValueMax);
		// mText_ProgressValuMin.setText(""+mPersonalworkValueMin);
	}

	private void onclickListner()
	{
		breakfast.setOnClickListener(new OnClickListener()
		{

			public void onClick(View v)
			{
				final Dialog dialog = new Dialog(Activity_MealsPage.this, R.style.dialog);
				View myView = LayoutInflater.from(Activity_MealsPage.this).inflate(R.layout.dialog_delayenjoy, null);
				dialog.setContentView(myView);
				mButton_yes = (Button) myView.findViewById(R.id.btn_yes);
				mButton_no = (Button) myView.findViewById(R.id.btn_no);

				mButton_yes.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						Bundle bundle = new Bundle();
						Intent intent = new Intent();
						intent.setClass(Activity_MealsPage.this, Activity_DelayEnjoy_selectfood.class);
						bundle.putString("meal", "0");
						intent.putExtras(bundle);
						startActivity(intent);
						Activity_MealsPage.this.finish();
					}
				});

				mButton_no.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						Bundle bundle = new Bundle();
						Intent intent = new Intent();
						intent.setClass(Activity_MealsPage.this, Activity_selectFoodPage.class);
						bundle.putString("meal", "0");
						intent.putExtras(bundle);
						startActivity(intent);
						Activity_MealsPage.this.finish();
					}
				});
				dialog.show();

			}
		});

		lunch.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				final Dialog dialog = new Dialog(Activity_MealsPage.this, R.style.dialog);
				View myView = LayoutInflater.from(Activity_MealsPage.this).inflate(R.layout.dialog_delayenjoy, null);
				dialog.setContentView(myView);
				mButton_yes = (Button) myView.findViewById(R.id.btn_yes);
				mButton_no = (Button) myView.findViewById(R.id.btn_no);

				mButton_yes.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						Bundle bundle = new Bundle();
						Intent intent = new Intent();
						intent.setClass(Activity_MealsPage.this, Activity_DelayEnjoy_selectfood.class);
						bundle.putString("meal", "1");
						intent.putExtras(bundle);
						startActivity(intent);
						Activity_MealsPage.this.finish();
					}
				});

				mButton_no.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						Bundle bundle = new Bundle();
						Intent intent = new Intent();
						intent.setClass(Activity_MealsPage.this, Activity_selectFoodPage.class);
						bundle.putString("meal", "1");
						intent.putExtras(bundle);
						startActivity(intent);
						Activity_MealsPage.this.finish();
					}
				});
				dialog.show();
			}
		});

		dinner.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				final Dialog dialog = new Dialog(Activity_MealsPage.this, R.style.dialog);
				View myView = LayoutInflater.from(Activity_MealsPage.this).inflate(R.layout.dialog_delayenjoy, null);
				dialog.setContentView(myView);
				mButton_yes = (Button) myView.findViewById(R.id.btn_yes);
				mButton_no = (Button) myView.findViewById(R.id.btn_no);

				mButton_yes.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						Bundle bundle = new Bundle();
						Intent intent = new Intent();
						intent.setClass(Activity_MealsPage.this, Activity_DelayEnjoy_selectfood.class);
						bundle.putString("meal", "2");
						intent.putExtras(bundle);
						startActivity(intent);
						Activity_MealsPage.this.finish();
					}
				});

				mButton_no.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						Bundle bundle = new Bundle();
						Intent intent = new Intent();
						intent.setClass(Activity_MealsPage.this, Activity_selectFoodPage.class);
						bundle.putString("meal", "2");
						intent.putExtras(bundle);
						startActivity(intent);
						Activity_MealsPage.this.finish();
					}
				});
				dialog.show();
			}
		});

		other.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				final Dialog dialog = new Dialog(Activity_MealsPage.this, R.style.dialog);
				View myView = LayoutInflater.from(Activity_MealsPage.this).inflate(R.layout.dialog_delayenjoy, null);
				dialog.setContentView(myView);
				mButton_yes = (Button) myView.findViewById(R.id.btn_yes);
				mButton_no = (Button) myView.findViewById(R.id.btn_no);

				mButton_yes.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						Bundle bundle = new Bundle();
						Intent intent = new Intent();
						intent.setClass(Activity_MealsPage.this, Activity_DelayEnjoy_selectfood.class);
						bundle.putString("meal", "3");
						intent.putExtras(bundle);
						startActivity(intent);
						Activity_MealsPage.this.finish();
					}
				});

				mButton_no.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						Bundle bundle = new Bundle();
						Intent intent = new Intent();
						intent.setClass(Activity_MealsPage.this, Activity_selectFoodPage.class);
						bundle.putString("meal", "3");
						intent.putExtras(bundle);
						startActivity(intent);
						Activity_MealsPage.this.finish();
					}
				});
				dialog.show();
			}
		});

	}

	private void init()
	{
		mProgressbar = (ProgressBar) findViewById(R.id.progressBar_mealPage);
		mText_ProgressValuMax = (TextView) findViewById(R.id.progressValueMax_mealPage);
		mText_ProgressValuMin = (TextView) findViewById(R.id.progressValueMin_mealPage);
		breakfast = (LinearLayout) findViewById(R.id.breakfast);
		lunch = (LinearLayout) findViewById(R.id.lunch);
		dinner = (LinearLayout) findViewById(R.id.dinner);
		other = (LinearLayout) findViewById(R.id.other);
		mTextView_breakfast_cal = (TextView) findViewById(R.id.textView_breakfast_cal);
		mTextView_lunch_cal = (TextView) findViewById(R.id.textView_lunch_cal);
		mTextView_dinner_cal = (TextView) findViewById(R.id.textView_dinner_cal);
		mTextView_other_cal = (TextView) findViewById(R.id.textView_other_cal);
	}

	private String getSkip() // 取得token
	{
		String fileName = "Skip";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = Activity_MealsPage.this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}

	private String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = Activity_MealsPage.this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			Intent intent = new Intent();
			intent.setClass(Activity_MealsPage.this, Activity_index_page.class);
			startActivity(intent);
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case android.R.id.home:
				Intent intent = new Intent(this, Activity_index_page.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				Activity_MealsPage.this.finish();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void open_news_dialog()
	{
		final Dialog dialog_news = new Dialog(Activity_MealsPage.this, R.style.dialog);
		View myView = LayoutInflater.from(Activity_MealsPage.this).inflate(R.layout.dialog_news, null);
		dialog_news.setContentView(myView);
		mCheckBox_skip = (CheckBox) myView.findViewById(R.id.checkBox_dialog_food_sport);
		mButton_Dialog_cancel_news = (ImageView) myView.findViewById(R.id.dialog_process_cancel);
		mButton_Dialog_cancel_news.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				dialog_news.cancel();
			}
		});
		dialog_news.show();

		mCheckBox_skip.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				if (mCheckBox_skip.isChecked())
				{
					String fileName_1 = "Skip";
					getDir(fileName_1, Context.MODE_PRIVATE);
					try
					{
						FileOutputStream writer_1 = openFileOutput(fileName_1, Context.MODE_PRIVATE);
						writer_1.write(String.valueOf(1).getBytes());
						writer_1.close();
					}
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					String fileName_1 = "Skip";
					getDir(fileName_1, Context.MODE_PRIVATE);
					try
					{
						FileOutputStream writer_1 = openFileOutput(fileName_1, Context.MODE_PRIVATE);
						writer_1.write(String.valueOf(0).getBytes());
						writer_1.close();
					}
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		});
	}

	public void launchRingDialog(View view)
	{
		final ProgressDialog ringProgressDialog = ProgressDialog.show(Activity_MealsPage.this, "Please wait ...", "Data Loading ...", true);
		ringProgressDialog.setCancelable(true);
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				if (mText_ProgressValuMin.getTextSize() != 0)
				{
					ringProgressDialog.dismiss();
				}
			}
		}).start();
	}

}
