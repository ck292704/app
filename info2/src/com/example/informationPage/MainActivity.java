package com.example.informationPage;

import index_page.Fragment_index_page;
import index_page.Activity_return;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import logout.Fragment_Logout;

import news_page.Activity_news_page;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import reset_project_page.Fragment_ResetPage;

import getinfoPage.LoginPage;
import healthytool.Fragment_HealthytoolPage;

import com.example.calendarPage.Activity_CalendarPage;
import com.example.hellochart.R;
import com.loopj.android.http.JsonHttpResponseHandler;

import API_Funtion.ClassAPI;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity
{	
	public String mToken = null;
	String meals_null_breakfast, meals_null_lunch, meals_null_dinner;
	private ClassAPI API_Funtion = new ClassAPI(MainActivity.this);
	private DrawerLayout layDrawer;
	private ListView lstDrawer;

	private ActionBarDrawerToggle drawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;

	private String[] drawer_menu;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.fragment_index_page);
		initActionBar();
		initDrawer();
		initDrawerList();
		if (savedInstanceState == null)
		{
			selectItem(0);
		}

//		String fileName = "Token";
//		int readed; // 已讀取的位元數
//		String content = ""; // 內容
//		byte[] buff = new byte[256]; // input stream buffer
//		// Input stream
//		try
//		{
//			FileInputStream reader = openFileInput(fileName);
//			while ((readed = reader.read(buff)) != -1)
//			{
//				content += new String(buff).trim();
//			}
//		}
//		catch (FileNotFoundException e)
//		{
//			e.printStackTrace();
//		}
//		catch (IOException e)
//		{
//			e.printStackTrace();
//		}
//		mToken = content;
		// Toast.makeText(MainActivity.this, mToken, Toast.LENGTH_LONG).show();
		
		
	}

	public boolean onKeyDown(int keyCode, KeyEvent event)
	{// 捕捉返回鍵
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			ConfirmExit();
		}
		return super.onKeyDown(keyCode, event);
	}
	
	
	
	
	public void ConfirmExit()
	{// 退出確認
		AlertDialog.Builder ad = new AlertDialog.Builder(this);
		ad.setTitle("離開");
		ad.setMessage("確定要離開?");
		ad.setNegativeButton("否", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int i)
			{
			}
		});
		ad.setPositiveButton("是", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int i)
			{
				finish();
			}
		});
		ad.show();
	}

	@SuppressLint("NewApi")
	private void initActionBar()
	{
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#ef3a5f8a"));
		//actionbar_color.setAlpha(180);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setBackgroundDrawable(actionbar_color);
	}

	private void initDrawer()
	{
		setContentView(R.layout.drawer);

		layDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		lstDrawer = (ListView) findViewById(R.id.left_drawer);

		layDrawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		
		mTitle = mDrawerTitle = getTitle();
		drawerToggle = new ActionBarDrawerToggle(this, layDrawer, R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close)
		{

			@Override
			public void onDrawerClosed(View view)
			{
				super.onDrawerClosed(view);
				getActionBar().setTitle(mTitle);
			}

			@Override
			public void onDrawerOpened(View drawerView)
			{
				super.onDrawerOpened(drawerView);
				getActionBar().setTitle(mDrawerTitle);
			}
		};
		drawerToggle.syncState();

		layDrawer.setDrawerListener(drawerToggle);
	}

	private void initDrawerList()
	{
		drawer_menu = this.getResources().getStringArray(R.array.drawer_menu);
		final TypedArray drawer_icon = this.getResources().obtainTypedArray(R.array.drawer_icons);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.drawer_list_item, drawer_menu);
		ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, R.layout.menu_list, R.id.txtItem1, drawer_menu)
		{
			@Override
			public View getView(int position, View convertView, ViewGroup parent)
			{
				LayoutInflater mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = mInflater.inflate(R.layout.menu_list, null);
				
				ImageView icon = (ImageView)convertView.findViewById(R.id.img_icon);
				icon.setImageDrawable(drawer_icon.getDrawable(position));
				return super.getView(position, convertView, parent);
			}
		};
		lstDrawer.setAdapter(adapter1);
		lstDrawer.setOnItemClickListener(new DrawerItemClickListener());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		// home
		if (drawerToggle.onOptionsItemSelected(item))
		{
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class DrawerItemClickListener implements ListView.OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		{
			selectItem(position);
		}
	}

	public void selectItem(int position)
	{
		Fragment fragment = null;
		switch (position)
		{
			case 0:
				//主頁index
				fragment = new Fragment_index_page();
				break;

			case 1:
				//行事曆
				//fragment = new Fragment_CalendarPage();
				break;

			case 2:
				//小工具
				fragment = new Fragment_HealthytoolPage();
				// Bundle args = new Bundle();
				// args.putString(FragmentCat.CAT_COLOR, "Brown");
				// fragment.setArguments(args);
				break;

			case 3:
				// 健康知識
				//fragment = new Activity_news_page();
				break;

			case 4:
				// 成就紀錄
				//fragment = new Activity_news_page();
				break;

			case 5:
				//重設計畫
				fragment = new Fragment_ResetPage();
				break;
				
			case 6:
				//登出
				fragment = new Fragment_Logout();
				break;

			default:
				return;
		}

		android.app.FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

		lstDrawer.setItemChecked(position, true);
		setTitle(drawer_menu[position]);
		layDrawer.closeDrawer(lstDrawer);
	}

	@Override
	public void setTitle(CharSequence title)
	{
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	private String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = MainActivity.this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}
	
	public void launchRingDialog(View view)
	{
		final ProgressDialog ringProgressDialog = ProgressDialog.show(MainActivity.this, "Please wait ...", "Data Loading ...", true);
		ringProgressDialog.setCancelable(true);
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					Thread.sleep(5000);
				}
				catch (Exception e)
				{
				}
				//selectItem(0);
				ringProgressDialog.dismiss();
			}
			
		}).start();
	}
	
	
}
