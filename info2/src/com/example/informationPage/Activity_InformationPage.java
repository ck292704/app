package com.example.informationPage;

import API_Funtion.ClassAPI;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.R.color;
import android.R.integer;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.achartengine.image.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sportPage.Sportpage;

import com.example.calendarPage.Activity_CalendarPage;
import com.example.hellochart.R;
import com.loopj.android.http.JsonHttpResponseHandler;

import healthytool.Fragment_HealthytoolPage;
import index_page.Activity_index_page;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import logout.Fragment_Logout;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class Activity_InformationPage extends Activity
{
	private TextView mTextView_bmi_weight;
	private TextView mTextView_first_weight;
	private TextView mTextView_last_weight;
	private TextView mTextView_this_weight;
	private TextView mTextView_last_fat;
	private TextView mTextView_this_fat;
	private Button mButton_myweight;
	private Button mButton_mybody;

	Boolean chart_tmp = true;
	private GraphicalView mChart;
	private ClassAPI API_Funtion = new ClassAPI(Activity_InformationPage.this);

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_information);
		ActionBar bar = getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#3a5f8a"));
		bar.setBackgroundDrawable(actionbar_color);
		mTextView_bmi_weight = (TextView) findViewById(R.id.textView_bmi_weight);
		mTextView_first_weight = (TextView) findViewById(R.id.textView_first_kg);
		mTextView_last_weight = (TextView) findViewById(R.id.textView_lastweek_weight);
		mTextView_this_weight = (TextView) findViewById(R.id.textView_thisweek_weight);
		mTextView_last_fat = (TextView) findViewById(R.id.textView_lastweek_fatrate);
		mTextView_this_fat = (TextView) findViewById(R.id.textView_thisweek_fatrate);
		mButton_myweight = (Button) findViewById(R.id.button_myweight);
		mButton_mybody = (Button) findViewById(R.id.button_mybodyrate);

		if (mChart != null)
		{
			mChart.repaint();
		}
		try
		{
			API_Funtion.weight_day_show(88122841, gettoken(), new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					try
					{
						if (response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 1).getDouble("BMI") < 18.5)
						{
							mTextView_bmi_weight.setText("過輕");
						}
						else if (response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 1).getDouble("BMI") >= 18.5 && response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 1).getDouble("BMI") < 24)
						{
							mTextView_bmi_weight.setText("適中");
						}
						else
						{
							mTextView_bmi_weight.setText("過重");
						}
						mTextView_first_weight.setText(response.getString("user_weight_first") + "KG");
						if (response.getJSONArray("user_data").length() <= 1)
						{
							mTextView_this_weight.setText("無");
							mTextView_last_weight.setText("無");
							mTextView_this_fat.setText("無");
							mTextView_last_fat.setText("無");
						}
//						else if(response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 2).getDouble("body_fat")<0 && response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 1).getDouble("body_fat")<0)
//						{
//							mTextView_last_weight.setText(response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 2).getString("weight_day") + "KG");
//							mTextView_this_weight.setText(response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 1).getString("weight_day") + "KG");
//							//mTextView_last_fat.setText(response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 2).getString("body_fat") + "%");
//							//mTextView_this_fat.setText(response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 1).getString("body_fat") + "%");
//							mTextView_this_fat.setText("無");
//							mTextView_last_fat.setText("無");
//						}
						else
						{
							mTextView_last_weight.setText(response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 2).getString("weight_day") + "KG");
							mTextView_this_weight.setText(response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 1).getString("weight_day") + "KG");
							mTextView_last_fat.setText(response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 2).getString("body_fat") + "%");
							mTextView_this_fat.setText(response.getJSONArray("user_data").getJSONObject(response.getJSONArray("user_data").length() - 1).getString("body_fat") + "%");
						}

					}
					catch (JSONException e1)
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		onclicklisten();
	}

	private void onclicklisten()
	{
		mButton_myweight.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				LinearLayout layout = (LinearLayout) findViewById(R.id.chartViewLayout);
				layout.removeAllViews();
				chart_tmp = true;
				onResume();
				// Toast.makeText(Activity_InformationPage.this, ""+chart_tmp,
				// Toast.LENGTH_LONG).show();
			}
		});

		mButton_mybody.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				LinearLayout layout = (LinearLayout) findViewById(R.id.chartViewLayout);
				layout.removeAllViews();
				chart_tmp = false;
				onResume();
				// Toast.makeText(Activity_InformationPage.this, ""+chart_tmp,
				// Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public void onResume()
	{
		super.onResume();
		if (mChart == null)
		{
			try
			{
				final ProgressDialog ringProgressDialog = ProgressDialog.show(Activity_InformationPage.this, "Please wait ...", "Data Loading ...", true);
				ringProgressDialog.setCancelable(true);
				API_Funtion.weight_day_show(88122841, gettoken(), new JsonHttpResponseHandler()
				{
					@Override
					public void onSuccess(JSONObject response)
					{
						try
						{
							if (chart_tmp == true)
							{
								// Toast.makeText(Activity_InformationPage.this,
								// ""+chart_tmp, Toast.LENGTH_LONG).show();
								double[] ychart = new double[response.getJSONArray("user_data").length()];
								double[] xchart = new double[response.getJSONArray("user_data").length()];
								for (int i = 0; i < response.getJSONArray("user_data").length(); i++)
								{
									ychart[i] = Double.parseDouble(response.getJSONArray("user_data").getJSONObject(i).getString("weight_day"));
									xchart[i] = (double) (i + 1);
								}

								LinearLayout layout = (LinearLayout) findViewById(R.id.chartViewLayout);
								String[] titles = new String[] { "公斤" }; // 定義折線的名稱
								List<double[]> x = new ArrayList<double[]>(); // 點的x坐標
								List<double[]> y = new ArrayList<double[]>(); // 點的y坐標
								// 數值X,Y坐標值輸入
								x.add(xchart); // X資料
								y.add(ychart); // Y資料
								XYMultipleSeriesDataset dataset = buildDatset(titles, x, y); // 儲存座標值

								int[] colors = new int[] { Color.parseColor("#2894FF") };// 折線的顏色
								PointStyle[] styles = new PointStyle[] { PointStyle.CIRCLE }; // 折線點的形狀
								XYMultipleSeriesRenderer renderer = buildRenderer(colors, styles, true);
								
								setChartSettings(renderer, "KG", "", "", 1, 5, GetMinValue(ychart)-10, GetMaxValue(ychart)+10, Color.BLACK);// 定義折線圖
								GraphicalView chart = ChartFactory.getLineChartView(Activity_InformationPage.this, dataset, renderer);
								// renderer.setClickEnabled(true);
								// renderer.setSelectableBuffer(10);
								// layout.removeView(chart);
								layout.addView(chart);
							}
							else
							{
								// Toast.makeText(Activity_InformationPage.this,
								// ""+chart_tmp, Toast.LENGTH_LONG).show();
								double[] ychart = new double[response.getJSONArray("user_data").length()];
								double[] xchart = new double[response.getJSONArray("user_data").length()];
								for (int i = 0; i < response.getJSONArray("user_data").length(); i++)
								{
									if(Double.parseDouble(response.getJSONArray("user_data").getJSONObject(i).getString("body_fat"))<0)
									{
										ychart[i] = 0;
									}
									else
									{
										ychart[i] = Double.parseDouble(response.getJSONArray("user_data").getJSONObject(i).getString("body_fat"));
									}
									xchart[i] = (double) (i + 1);
								}

								LinearLayout layout = (LinearLayout) findViewById(R.id.chartViewLayout);
								String[] titles = new String[] { "體脂肪率" }; // 定義折線的名稱
								List<double[]> x = new ArrayList<double[]>(); // 點的x坐標
								List<double[]> y = new ArrayList<double[]>(); // 點的y坐標
								// 數值X,Y坐標值輸入
								x.add(xchart); // X資料
								y.add(ychart); // Y資料
								XYMultipleSeriesDataset dataset = buildDatset(titles, x, y); // 儲存座標值

								int[] colors = new int[] { Color.parseColor("#00dd77") };// 折線的顏色
								PointStyle[] styles = new PointStyle[] { PointStyle.CIRCLE }; // 折線點的形狀
								XYMultipleSeriesRenderer renderer = buildRenderer(colors, styles, true);

								setChartSettings(renderer, "體脂肪率", "", "", 1, 5, GetMinValue(ychart)-5, GetMaxValue(ychart)+5, Color.BLACK);// 定義折線圖
								GraphicalView chart = ChartFactory.getLineChartView(Activity_InformationPage.this, dataset, renderer);
								// renderer.setClickEnabled(true);
								// renderer.setSelectableBuffer(10);
								// layout.removeView(chart);
								layout.addView(chart);
							}
							ringProgressDialog.dismiss();
						}
						catch (JSONException e1)
						{
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}

				});
			}
			catch (UnsupportedEncodingException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (JSONException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			mChart.repaint();
		}
	}

	@SuppressWarnings("deprecation")
	private void setChartSettings(final XYMultipleSeriesRenderer renderer, String title, String xTitle, String yTitle, double xMin, double xMax, double yMin, double yMax, int axesColor)
	{
		renderer.setChartTitle(title); // 折線圖名稱
		renderer.setChartTitleTextSize(30); // 折線圖名稱字形大小
		renderer.setXTitle(xTitle); // X軸名稱
		renderer.setYTitle(yTitle); // Y軸名稱
		renderer.setXAxisMin(xMin); // X軸顯示最小值
		renderer.setXAxisMax(xMax); // X軸顯示最大值
		renderer.setXLabelsColor(Color.BLACK); // X軸線顏色
		renderer.setYAxisMin(yMin); // Y軸顯示最小值
		renderer.setYAxisMax(yMax); // Y軸顯示最大值
		renderer.setAxesColor(axesColor); // 設定坐標軸顏色
		renderer.setYLabelsColor(0, Color.BLACK); // Y軸線顏色
		renderer.setLabelsColor(Color.BLACK); // 設定標籤顏色
		renderer.setMarginsColor(Color.parseColor("#ddE0E0E0")); // 設定背景顏色
		renderer.setLabelsTextSize(25); // 設定XY標籤字體大小
		renderer.setShowGrid(true); // 設定格線
		renderer.setAxisTitleTextSize(30); // 設定"日期"字型大小
		renderer.setPointSize(8); // 設定point大小
		renderer.setShowLegend(false);// legend開關
		renderer.setPanEnabled(true, false); // XY拉桿開關
		renderer.setPanLimits(new double[] { 0, 100, 0, 100 });// 限制拉桿範圍

		renderer.setXLabelsAlign(Align.CENTER);
		renderer.setXLabels(0);
		renderer.setDisplayChartValues(true); // 顯示點上值
		renderer.setChartValuesTextSize(30); // 點上值大小設定
		try
		{
			API_Funtion.weight_day_show(88122841, gettoken(), new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					try
					{
						String[] mDate = new String[response.getJSONArray("user_data").length()];
						String[] mDate_trans = new String[response.getJSONArray("user_data").length()];
						// Toast.makeText(getActivity(), "" +
						// response.getJSONArray("user_weight").getJSONObject(0).getString("weight_day_date"),
						// Toast.LENGTH_LONG).show();
						for (int i = 0; i < response.getJSONArray("user_data").length(); i++)
						{
							// mDate.add(response.getJSONArray("user_weight").getJSONObject(i).getString("weight_day_date"));
							mDate[i] = response.getJSONArray("user_data").getJSONObject(i).getString("weight_day_date");
							mDate_trans[i] = Trans_date(mDate[i]);
							for (int j = 0; j < mDate.length; j++)
							{
								renderer.addTextLabel(j + 1, mDate_trans[j]);
							}
						}
					}
					catch (JSONException e1)
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// String[] date = (String[]) mDate.toArray(); // 加入日期

	}

	private XYMultipleSeriesRenderer buildRenderer(int[] colors, PointStyle[] styles, boolean fill)
	{
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
		int length = colors.length;
		for (int i = 0; i < length; i++)
		{
			XYSeriesRenderer r = new XYSeriesRenderer();
			r.setColor(colors[i]);
			r.setPointStyle(styles[i]);
			r.setFillPoints(fill);
			r.setLineWidth(3);
			renderer.addSeriesRenderer(r);
		}
		return renderer;
	}

	private XYMultipleSeriesDataset buildDatset(String[] titles, List<double[]> xValues, List<double[]> yValues)
	{
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		int length = titles.length;
		for (int i = 0; i < length; i++)
		{
			XYSeries series = new XYSeries(titles[i]);
			double[] xV = xValues.get(i);
			double[] yV = yValues.get(i);
			int seriesLength = xV.length;
			for (int k = 0; k < seriesLength; k++)
			{
				series.add(xV[k], yV[k]);
			}
			dataset.addSeries(series);
		}
		return dataset;
	}

	private String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}

	public String Trans_date(String date)
	{
		if(date.length()!=8)
		{
			date = "0"+date;
		}
		String msg = null;
		DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		try
		{
			Date today = dateFormat.parse(date);
			DateFormat chtDateFormat = new SimpleDateFormat("yyyy/MM/dd");
			msg = chtDateFormat.format(today);
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
		return msg;
	}

	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			Intent intent = new Intent();
			intent.setClass(Activity_InformationPage.this, Activity_index_page.class);
			startActivity(intent);
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case android.R.id.home:
				Intent intent = new Intent(this, Activity_index_page.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				Activity_InformationPage.this.finish();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	private Double GetMaxValue(double[] array)
	{
		double tmp = 0;
		for(int i = 0;i<array.length;i++)
		{
			if(tmp < array[i])
			{
				tmp = array[i];
			}
		}
		return  tmp;
	}
	private Double GetMinValue(double[] array)
	{
		double tmp ;
		tmp =array[0];
		for(int i = 0;i<array.length;i++)
		{
			if(tmp > array[i])
			{
				tmp = array[i];
			}
		}
		return tmp;
	}
}
