package com.example.calendarPage;

import index_page.Activity_index_page;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.ObjectOutputStream.PutField;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.hellochart.R;
import com.example.hellochart.R.id;
import com.example.hellochart.R.layout;
import com.example.hellochart.R.menu;
import com.example.informationPage.Activity_MealsPage;
import com.example.informationPage.MainActivity;
import com.loopj.android.http.JsonHttpResponseHandler;

import API_Funtion.ClassAPI;
import android.R.integer;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Activity_CalendarPage extends Activity
{
	private ClassAPI API_Funtion = new ClassAPI(Activity_CalendarPage.this);
	public String mSelectDate = String.valueOf(API_Funtion.GetTimestamp());
	public int mHour, mMin;
	public GregorianCalendar month, itemmonth;
	public CalendarAdapter adapter;
	public Handler handler;
	public ArrayList<String> items;
	public int childId = 0;

	public ArrayList<String> mEvent_name = new ArrayList<String>();
	public ArrayList<Integer> mEvent_time = new ArrayList<Integer>();

	public ListView mListView_information;
	public ArrayAdapter<String> adapter_list;
	public ArrayList<String> items_list = new ArrayList<String>();

	ArrayList<String> event;
	LinearLayout rLayout;
	ArrayList<String> date;
	ArrayList<String> desc;

	private Button mButton_breakfast;
	private Button mButton_lunch;
	private Button mButton_dinner;
	private Button mButton_other;
	private Button mButton_sports;
	private Button mButton_events;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.calendar);
		
		ActionBar bar = getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#ef3a5f8a"));
		bar.setBackgroundDrawable(actionbar_color);
		
		adapter_list = new ArrayAdapter(this, android.R.layout.simple_list_item_1, items_list);
		mButton_breakfast = (Button) findViewById(R.id.button_breakfast);
		mButton_lunch = (Button) findViewById(R.id.button_lunch);
		mButton_dinner = (Button) findViewById(R.id.button_dinner);
		mButton_other = (Button) findViewById(R.id.button_other);
		mButton_sports = (Button) findViewById(R.id.button_sports);
		mButton_events = (Button) findViewById(R.id.button_events);
		mListView_information = (ListView) findViewById(R.id.listView_information);
		//mListView_information.setEnabled(false);
		onclick();

		try
		{
			API_Funtion.food_show(88122841, gettoken(), Integer.parseInt(mSelectDate), new JsonHttpResponseHandler()
			{

				@Override
				public void onSuccess(JSONObject response)
				{
					try
					{
						for (int i = 0; i < response.getJSONArray("breakfast").length(); i++)
						{
							String foodname = response.getJSONArray("breakfast").getJSONObject(i).getString("food_name");
							int food_portion = response.getJSONArray("breakfast").getJSONObject(i).getInt("meal_portion");
							int food_cal = response.getJSONArray("breakfast").getJSONObject(i).getInt("food_cal");
							items_list.add(foodname + "  " + food_portion + "份" + "  " + food_cal * food_portion + "大卡");
						}
						adapter_list = new ArrayAdapter(Activity_CalendarPage.this, android.R.layout.simple_list_item_1, items_list);
						mListView_information.setAdapter(adapter_list);
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					super.onSuccess(response);
				}

			});
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		rLayout = (LinearLayout) findViewById(R.id.text);
		month = (GregorianCalendar) GregorianCalendar.getInstance();
		itemmonth = (GregorianCalendar) month.clone();

		items = new ArrayList<String>();

		adapter = new CalendarAdapter(this, month);

		GridView gridview = (GridView) findViewById(R.id.gridview);
		gridview.setAdapter(adapter);

		handler = new Handler();
		handler.post(calendarUpdater);

		TextView title = (TextView) findViewById(R.id.title);
		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));

		RelativeLayout previous = (RelativeLayout) findViewById(R.id.previous);

		previous.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				setPreviousMonth();
				refreshCalendar();
			}
		});

		RelativeLayout next = (RelativeLayout) findViewById(R.id.next);
		next.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				setNextMonth();
				refreshCalendar();

			}
		});

		gridview.setOnItemClickListener(new OnItemClickListener()
		{
			public void onItemClick(AdapterView<?> parent, View v, int position, long id)
			{
				// removing the previous view if added
				if (((LinearLayout) rLayout).getChildCount() > 0)
				{
					((LinearLayout) rLayout).removeAllViews();
				}
				desc = new ArrayList<String>();
				date = new ArrayList<String>();
				((CalendarAdapter) parent.getAdapter()).setSelected(v);
				String selectedGridDate = CalendarAdapter.dayString.get(position);
				String[] separatedTime = selectedGridDate.split("-");
				String gridvalueString = separatedTime[2].replaceFirst("^0*", "");// taking
																					// last
																					// part
																					// of
																					// date.
																					// ie;
																					// 2
																					// from
																					// 2012-12-02.
				int gridvalue = Integer.parseInt(gridvalueString);
				mSelectDate = separatedTime[2] + separatedTime[1] + separatedTime[0];
				// Toast.makeText(getActivity(), mSelectDate,
				// Toast.LENGTH_SHORT).show(); // 取得日期
				// navigate to next or previous month on clicking offdays.
				if ((gridvalue > 10) && (position < 8))
				{
					setPreviousMonth();
					refreshCalendar();
				}
				else if ((gridvalue < 7) && (position > 28))
				{
					setNextMonth();
					refreshCalendar();
				}
				((CalendarAdapter) parent.getAdapter()).setSelected(v);

				for (int i = 0; i < Utility.startDates.size(); i++)
				{
					if (Utility.startDates.get(i).equals(selectedGridDate))
					{
						desc.add(Utility.nameOfEvent.get(i));
					}
				}
				desc = null;
				// -----//
				items_list.clear();
				adapter_list.clear();
				mButton_breakfast.setBackgroundColor(Color.parseColor("#6495ed"));
				mButton_lunch.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_dinner.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_other.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_sports.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_events.setBackgroundResource(R.drawable.foodmenu_portion_shape);

				try
				{
					API_Funtion.food_show(88122841, gettoken(), Integer.parseInt(mSelectDate), new JsonHttpResponseHandler()
					{

						@Override
						public void onSuccess(JSONObject response)
						{
							try
							{
								for (int i = 0; i < response.getJSONArray("breakfast").length(); i++)
								{
									String foodname = response.getJSONArray("breakfast").getJSONObject(i).getString("food_name");
									int food_portion = response.getJSONArray("breakfast").getJSONObject(i).getInt("meal_portion");
									int food_cal = response.getJSONArray("breakfast").getJSONObject(i).getInt("food_cal");
									items_list.add(foodname + "  " + food_portion + "份" + "  " + food_cal * food_portion + "大卡");
								}
								adapter_list = new ArrayAdapter(Activity_CalendarPage.this, android.R.layout.simple_list_item_1, items_list);
								mListView_information.setAdapter(adapter_list);
							}
							catch (JSONException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							super.onSuccess(response);
						}

					});
				}
				catch (NumberFormatException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (UnsupportedEncodingException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// ------//
			}
		});
	}

//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
//	{
//		// items_list.clear();
//		// adapter_list.clear();
//		adapter_list = new ArrayAdapter(this, android.R.layout.simple_list_item_1, items_list);
//		setHasOptionsMenu(true);
//		return initView(inflater, container);
//	}

//	private View initView(LayoutInflater inflater, ViewGroup container)
//	{
//		View view = inflater.inflate(R.layout.calendar, container, false);
//
//		mButton_breakfast = (Button) view.findViewById(R.id.button_breakfast);
//		mButton_lunch = (Button) view.findViewById(R.id.button_lunch);
//		mButton_dinner = (Button) view.findViewById(R.id.button_dinner);
//		mButton_other = (Button) view.findViewById(R.id.button_other);
//		mButton_sports = (Button) view.findViewById(R.id.button_sports);
//		mButton_events = (Button) view.findViewById(R.id.button_events);
//		mListView_information = (ListView) view.findViewById(R.id.listView_information);
//		//mListView_information.setEnabled(false);
//		onclick();
//
//		try
//		{
//			API_Funtion.food_show(88122841, gettoken(), Integer.parseInt(mSelectDate), new JsonHttpResponseHandler()
//			{
//
//				@Override
//				public void onSuccess(JSONObject response)
//				{
//					try
//					{
//						for (int i = 0; i < response.getJSONArray("breakfast").length(); i++)
//						{
//							String foodname = response.getJSONArray("breakfast").getJSONObject(i).getString("food_name");
//							int food_portion = response.getJSONArray("breakfast").getJSONObject(i).getInt("meal_portion");
//							int food_cal = response.getJSONArray("breakfast").getJSONObject(i).getInt("food_cal");
//							items_list.add(foodname + "  " + food_portion + "份" + "  " + food_cal * food_portion + "大卡");
//						}
//						adapter_list = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, items_list);
//						mListView_information.setAdapter(adapter_list);
//					}
//					catch (JSONException e)
//					{
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					super.onSuccess(response);
//				}
//
//			});
//		}
//		catch (NumberFormatException e)
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		catch (UnsupportedEncodingException e)
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		catch (JSONException e)
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		rLayout = (LinearLayout) view.findViewById(R.id.text);
//		month = (GregorianCalendar) GregorianCalendar.getInstance();
//		itemmonth = (GregorianCalendar) month.clone();
//
//		items = new ArrayList<String>();
//
//		adapter = new CalendarAdapter(getActivity(), month);
//
//		GridView gridview = (GridView) view.findViewById(R.id.gridview);
//		gridview.setAdapter(adapter);
//
//		handler = new Handler();
//		handler.post(calendarUpdater);
//
//		TextView title = (TextView) view.findViewById(R.id.title);
//		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
//
//		RelativeLayout previous = (RelativeLayout) view.findViewById(R.id.previous);
//
//		previous.setOnClickListener(new OnClickListener()
//		{
//
//			@Override
//			public void onClick(View v)
//			{
//				setPreviousMonth();
//				refreshCalendar();
//			}
//		});
//
//		RelativeLayout next = (RelativeLayout) view.findViewById(R.id.next);
//		next.setOnClickListener(new OnClickListener()
//		{
//
//			@Override
//			public void onClick(View v)
//			{
//				setNextMonth();
//				refreshCalendar();
//
//			}
//		});
//
//		gridview.setOnItemClickListener(new OnItemClickListener()
//		{
//			public void onItemClick(AdapterView<?> parent, View v, int position, long id)
//			{
//				// removing the previous view if added
//				if (((LinearLayout) rLayout).getChildCount() > 0)
//				{
//					((LinearLayout) rLayout).removeAllViews();
//				}
//				desc = new ArrayList<String>();
//				date = new ArrayList<String>();
//				((CalendarAdapter) parent.getAdapter()).setSelected(v);
//				String selectedGridDate = CalendarAdapter.dayString.get(position);
//				String[] separatedTime = selectedGridDate.split("-");
//				String gridvalueString = separatedTime[2].replaceFirst("^0*", "");// taking
//																					// last
//																					// part
//																					// of
//																					// date.
//																					// ie;
//																					// 2
//																					// from
//																					// 2012-12-02.
//				int gridvalue = Integer.parseInt(gridvalueString);
//				mSelectDate = separatedTime[2] + separatedTime[1] + separatedTime[0];
//				// Toast.makeText(getActivity(), mSelectDate,
//				// Toast.LENGTH_SHORT).show(); // 取得日期
//				// navigate to next or previous month on clicking offdays.
//				if ((gridvalue > 10) && (position < 8))
//				{
//					setPreviousMonth();
//					refreshCalendar();
//				}
//				else if ((gridvalue < 7) && (position > 28))
//				{
//					setNextMonth();
//					refreshCalendar();
//				}
//				((CalendarAdapter) parent.getAdapter()).setSelected(v);
//
//				for (int i = 0; i < Utility.startDates.size(); i++)
//				{
//					if (Utility.startDates.get(i).equals(selectedGridDate))
//					{
//						desc.add(Utility.nameOfEvent.get(i));
//					}
//				}
//				desc = null;
//				// -----//
//				items_list.clear();
//				adapter_list.clear();
//				mButton_breakfast.setBackgroundColor(Color.parseColor("#6495ed"));
//				mButton_lunch.setBackgroundResource(R.drawable.foodmenu_portion_shape);
//				mButton_dinner.setBackgroundResource(R.drawable.foodmenu_portion_shape);
//				mButton_other.setBackgroundResource(R.drawable.foodmenu_portion_shape);
//				mButton_sports.setBackgroundResource(R.drawable.foodmenu_portion_shape);
//				mButton_events.setBackgroundResource(R.drawable.foodmenu_portion_shape);
//
//				try
//				{
//					API_Funtion.food_show(88122841, gettoken(), Integer.parseInt(mSelectDate), new JsonHttpResponseHandler()
//					{
//
//						@Override
//						public void onSuccess(JSONObject response)
//						{
//							try
//							{
//								for (int i = 0; i < response.getJSONArray("breakfast").length(); i++)
//								{
//									String foodname = response.getJSONArray("breakfast").getJSONObject(i).getString("food_name");
//									int food_portion = response.getJSONArray("breakfast").getJSONObject(i).getInt("meal_portion");
//									int food_cal = response.getJSONArray("breakfast").getJSONObject(i).getInt("food_cal");
//									items_list.add(foodname + "  " + food_portion + "份" + "  " + food_cal * food_portion + "大卡");
//								}
//								adapter_list = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, items_list);
//								mListView_information.setAdapter(adapter_list);
//							}
//							catch (JSONException e)
//							{
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//							super.onSuccess(response);
//						}
//
//					});
//				}
//				catch (NumberFormatException e)
//				{
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				catch (UnsupportedEncodingException e)
//				{
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				catch (JSONException e)
//				{
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				// ------//
//			}
//		});
//
//		return view;
//	}

	private void onclick()
	{
		mButton_breakfast.setBackgroundColor(Color.parseColor("#6495ed"));
		mButton_breakfast.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				//mListView_information.setClickable(false);
				items_list.clear();
				adapter_list.clear();
				mButton_breakfast.setBackgroundColor(Color.parseColor("#6495ed"));
				mButton_lunch.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_dinner.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_other.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_sports.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_events.setBackgroundResource(R.drawable.foodmenu_portion_shape);

				try
				{
					API_Funtion.food_show(88122841, gettoken(), Integer.parseInt(mSelectDate), new JsonHttpResponseHandler()
					{

						@Override
						public void onSuccess(JSONObject response)
						{

							try
							{
								for (int i = 0; i < response.getJSONArray("breakfast").length(); i++)
								{
									String foodname = response.getJSONArray("breakfast").getJSONObject(i).getString("food_name");
									int food_portion = response.getJSONArray("breakfast").getJSONObject(i).getInt("meal_portion");
									int food_cal = response.getJSONArray("breakfast").getJSONObject(i).getInt("food_cal");
									items_list.add(foodname + "  " + food_portion + "份" + "  " + food_cal * food_portion + "大卡");
								}
								adapter_list = new ArrayAdapter(Activity_CalendarPage.this, android.R.layout.simple_list_item_1, items_list);
								mListView_information.setAdapter(adapter_list);
								mListView_information.setOnItemClickListener(null);
							}
							catch (JSONException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							super.onSuccess(response);
						}

					});
				}
				catch (NumberFormatException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (UnsupportedEncodingException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		mButton_lunch.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				//mListView_information.setEnabled(false);
				items_list.clear();
				adapter_list.clear();
				mButton_breakfast.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_lunch.setBackgroundColor(Color.parseColor("#6495ed"));
				mButton_dinner.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_other.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_sports.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_events.setBackgroundResource(R.drawable.foodmenu_portion_shape);

				try
				{
					API_Funtion.food_show(88122841, gettoken(), Integer.parseInt(mSelectDate), new JsonHttpResponseHandler()
					{

						@Override
						public void onSuccess(JSONObject response)
						{
							try
							{
								for (int i = 0; i < response.getJSONArray("lunch").length(); i++)
								{
									String foodname = response.getJSONArray("lunch").getJSONObject(i).getString("food_name");
									int food_portion = response.getJSONArray("lunch").getJSONObject(i).getInt("meal_portion");
									int food_cal = response.getJSONArray("lunch").getJSONObject(i).getInt("food_cal");
									items_list.add(foodname + "  " + food_portion + "份" + "  " + food_cal * food_portion + "大卡");
								}
								adapter_list = new ArrayAdapter(Activity_CalendarPage.this, android.R.layout.simple_list_item_1, items_list);
								mListView_information.setAdapter(adapter_list);
								mListView_information.setOnItemClickListener(null);
							}
							catch (JSONException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							super.onSuccess(response);
						}

					});
				}
				catch (NumberFormatException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (UnsupportedEncodingException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		mButton_dinner.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				//mListView_information.setEnabled(false);
				items_list.clear();
				adapter_list.clear();
				mButton_breakfast.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_lunch.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_dinner.setBackgroundColor(Color.parseColor("#6495ed"));
				mButton_other.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_sports.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_events.setBackgroundResource(R.drawable.foodmenu_portion_shape);

				try
				{
					API_Funtion.food_show(88122841, gettoken(), Integer.parseInt(mSelectDate), new JsonHttpResponseHandler()
					{

						@Override
						public void onSuccess(JSONObject response)
						{
							try
							{
								for (int i = 0; i < response.getJSONArray("dinner").length(); i++)
								{
									String foodname = response.getJSONArray("dinner").getJSONObject(i).getString("food_name");
									int food_portion = response.getJSONArray("dinner").getJSONObject(i).getInt("meal_portion");
									int food_cal = response.getJSONArray("dinner").getJSONObject(i).getInt("food_cal");
									items_list.add(foodname + "  " + food_portion + "份" + "  " + food_cal * food_portion + "大卡");
								}
								adapter_list = new ArrayAdapter(Activity_CalendarPage.this, android.R.layout.simple_list_item_1, items_list);
								mListView_information.setAdapter(adapter_list);
								mListView_information.setOnItemClickListener(null);
							}
							catch (JSONException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							super.onSuccess(response);
						}

					});
				}
				catch (NumberFormatException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (UnsupportedEncodingException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		mButton_other.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				//mListView_information.setEnabled(false);
				items_list.clear();
				adapter_list.clear();
				mButton_breakfast.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_lunch.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_dinner.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_other.setBackgroundColor(Color.parseColor("#6495ed"));
				mButton_sports.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_events.setBackgroundResource(R.drawable.foodmenu_portion_shape);

				try
				{
					API_Funtion.food_show(88122841, gettoken(), Integer.parseInt(mSelectDate), new JsonHttpResponseHandler()
					{

						@Override
						public void onSuccess(JSONObject response)
						{
							try
							{
								for (int i = 0; i < response.getJSONArray("other").length(); i++)
								{
									String foodname = response.getJSONArray("other").getJSONObject(i).getString("food_name");
									int food_portion = response.getJSONArray("other").getJSONObject(i).getInt("meal_portion");
									int food_cal = response.getJSONArray("other").getJSONObject(i).getInt("food_cal");
									items_list.add(foodname + "  " + food_portion + "份" + "  " + food_cal * food_portion + "大卡");
								}
								adapter_list = new ArrayAdapter(Activity_CalendarPage.this, android.R.layout.simple_list_item_1, items_list);
								mListView_information.setAdapter(adapter_list);
								mListView_information.setOnItemClickListener(null);
							}
							catch (JSONException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							super.onSuccess(response);
						}

					});
				}
				catch (NumberFormatException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (UnsupportedEncodingException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		mButton_sports.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				//mListView_information.setClickable(false);
				items_list.clear();
				adapter_list.clear();
				mButton_breakfast.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_lunch.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_dinner.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_other.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_sports.setBackgroundColor(Color.parseColor("#6495ed"));
				mButton_events.setBackgroundResource(R.drawable.foodmenu_portion_shape);

				try
				{
					API_Funtion.calendar_sport(88122841, gettoken(), Integer.parseInt(mSelectDate), new JsonHttpResponseHandler()
					{

						@Override
						public void onSuccess(JSONObject response)
						{
							try
							{
								for (int i = 0; i < response.getJSONArray("sport").length(); i++)
								{
									String sportname = response.getJSONArray("sport").getJSONObject(i).getString("sports_name");
									String sports_consume = response.getJSONArray("sport").getJSONObject(i).getString("sports_consume");
									int sport_cycle = response.getJSONArray("sport").getJSONObject(i).getInt("sport_cycle");
									int sports_cal = response.getJSONArray("sport").getJSONObject(i).getInt("sports_cal");
									items_list.add(sportname + " " + sports_consume + "  " + sport_cycle + " 次循環" + "  " + sport_cycle * sports_cal + " 大卡");
								}
								adapter_list = new ArrayAdapter(Activity_CalendarPage.this, android.R.layout.simple_list_item_1, items_list);
								mListView_information.setAdapter(adapter_list);
								mListView_information.setOnItemClickListener(null);
							}
							catch (JSONException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							super.onSuccess(response);
						}
					});
				}
				catch (NumberFormatException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (UnsupportedEncodingException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		mButton_events.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mListView_information.setEnabled(true);
				items_list.clear();
				adapter_list.clear();
				mButton_breakfast.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_lunch.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_dinner.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_other.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_sports.setBackgroundResource(R.drawable.foodmenu_portion_shape);
				mButton_events.setBackgroundColor(Color.parseColor("#6495ed"));
				try
				{
					API_Funtion.event_day_show(88122841, gettoken(), Integer.parseInt(mSelectDate), new JsonHttpResponseHandler()
					{

						@Override
						public void onSuccess(JSONObject response)
						{
							try
							{
								for (int i = 0; i < response.getJSONArray("event").length(); i++)
								{
									String event_name = response.getJSONArray("event").getJSONObject(i).getString("event_day");
									int event_time = response.getJSONArray("event").getJSONObject(i).getInt("event_time");
									mEvent_name.add(i,event_name);
									mEvent_time.add(i,event_time);
									Log.e("***********", response.toString());
									Log.e("----------", mEvent_name.get(i) + mEvent_time.get(i));

									if (event_time % 100 < 10)
									{
										items_list.add(event_name + "   " + event_time / 100 + ":" + "0" + event_time % 100);
									}
									else
									{
										items_list.add(event_name + "   " + event_time / 100 + ":" + event_time % 100);
									}

								}
								adapter_list = new ArrayAdapter(Activity_CalendarPage.this, android.R.layout.simple_list_item_1, items_list);
//								mListView_information.setOnItemClickListener(new OnItemClickListener()
//								{
//
//									@Override
//									public void onItemClick(AdapterView<?> parent, View view, int position, long id)
//									{
//										openDialog_delete(position);
//									}
//								});
								mListView_information.setAdapter(adapter_list);
							}
							catch (JSONException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							super.onSuccess(response);
						}

					});
				}
				catch (NumberFormatException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (UnsupportedEncodingException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				mListView_information.setOnItemClickListener(new OnItemClickListener()
				{
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id)
					{
						//Toast.makeText(Activity_CalendarPage.this, ""+position, Toast.LENGTH_LONG).show();
						openDialog_delete(position);

					}
				});
			}
		});
	}

	protected void setNextMonth()
	{
		if (month.get(GregorianCalendar.MONTH) == month.getActualMaximum(GregorianCalendar.MONTH))
		{
			month.set((month.get(GregorianCalendar.YEAR) + 1), month.getActualMinimum(GregorianCalendar.MONTH), 1);
		}
		else
		{
			month.set(GregorianCalendar.MONTH, month.get(GregorianCalendar.MONTH) + 1);
		}

	}

	protected void setPreviousMonth()
	{
		if (month.get(GregorianCalendar.MONTH) == month.getActualMinimum(GregorianCalendar.MONTH))
		{
			month.set((month.get(GregorianCalendar.YEAR) - 1), month.getActualMaximum(GregorianCalendar.MONTH), 1);
		}
		else
		{
			month.set(GregorianCalendar.MONTH, month.get(GregorianCalendar.MONTH) - 1);
		}

	}

	public void refreshCalendar()
	{
		TextView title = (TextView) findViewById(R.id.title);

		adapter.refreshDays();
		adapter.notifyDataSetChanged();
		handler.post(calendarUpdater); // generate some calendar items

		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
	}

	public Runnable calendarUpdater = new Runnable()
	{

		@Override
		public void run()
		{
		}
	};

	// 自訂的ExpandListAdapter
	class ExpandableAdapter extends BaseExpandableListAdapter
	{
		private Context context;
		List<Map<String, String>> groups;
		List<List<Map<String, String>>> childs;

		/*
		 * 構造函數: 參數1:context物件 參數2:一級清單資料來源 參數3:二級清單資料來源
		 */
		public ExpandableAdapter(Context context, List<Map<String, String>> groups, List<List<Map<String, String>>> childs)
		{
			this.groups = groups;
			this.childs = childs;
			this.context = context;
		}

		public Object getChild(int groupPosition, int childPosition)
		{
			return childs.get(groupPosition).get(childPosition);
		}

		public long getChildId(int groupPosition, int childPosition)
		{
			return childPosition;
		}

		// 獲取二級清單的View物件
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent)
		{
			@SuppressWarnings("unchecked")
			String text = ((Map<String, String>) getChild(groupPosition, childPosition)).get("child");
			LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			// 獲取二級清單對應的佈局檔, 並將其各元素設置相應的屬性
			LinearLayout linearLayout = (LinearLayout) layoutInflater.inflate(R.layout.child, null);
			TextView tv = (TextView) linearLayout.findViewById(R.id.child_tv);
			tv.setText(text);

			return linearLayout;
		}

		public int getChildrenCount(int groupPosition)
		{
			return childs.get(groupPosition).size();
		}

		public Object getGroup(int groupPosition)
		{
			return groups.get(groupPosition);
		}

		public int getGroupCount()
		{
			return groups.size();
		}

		public long getGroupId(int groupPosition)
		{
			return groupPosition;
		}

		// 獲取一級清單View物件
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
		{
			String text = groups.get(groupPosition).get("group");
			LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			// 獲取一級清單佈局檔,設置相應元素屬性
			LinearLayout linearLayout = (LinearLayout) layoutInflater.inflate(R.layout.group, null);
			TextView textView = (TextView) linearLayout.findViewById(R.id.group_tv);
			textView.setText(text);

			return linearLayout;
		}

		public boolean hasStableIds()
		{
			return false;
		}

		public boolean isChildSelectable(int groupPosition, int childPosition)
		{
			return true;
		}

	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 1, "新增待辦").setShowAsAction(2);
        return true;
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case 0:
				openDialog();
				break;
			case android.R.id.home:            
		         Intent intent = new Intent(this, Activity_index_page.class);            
		         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
		         startActivity(intent);
		         Activity_CalendarPage.this.finish();
		         return true;        
		     default:            
		         return super.onOptionsItemSelected(item); 
		}
		return super.onOptionsItemSelected(item);
	}

	private void openDialog()
	{
		final TimePicker mTimePicker;
		final EditText mEditText;
		AlertDialog.Builder dialog = new AlertDialog.Builder(Activity_CalendarPage.this);
		dialog.setTitle("新增待辦事項");
		LayoutInflater inflater = Activity_CalendarPage.this.getLayoutInflater();
		View layout = inflater.inflate(R.layout.dialog_addevent, (ViewGroup) Activity_CalendarPage.this.findViewById(R.id.dialog));
		mTimePicker = (TimePicker) layout.findViewById(R.id.timePicker1);
		mEditText = (EditText) layout.findViewById(R.id.edit_event);
		dialog.setView(layout);
		mTimePicker.setOnTimeChangedListener(timechang);
		dialog.setPositiveButton("取消", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.cancel();
			}
		});

		dialog.setNegativeButton("新增", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				try
				{
					API_Funtion.event_day_add(88122841, gettoken(), mEditText.getText().toString(), Integer.parseInt(mSelectDate), mHour * 100 + mMin, new JsonHttpResponseHandler()
					{

						@SuppressLint("NewApi")
						@Override
						public void onSuccess(JSONObject response)
						{
//							mEvent_name.add( mEditText.getText().toString());
//							mEvent_time.add(mHour * 100 + mMin);
							//Toast.makeText(getActivity(), "成功新增", Toast.LENGTH_LONG).show();
							super.onSuccess(response);
							mButton_events.callOnClick();
						}
					});
				}
				catch (UnsupportedEncodingException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		dialog.show();
	}

	public TimePicker.OnTimeChangedListener timechang = new TimePicker.OnTimeChangedListener()
	{

		@Override
		public void onTimeChanged(TimePicker view, int hourOfDay, int minute)
		{
			mHour = hourOfDay;
			mMin = minute;
		}
	};

	private void openDialog_delete(final int position)
	{
		AlertDialog.Builder dialog_delete = new AlertDialog.Builder(Activity_CalendarPage.this);
		dialog_delete.setTitle("刪除事項");
		dialog_delete.setMessage("確認是否刪除此事項？");
		dialog_delete.setPositiveButton("返回", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.cancel();
			}
		});

		dialog_delete.setNegativeButton("刪除", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				// Toast.makeText(getActivity(), "ok"+position,
				// Toast.LENGTH_LONG).show();
				try
				{
					API_Funtion.event_delete(88122841, gettoken(), mEvent_name.get(position), Integer.parseInt(mSelectDate), mEvent_time.get(position), new JsonHttpResponseHandler()
					{

						@Override
						public void onSuccess(JSONObject response)
						{
							 //Toast.makeText(getActivity(), ""+mEvent_name.get(position),Toast.LENGTH_LONG).show();
							mEvent_name.remove(position);
							mEvent_time.remove(position);
							items_list.remove(position);
							adapter_list = new ArrayAdapter(Activity_CalendarPage.this, android.R.layout.simple_list_item_1, items_list);
							mListView_information.setAdapter(adapter_list);
							super.onSuccess(response);
						}

					});

				}
				catch (NumberFormatException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (UnsupportedEncodingException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		dialog_delete.show();
	}

	private String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = Activity_CalendarPage.this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			Intent intent = new Intent();
			intent.setClass(Activity_CalendarPage.this, Activity_index_page.class);
			startActivity(intent);
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}
}
