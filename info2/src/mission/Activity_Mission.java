package mission;

import index_page.Activity_index_page;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.json.JSONException;
import org.json.JSONObject;

import sportPage.Sportpage;

import com.example.hellochart.R;
import com.example.informationPage.MainActivity;
import com.loopj.android.http.JsonHttpResponseHandler;

import API_Funtion.ClassAPI;
import android.opengl.Visibility;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Activity_Mission extends Activity
{

	private ImageView mImageViewMission1;
	private ImageView mImageViewMission2;
	private ImageView mImageViewMission3;
	private ImageView mImageViewMission4;
	private ImageView mImageViewMission5;
	private TextView mTextViewMission1Content1;
	private TextView mTextViewShowAchievement;

	private ClassAPI API_Funtion = new ClassAPI(Activity_Mission.this);
	int Achievement_point = 0;
	int foodflag = 0;
	int sportflag = 0;
	int weightflag = 0;
	int daly_enjoyflag = 0;
	int newsflag = 0;
	String meals_null_breakfast,meals_null_lunch,meals_null_dinner;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mission);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#ef3a5f8a"));
		getActionBar().setBackgroundDrawable(actionbar_color);
		findview();
		try
		{
			//----------------------------//
			API_Funtion.mission_show(88122841, gettoken(), API_Funtion.GetTimestamp(), new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					super.onSuccess(response);
					try
					{
//						if(response.getJSONArray("task").getJSONObject(0).getInt("data")==1)
//						{
//							foodflag = getfoodflag();
//							setMissonOne();
//						}
						if(response.getJSONArray("task").getJSONObject(1).getInt("data")==1)
						{
							
							setMissonTwo();
						}
						if(response.getJSONArray("task").getJSONObject(2).getInt("data")==1)
						{
							
							setMissonThree();
						}
						if(response.getJSONArray("task").getJSONObject(3).getInt("data")==1)
						{
							
							setMissonFour();
						}
						if(response.getJSONArray("task").getJSONObject(4).getInt("data")==1)
						{
							
							setMissonFive();
						}
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		foodflag = getfoodflag();
		if (foodflag != 0)
		{
			setMissonOne();
		}
//		if (sportflag == 1)
//		{
//			setMissonTwo();
//		}
//		if (weightflag == 1)
//		{
//			setMissonThree();
//		}
//		if (daly_enjoyflag == 1)
//		{
//			setMissonFour();
//		}
//		if (newsflag == 1)
//		{
//			setMissonFive();
//		}
		mTextViewShowAchievement.setText("Achievement point:" + Achievement_point);
	}

	private void setMissonOne()
	{
		// TODO Auto-generated method stub
		if (foodflag > 0 && foodflag < 3)
		{
			mTextViewMission1Content1.setText("Recording your all day meals(" + foodflag + "/3)");
		}
		else
		{

			mTextViewMission1Content1.setText("Recording your all day meals(" + foodflag + "/3)");
			Achievement_point++;
			//mTextViewShowAchievement.setText("Achievement point:" + Achievement_point);
			mImageViewMission1.setAlpha((float) 0.8);
		}
	}

	private void setMissonTwo()
	{
		// TODO Auto-generated method stub
		mImageViewMission2.setAlpha((float) 0.8);
		Achievement_point++;
	}

	private void setMissonThree()
	{
		// TODO Auto-generated method stub
		mImageViewMission3.setAlpha((float) 0.8);
		Achievement_point++;
	}

	private void setMissonFour()
	{
		// TODO Auto-generated method stub
		mImageViewMission4.setAlpha((float) 0.8);
		Achievement_point++;
	}

	private void setMissonFive()
	{
		// TODO Auto-generated method stub
		mImageViewMission5.setAlpha((float) 0.8);
		Achievement_point++;
	}

	private void findview()
	{

		mTextViewMission1Content1 = (TextView) findViewById(R.id.textView_dailyM_Fdcontent);
		// ///////////////////////////////////
		mTextViewShowAchievement = (TextView) findViewById(R.id.textView_dailyM_point);
		mImageViewMission1 = (ImageView) findViewById(R.id.imageView_m1);
		mImageViewMission2 = (ImageView) findViewById(R.id.imageView_m2);
		mImageViewMission3 = (ImageView) findViewById(R.id.imageView_m3);
		mImageViewMission4 = (ImageView) findViewById(R.id.imageView_m4);
		mImageViewMission5 = (ImageView) findViewById(R.id.imageView_m5);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			Intent intent = new Intent();
			intent.setClass(Activity_Mission.this, Activity_index_page.class);
			startActivity(intent);
			this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case android.R.id.home:
				Intent intent = new Intent(this, Activity_index_page.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				this.finish();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = Activity_Mission.this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}

	private int getfoodflag()
	{
		Bundle bundle =this.getIntent().getExtras();
		int flag = bundle.getInt("foodflag");
		return flag;
	}
}
