package getinfoPage;

import com.crashlytics.android.Crashlytics;
import index_page.Activity_index_page;

import io.fabric.sdk.android.Fabric;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import sportPage.Sportpage;
import widget.AppWidgetExample;

import com.example.hellochart.R;
import com.example.informationPage.Activity_selectFoodPage;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Session.StatusCallback;
import com.facebook.model.GraphUser;
import com.loopj.android.http.JsonHttpResponseHandler;


import API_Funtion.ClassAPI;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Activity_about extends Activity
{
	int sport_cal, sport_cycle,mCaloriseTotal = 0;
	int foodcal ;
	int max ;
	private ClassAPI API_Funtion = new ClassAPI(Activity_about.this);
	String mName, mUid, mGender;
	private String mTokenFileName = "Token"; // ?????W
	private int readed; // ?w??????
	String mToken = null; // ???e
	byte[] buff = new byte[256]; // input stream buffer
	private int tmp = 0;
	
	private Button btn;
	
	private View mView;
	private ViewPager mViewPager;
	private ArrayList<View> mPageViews;
	private ImageView mImageView;
	private ImageView[] mImageViews;
	
	private ViewGroup mainViewGroup;
	private ViewGroup indicatorViewGroup;
	
	private TextView mButton_login;
	LayoutInflater mInflater;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_about);
		Fabric.with(this, new Crashlytics());
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        if(isNetworkConnected()==true)
		{
//        	String fileName_1 = "Skip";
//			getDir(fileName_1, Context.MODE_PRIVATE);
//			try
//			{
//				FileOutputStream writer_1 = openFileOutput(fileName_1, Context.MODE_PRIVATE);
//				writer_1.write(String.valueOf(0).getBytes());
//				writer_1.close();
//			}
//			catch (IOException e)
//			{
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
		}
		else
		{
			AlertDialog.Builder ad = new AlertDialog.Builder(this);
			ad.setTitle("尚未連接網路連線");
			ad.setMessage("請確認 WI-FI 或 行動網路 已連接，再重啟程式！");
			ad.setPositiveButton("離開", new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int i)
				{
					finish();
				}
			});
			ad.show();
			//Toast.makeText(LoginPage.this, "??T?{?w?s???????s?u!", Toast.LENGTH_LONG).show();
			//this.finish();
		}
		
		
		// Input stream
		try
		{
			FileInputStream reader = openFileInput(mTokenFileName);
			while ((readed = reader.read(buff)) != -1)
			{
				mToken += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		//food_show();

		if (mToken != null && isNetworkConnected()==true)
		{
			//Log.e("-------------", ""+max+" "+foodcal+" "+sport_cal);
//			Intent intent = new Intent(AppWidgetExample.ACTION_TEXT_CHANGED);
//			intent.putExtra("foodcal", 1500);
//			intent.putExtra("sportcal", 2100);
//			intent.putExtra("max", 2800);
//			getApplicationContext().sendBroadcast(intent);
			
			Intent info_intent = new Intent();
			info_intent.setClass(Activity_about.this, Activity_index_page.class);
			startActivity(info_intent);
			System.gc();
			finish();
		}
		else
		{
		}
        
        mInflater = getLayoutInflater();
        
        mainViewGroup = (ViewGroup) mInflater.inflate(R.layout.activity_about, null);
        
        mPageViews = new ArrayList<View>();
        mPageViews.add(mInflater.inflate(R.layout.about00, null));
        mPageViews.add(mInflater.inflate(R.layout.about01, null));
        mPageViews.add(mInflater.inflate(R.layout.about02, null));
        mPageViews.add(mInflater.inflate(R.layout.about03, null));
        mPageViews.add(mInflater.inflate(R.layout.about04, null));
        mPageViews.add(mInflater.inflate(R.layout.about05, null));
        mPageViews.add(mInflater.inflate(R.layout.about06, null));
        mPageViews.add(mInflater.inflate(R.layout.about07, null));
        
        mButton_login = (TextView)mainViewGroup.findViewById(R.id.btn_start_1);
        mButton_login.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				Facebooklogin();
			}
		});
        mImageViews = new ImageView[mPageViews.size()];
        
        
        mViewPager = (ViewPager) mainViewGroup.findViewById(R.id.myviewpager);
        indicatorViewGroup = (ViewGroup) mainViewGroup.findViewById(R.id.mybottomviewgroup);
        
        for (int i = 0; i < mImageViews.length; i++) {
        	mImageView = new ImageView(Activity_about.this);  
        	mImageView.setLayoutParams(new LayoutParams(20,20));  
        	mImageView.setPadding(20, 0, 20, 0);
        	
        	if (i == 0) {
        		mImageView.setBackgroundResource(R.drawable.blue_dot);				
			} else {
				mImageView.setBackgroundResource(R.drawable.gray_dot);
			}
			
        	mImageViews[i] = mImageView;
        	
        	indicatorViewGroup.addView(mImageViews[i]);
		}
        
        setContentView(mainViewGroup);
        
        
        mViewPager.setAdapter(new MyPagerAdapter());
        mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				for (int i = 0; i < mImageViews.length; i++) {
					if(i == arg0) {
						mImageViews[i].setBackgroundResource(R.drawable.blue_dot);
					} else {
						mImageViews[i].setBackgroundResource(R.drawable.gray_dot);
					}
				}
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
        
    }
    
    public void Facebooklogin()
	{
		Session.openActiveSession(this, true, new StatusCallback()
		{
			@Override
			public void call(Session session, SessionState state, Exception exception)
			{
				if (session == Session.getActiveSession())
				{
					if (session.isOpened())
					{
						mToken = session.getAccessToken();
						// Toast.makeText(MainActivity.this,
						// session.getAccessToken(),Toast.LENGTH_LONG).show();
						Request.newMeRequest(session, new GraphUserCallback()
						{

							@Override
							public void onCompleted(GraphUser user, Response response)
							{
								// TODO Auto-generated method stub
								if (user != null)
								{
									mName = user.getName();
									mUid = user.getId();
									mGender = user.getProperty("gender").toString();
									// final Intent info_intent = new Intent();
									final Intent sign_intent = new Intent();
									final Intent info_intent = new Intent();
									sign_intent.setClass(Activity_about.this, getOtherinf.class);
									info_intent.setClass(Activity_about.this, Activity_index_page.class);
									Bundle bundle = new Bundle();
									bundle.putString("key_Name", mName);
									bundle.putString("key_Gender", mGender);
									bundle.putString("key_Uid", mUid);
									sign_intent.putExtras(bundle);
									// Toast.makeText(LoginPage.this, "login",
									// Toast.LENGTH_LONG).show();
									final ProgressDialog ringProgressDialog = ProgressDialog.show(Activity_about.this, "Please wait ...", "Data Loading ...", true);
									ringProgressDialog.setCancelable(true);
									try
									{
										API_Funtion.login(88122841, mUid, new JsonHttpResponseHandler()
										{

											@Override
											public void onSuccess(JSONObject response)
											{
												// Toast.makeText(LoginPage.this,
												// "login11",
												// Toast.LENGTH_LONG).show();
												// Toast.makeText(LoginPage.this,
												// "login",
												// Toast.LENGTH_LONG).show();
												try
												{
													if (response.getString("msg").equals("Cant find the user"))
													{
														// Toast.makeText(LoginPage.this,
														// "error",
														// Toast.LENGTH_LONG).show();
														startActivity(sign_intent);
														Activity_about.this.finish();
													}
													else
													{
//														 Toast.makeText(LoginPage.this,
//														 "error",
//														 Toast.LENGTH_LONG).show();
														// ?stoken
														getDir(mTokenFileName, Context.MODE_PRIVATE);
														FileOutputStream writer = openFileOutput(mTokenFileName, Context.MODE_PRIVATE);
														writer.write(response.getString("session_token").getBytes());
														writer.close();

														//Log.e("*********", "wtf");
														startActivity(info_intent);
														Activity_about.this.finish();
														// Toast.makeText(LoginPage.this,
														// "error1111",
														// Toast.LENGTH_LONG).show();
													}
													super.onSuccess(response);
												}
												catch (JSONException e)
												{
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
												catch (FileNotFoundException e)
												{
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
												catch (IOException e)
												{
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
											}
										});
									}
									catch (UnsupportedEncodingException e)
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									catch (JSONException e)
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									// Toast.makeText(MainActivity.this,
									// " "+mName+" "+mUid+" "+mGender,
									// Toast.LENGTH_LONG).show();

								}
							}
						}).executeAsync();
					}
				}
			}
		});
	}
    
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}

	private boolean isNetworkConnected()
	{
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null)
		{
			return false;
		}
		else
		{
			return true;
		}

	}
    
    class MyPagerAdapter extends PagerAdapter {

    	@Override  
        public int getCount() {  
            return mPageViews.size();  
        }  
  
        @Override  
        public boolean isViewFromObject(View arg0, Object arg1) {  
            return arg0 == arg1;  
        }  
  
        @Override  
        public int getItemPosition(Object object) {  
            // TODO Auto-generated method stub  
            return super.getItemPosition(object);  
        }  
  
        @Override  
        public void destroyItem(View arg0, int arg1, Object arg2) {  
            // TODO Auto-generated method stub  
            ((ViewPager) arg0).removeView(mPageViews.get(arg1));  
        }  
  
        @Override  
        public Object instantiateItem(View arg0, int arg1) {  
            // TODO Auto-generated method stub  
            ((ViewPager) arg0).addView(mPageViews.get(arg1));  
            return mPageViews.get(arg1);  
        }  
  
        @Override  
        public void restoreState(Parcelable arg0, ClassLoader arg1) {  
            // TODO Auto-generated method stub  
  
        }  
  
        @Override  
        public Parcelable saveState() {  
            // TODO Auto-generated method stub  
            return null;  
        }  
  
        @Override  
        public void startUpdate(View arg0) {  
            // TODO Auto-generated method stub  
  
        }  
  
        @Override  
        public void finishUpdate(View arg0) {  
            // TODO Auto-generated method stub  
  
        } 
    	
    }

    private void getWidgetData()
    {
    	try
		{
			API_Funtion.calendar_sport(88122841, gettoken(),API_Funtion.GetTimestamp(), new JsonHttpResponseHandler()
			{
				@Override
				public void onSuccess(JSONObject response)
				{
					// TODO Auto-generated method stub
					super.onSuccess(response);
					try
					{
						for (int i = 0; i < response.getJSONArray("sport").length(); i++)
						{
							sport_cal = response.getJSONArray("sport").getJSONObject(i).getInt("sports_cal");
							sport_cycle = response.getJSONArray("sport").getJSONObject(i).getInt("sport_cycle");
							//mArrayListConsumeCalorise.add(sport_cal * sport_cycle);
							mCaloriseTotal += sport_cal * sport_cycle;						
						}
						// Toast.makeText(Sportpage.this,""+mArrayListUserSportId,Toast.LENGTH_LONG).show();

					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			});
			
			API_Funtion.food_show(88122841, gettoken(), API_Funtion.GetTimestamp(), new JsonHttpResponseHandler()
			{

				@Override
				public void onSuccess(JSONObject response)
				{
					try
					{
						foodcal = response.getInt("foodcal_all");
						max = response.getInt("user_cal");
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					super.onSuccess(response);
				}
			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = Activity_about.this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}
}
