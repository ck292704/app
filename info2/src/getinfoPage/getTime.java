package getinfoPage;

import index_page.Activity_index_page;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.TimeZone;

import org.apache.http.conn.ClientConnectionManager;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.hellochart.R;
import com.example.informationPage.MainActivity;
import com.loopj.android.http.JsonHttpResponseHandler;

import API_Funtion.ClassAPI;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

public class getTime extends Activity
{
	private ClassAPI API_Funtion = new ClassAPI(getTime.this);
	private int mHour, mMin;
	private Button mButtonBtime, mButtonLtime, mButtonDtime, mButtonFinish;
	static final int mB_TIMEPICKER = 0;
	static final int mL_TIMEPICKER = 1;
	static final int mD_TIMEPICKER = 2;
	int needcal;
	String mLevel;
	int mAge;
	String mName, mGender, mWorkPower, mUid, mToken;
	String mBtime, mLtime, mDtime;
	double mHeight, mWeight;
	int mGenderCode;

	// User Information

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gettime);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#df3a5f8a"));
		getActionBar().setBackgroundDrawable(actionbar_color);
		iniView();
		setListener();
		bundle();
		makeGenderCode();
		// Toast.makeText(getTime.this,""+mToken,Toast.LENGTH_LONG).show();
		if (mLevel.equals("0"))
		{
			mWorkPower = "Weak";
		}
		else if (mLevel.equals("1"))
		{
			mWorkPower = "Medium";

		}
		else
		{
			mWorkPower = "Strong";
		}

	}

	private void makeGenderCode()
	{
		// TODO Auto-generated method stub
		if (mGender.equals("male"))
		{
			mGenderCode = 0;
		}
		else if (mGender.equals("female"))
		{
			mGenderCode = 1;
		}
	}

	private void iniView()
	{
		mButtonBtime = (Button) findViewById(R.id.btn_btime);
		mButtonLtime = (Button) findViewById(R.id.btn_ltime);
		mButtonDtime = (Button) findViewById(R.id.btn_dtime);
		mButtonFinish = (Button) findViewById(R.id.btn_next3);
	}

	private void setListener()
	{
		mButtonBtime.setOnClickListener(btimepick);
		mButtonLtime.setOnClickListener(ltimepick);
		mButtonDtime.setOnClickListener(dtimepick);
		mButtonFinish.setOnClickListener(finish);
	}

	private void bundle()
	{
		Bundle bundle = this.getIntent().getExtras();
		mName = bundle.getString("key_Name");
		mUid = bundle.getString("key_Uid");
		mToken = bundle.getString("key_token");
		mGender = bundle.getString("key_Gender");
		mLevel = bundle.getString("key_Level");
		mAge = bundle.getInt("key_Age");
		mHeight = bundle.getDouble("key_Height");
		mWeight = bundle.getDouble("key_Weight");
	}

	private OnClickListener finish = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			// TODO Auto-generated method stub
			if (mBtime == null || mLtime == null || mDtime == null)
			{
				Toast.makeText(getTime.this, "Cheak you're notification time", Toast.LENGTH_LONG).show();
			}
			else
			{
				new AlertDialog.Builder(getTime.this).setTitle("Cheak Information").setIcon(R.drawable.ic_launcher).setMessage("Name:" + mName + "\nGender:" + mGender + "\nAge:" + mAge + "\nHeight:" + mHeight + "Cm\nWeight:" + mWeight + "Kg\nWorkPower:" + mWorkPower + "\nYou're notification time:\n" + mBtime + "\t" + mLtime + "\t" + mDtime).setPositiveButton("Cancel", new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						// TODO Auto-generated method stub

					}
				}).setNegativeButton("Ok", new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						if (mGender.equals("male"))
						{
							mGender = "0";
							needcal = (int) (66 + (13.7 * mWeight) + (5 * mHeight) - (6.8 * mAge));
						}
						else
						{
							mGender = "1";
							needcal = (int) (655 + (9.6 * mWeight) + (1.8 * mHeight) - (4.7 * mAge));
						}

						if (mLevel.equals("0"))
						{
							needcal =  (int) (needcal * 1.2);
						}
						else if (mLevel.equals("1"))
						{
							needcal =  (int) (needcal * 1.55);
						}
						else
						{
							needcal =  (int) (needcal * 1.9);
						}
						// json && new Activity;
						try
						{

							API_Funtion.user_sign(88122841, mUid, mGender, mAge, mWeight, mHeight, mName, mLevel,needcal, new JsonHttpResponseHandler("utf8")
							{
								@Override
								public void onSuccess(JSONObject response)
								{
									try
									{
										// 存token
										String fileName = "Token";
										getDir(fileName, Context.MODE_PRIVATE);
										FileOutputStream writer = openFileOutput(fileName, Context.MODE_PRIVATE);
										writer.write(response.getString("session_token").getBytes());
										writer.close();

										// 
										String fileName_1 = "Skip";
										getDir(fileName_1, Context.MODE_PRIVATE);
										FileOutputStream writer_1 = openFileOutput(fileName_1, Context.MODE_PRIVATE);
										writer_1.write(String.valueOf(0).getBytes());
										writer_1.close();
										
										//
										String fileName_2 = "Skip_sport";
										getDir(fileName_2, Context.MODE_PRIVATE);
										FileOutputStream writer_2 = openFileOutput(fileName_2, Context.MODE_PRIVATE);
										writer_2.write(String.valueOf(0).getBytes());
										writer_2.close();
										//Toast.makeText(getTime.this, ""+needcal, Toast.LENGTH_LONG).show();

										API_Funtion.weight_day(88122841, response.getString("session_token"), mWeight,0,mHeight, new JsonHttpResponseHandler()
										{

											@Override
											public void onSuccess(JSONObject response)
											{
												Intent intent = new Intent();
												intent.setClass(getTime.this, Activity_index_page.class);
												startActivity(intent);
												getTime.this.finish();
												super.onSuccess(response);
											}

										});

										// Toast.makeText(getTime.this,
										// response.getString("msg"),
										// Toast.LENGTH_LONG).show();

									}
									catch (JSONException e)
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									catch (IOException e)
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									super.onSuccess(response);
								}

							});
						}
						catch (UnsupportedEncodingException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						catch (JSONException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				})

				.show();

			}
		}
	};

	private OnClickListener btimepick = new OnClickListener()
	{

		@Override
		public void onClick(View v)
		{
			// TODO Auto-generated method stub
			final Calendar c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 6);
			c.set(Calendar.MINUTE,00);
			mHour = c.get(Calendar.HOUR_OF_DAY);		
			mMin = c.get(Calendar.MINUTE);
			showDialog(mB_TIMEPICKER);
		}
	};

	private OnClickListener ltimepick = new OnClickListener()
	{

		@Override
		public void onClick(View v)
		{
			// TODO Auto-generated method stub
			final Calendar c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 12);
			c.set(Calendar.MINUTE,00);
			mHour = c.get(Calendar.HOUR_OF_DAY);
			mMin = c.get(Calendar.MINUTE);
			showDialog(mL_TIMEPICKER);
		}
	};

	private OnClickListener dtimepick = new OnClickListener()
	{

		@Override
		public void onClick(View v)
		{
			// TODO Auto-generated method stub
			final Calendar c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 18);
			c.set(Calendar.MINUTE,00);
			mHour = c.get(Calendar.HOUR_OF_DAY);
			mMin = c.get(Calendar.MINUTE);
			showDialog(mD_TIMEPICKER);
		}
	};

	protected Dialog onCreateDialog(int id)
	{
		switch (id)
		{
			case mB_TIMEPICKER:
				return new TimePickerDialog(this, B_TimeSetListener, mHour, mMin, false);
			case mL_TIMEPICKER:
				return new TimePickerDialog(this, L_TimeSetListener, mHour, mMin, false);
			case mD_TIMEPICKER:
				return new TimePickerDialog(this, D_TimeSetListener, mHour, mMin, false);
		}
		return null;

	}

	private TimePickerDialog.OnTimeSetListener B_TimeSetListener = new TimePickerDialog.OnTimeSetListener()
	{

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute)
		{
			// TODO Auto-generated method stub
			mBtime = "" + String.valueOf(hourOfDay) + "" + ":" + String.valueOf(minute);
			mButtonBtime.setText("Breakfast Time\n\n "+mBtime);			
			View Viewline1=(View)findViewById(R.id.viewline1);
			Viewline1.setAlpha(255);
			
			AlarmManager alarmManager1 = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
			Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
			PendingIntent pendingIntent1 = PendingIntent.getBroadcast(getBaseContext(), 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			long firstTime = SystemClock.elapsedRealtime();
			long systemTime = System.currentTimeMillis(); 
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			cal.setTimeZone(TimeZone.getTimeZone("GMT+8"));
			cal.set(Calendar.HOUR_OF_DAY,hourOfDay);
			cal.set(Calendar.MINUTE,minute);
			cal.set(Calendar.SECOND,0);
			//Toast.makeText(getTime.this, ""+hourOfDay+":"+minute, Toast.LENGTH_LONG).show();
			long selectTime1 = cal.getTimeInMillis();
			if(systemTime > selectTime1) {  
				//Toast.makeText(MainActivity.this,"设置的时间小于当前时间1", Toast.LENGTH_SHORT).show();  
				cal.add(Calendar.DAY_OF_MONTH, 1);  
				selectTime1 = cal.getTimeInMillis();  
			}
			long time1 = selectTime1 - systemTime;  
			firstTime += time1;
			alarmManager1.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,  
	                firstTime, 24*60*60*1000, pendingIntent1);
		}
		
	};

	private TimePickerDialog.OnTimeSetListener L_TimeSetListener = new TimePickerDialog.OnTimeSetListener()
	{

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute)
		{
			// TODO Auto-generated method stub
			mLtime = "" + String.valueOf(hourOfDay) + "" + ":" + String.valueOf(minute);			
			mButtonLtime.setText("Lunch Time\n\n "+mLtime);			
			View Viewline2=(View)findViewById(R.id.viewline2);
			Viewline2.setAlpha(255);
			
			AlarmManager alarmManager2 = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
			Intent intent2 = new Intent(getBaseContext(), AlarmReceiver.class);
			PendingIntent pendingIntent2 = PendingIntent.getBroadcast(getBaseContext(), 2, intent2, PendingIntent.FLAG_CANCEL_CURRENT);
			long firstTime2 = SystemClock.elapsedRealtime();
			long systemTime = System.currentTimeMillis(); 
			Calendar cal2 = Calendar.getInstance();
			cal2.setTimeInMillis(System.currentTimeMillis());
			cal2.setTimeZone(TimeZone.getTimeZone("GMT+8"));
			cal2.set(Calendar.HOUR_OF_DAY,hourOfDay);
			cal2.set(Calendar.MINUTE,minute);
			cal2.set(Calendar.SECOND,0);
			long selectTime2 = cal2.getTimeInMillis();
			if(systemTime > selectTime2) {  
				//Toast.makeText(MainActivity.this,"设置的时间小于当前时间1", Toast.LENGTH_SHORT).show();  
				cal2.add(Calendar.DAY_OF_MONTH, 1);  
				selectTime2 = cal2.getTimeInMillis();  
			}
			long time2 = selectTime2 - systemTime;  
			firstTime2 += time2;
			alarmManager2.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,  
	                firstTime2, 24*60*60*1000, pendingIntent2);
			//Toast.makeText(getTime.this,"ok1", Toast.LENGTH_SHORT).show();
		}
	};

	private TimePickerDialog.OnTimeSetListener D_TimeSetListener = new TimePickerDialog.OnTimeSetListener()
	{

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute)
		{
			// TODO Auto-generated method stub
			mDtime = "" + String.valueOf(hourOfDay) + "" + ":" + String.valueOf(minute);
			mButtonDtime.setText("Dinner Time\n\n "+mDtime);
			View Viewline3=(View)findViewById(R.id.viewline3);
			Viewline3.setAlpha(255);
			
			AlarmManager alarmManager3 = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
			Intent intent3 = new Intent(getBaseContext(), AlarmReceiver.class);
			PendingIntent pendingIntent3 = PendingIntent.getBroadcast(getBaseContext(), 3, intent3, PendingIntent.FLAG_CANCEL_CURRENT);
			long firstTime3 = SystemClock.elapsedRealtime();
			long systemTime = System.currentTimeMillis(); 
			Calendar cal3 = Calendar.getInstance();
			cal3.setTimeInMillis(System.currentTimeMillis());
			cal3.setTimeZone(TimeZone.getTimeZone("GMT+8"));
			cal3.set(Calendar.HOUR_OF_DAY,hourOfDay);
			cal3.set(Calendar.MINUTE,minute);
			cal3.set(Calendar.SECOND,0);
			long selectTime3 = cal3.getTimeInMillis();
			if(systemTime > selectTime3) {  
				//Toast.makeText(getTime.this,"设置的时间小于当前时间1", Toast.LENGTH_SHORT).show();  
				cal3.add(Calendar.DAY_OF_MONTH, 1);  
				selectTime3 = cal3.getTimeInMillis();  
			}
			long time3 = selectTime3 - systemTime;  
			firstTime3 += time3;
			alarmManager3.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,  
	                firstTime3, 24*60*60*1000, pendingIntent3);
		}
	};
	
}
