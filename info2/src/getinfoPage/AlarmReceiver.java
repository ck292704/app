package getinfoPage;

import index_page.Activity_index_page;

import com.example.hellochart.R;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver {
	
	private static final int MY_NOTIFICATION_ID=1;
	NotificationManager notificationManager;
	Notification myNotification;
	Bitmap remote_picture = null;

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public void onReceive(Context context, Intent intent) {
		//Toast.makeText(context, "Alarm received!", Toast.LENGTH_LONG).show();

		 Intent myIntent = new Intent(Intent.ACTION_VIEW, null);
	    myIntent.setClass(context, Activity_index_page.class);
	    PendingIntent pendingIntent = PendingIntent.getActivity(
	    	      context, 
	    	      0, 
	    	      myIntent, 
	    	      Intent.FLAG_ACTIVITY_NEW_TASK);
	    
//	    myNotification = new NotificationCompat.Builder(context)
//	    		.setContentTitle("吃飯時間到!")
//	    		.setContentText("該吃飯囉")
//	    		.setTicker("Life Recorded 提醒")
//	    		.setWhen(System.currentTimeMillis())
//	    		.setContentIntent(pendingIntent)
//	    		.setDefaults(Notification.DEFAULT_SOUND)
//	    		.setAutoCancel(true)
//	    		.setSmallIcon(R.drawable.project_logo)
//	    		.build();
	    
	    
	        Bitmap remote_picture = null;

	        // Create the style object with BigPictureStyle subclass.
	        NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
	        notiStyle.setBigContentTitle("吃飯時間到");
	        notiStyle.setSummaryText("該吃飯囉!");

	        remote_picture = BitmapFactory.decodeResource(context.getResources(),R.drawable.tttts);
	        
	        // Add the big picture to the style.
	        notiStyle.bigPicture(remote_picture);

	        // Creates an explicit intent for an ResultActivity to receive.
	        Intent resultIntent = new Intent(context, Activity_about.class);

	        // This ensures that the back button follows the recommended convention for the back key.
	        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

	        // Adds the back stack for the Intent (but not the Intent itself).
	        stackBuilder.addParentStack(Activity_about.class);

	        // Adds the Intent that starts the Activity to the top of the stack.
	        stackBuilder.addNextIntent(resultIntent);
	       // PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

	        
	        myNotification = new NotificationCompat.Builder(context)
	                .setSmallIcon(R.drawable.project_logo55_55)
	                .setAutoCancel(true)
	                .setDefaults(Notification.DEFAULT_SOUND)
	                .setContentIntent(pendingIntent)
	                .setContentTitle("吃飯時間到")
	                .setContentText("該吃飯囉!")
	                .setTicker("Life Recorded 提醒")
	                .setWhen(System.currentTimeMillis())
	                .setStyle(notiStyle).build();
	        
	        notificationManager = 
	    		(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
	    notificationManager.notify(MY_NOTIFICATION_ID, myNotification);
	    
	}

	

}
