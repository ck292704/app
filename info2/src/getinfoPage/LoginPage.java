package getinfoPage;

import index_page.Activity_index_page;
import index_page.Activity_return;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.BreakIterator;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.hellochart.R;
import com.example.informationPage.MainActivity;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Session.StatusCallback;
import com.facebook.model.GraphUser;
import com.loopj.android.http.JsonHttpResponseHandler;

import API_Funtion.ClassAPI;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class LoginPage extends Activity
{
	private ClassAPI API_Funtion = new ClassAPI(LoginPage.this);
	String mName, mUid, mGender;
	private Button mButtonLogin;
	private ImageButton mButtonLogin_1;

	private ImageView mButton_Dialog_cancel;
	private Button mButton_yes;
	private Button mButton_no;
	
	private String mTokenFileName = "Token"; // 讀取的黨名
	private int readed; // 已讀取的位元數
	String mToken = null; // 內容
	byte[] buff = new byte[256]; // input stream buffer
	
	private int tmp = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_loginpage);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#df3a5f8a"));
		getActionBar().setBackgroundDrawable(actionbar_color);
		mButtonLogin_1 = (ImageButton) findViewById(R.id.imageButton_login);
		// mButtonLogin = (Button) findViewById(R.id.bth_login);
		// mButtonLogin.setOnClickListener(login);
		mButtonLogin_1.setOnClickListener(login_1);
		// Facebooklogin();

		//----確認網路是否連接----//
		if(isNetworkConnected()==true)
		{
			//Toast.makeText(LoginPage.this, "歡迎使用", Toast.LENGTH_SHORT).show();
		}
		else
		{
			AlertDialog.Builder ad = new AlertDialog.Builder(this);
			ad.setTitle("尚未連接網路");
			ad.setMessage("請確認  WI-FI 或  行動網路  已開啟,再重啟程式!");
			ad.setPositiveButton("離開", new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int i)
				{
					finish();
				}
			});
			ad.show();
			//Toast.makeText(LoginPage.this, "請確認已連接網路連線!", Toast.LENGTH_LONG).show();
			//this.finish();
		}
		
		
		// Input stream
		try
		{
			FileInputStream reader = openFileInput(mTokenFileName);
			while ((readed = reader.read(buff)) != -1)
			{
				mToken += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		food_show();

		if (mToken != null && isNetworkConnected()==true)
		{
			Intent info_intent = new Intent();
			info_intent.setClass(LoginPage.this, Activity_index_page.class);
			startActivity(info_intent);
			System.gc();
			finish();
		}
		else
		{
			if(this.getIntent().hasExtra("tmp")==true)
			{
				Bundle bundle =this.getIntent().getExtras();
				tmp = bundle.getInt("tmp");
			}
			
			if(tmp ==1)
			{
				Facebooklogin();
			}
		}
	}

	
	@Override
	protected void onResume()
	{
		super.onResume();
	}


	public void Facebooklogin()
	{
		Session.openActiveSession(this, true, new StatusCallback()
		{
			@Override
			public void call(Session session, SessionState state, Exception exception)
			{
				if (session == Session.getActiveSession())
				{
					if (session.isOpened())
					{
						mToken = session.getAccessToken();
						// Toast.makeText(MainActivity.this,
						// session.getAccessToken(),Toast.LENGTH_LONG).show();
						Request.newMeRequest(session, new GraphUserCallback()
						{

							@Override
							public void onCompleted(GraphUser user, Response response)
							{
								// TODO Auto-generated method stub
								if (user != null)
								{
									mName = user.getName();
									mUid = user.getId();
									mGender = user.getProperty("gender").toString();
									// final Intent info_intent = new Intent();
									final Intent sign_intent = new Intent();
									final Intent info_intent = new Intent();
									sign_intent.setClass(LoginPage.this, getOtherinf.class);
									info_intent.setClass(LoginPage.this, Activity_index_page.class);
									Bundle bundle = new Bundle();
									bundle.putString("key_Name", mName);
									bundle.putString("key_Gender", mGender);
									bundle.putString("key_Uid", mUid);
									sign_intent.putExtras(bundle);
									// Toast.makeText(LoginPage.this, "login",
									// Toast.LENGTH_LONG).show();
									try
									{
										API_Funtion.login(88122841, mUid, new JsonHttpResponseHandler()
										{

											@Override
											public void onSuccess(JSONObject response)
											{
												// Toast.makeText(LoginPage.this,
												// "login11",
												// Toast.LENGTH_LONG).show();
												// Toast.makeText(LoginPage.this,
												// "login",
												// Toast.LENGTH_LONG).show();
												try
												{
													if (response.getString("msg").equals("Cant find the user"))
													{
														// Toast.makeText(LoginPage.this,
														// "error",
														// Toast.LENGTH_LONG).show();
														startActivity(sign_intent);
														LoginPage.this.finish();
													}
													else
													{
//														 Toast.makeText(LoginPage.this,
//														 "error",
//														 Toast.LENGTH_LONG).show();
														// 存token
														getDir(mTokenFileName, Context.MODE_PRIVATE);
														FileOutputStream writer = openFileOutput(mTokenFileName, Context.MODE_PRIVATE);
														writer.write(response.getString("session_token").getBytes());
														writer.close();

														// 存每日所需熱量
														String needcal = response.getString("user_cal");
														String fileName_1 = "Need";
														getDir(fileName_1, Context.MODE_PRIVATE);
														FileOutputStream writer_1 = openFileOutput(fileName_1, Context.MODE_PRIVATE);
														writer_1.write(needcal.getBytes());
														writer_1.close();
														//Log.e("*********", "wtf");
														startActivity(info_intent);
														LoginPage.this.finish();
														// Toast.makeText(LoginPage.this,
														// "error1111",
														// Toast.LENGTH_LONG).show();
													}
													super.onSuccess(response);
												}
												catch (JSONException e)
												{
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
												catch (FileNotFoundException e)
												{
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
												catch (IOException e)
												{
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
											}
										});
									}
									catch (UnsupportedEncodingException e)
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									catch (JSONException e)
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									// Toast.makeText(MainActivity.this,
									// " "+mName+" "+mUid+" "+mGender,
									// Toast.LENGTH_LONG).show();

								}
							}
						}).executeAsync();
					}
				}
			}
		});
	}

	// private OnClickListener login = new OnClickListener()
	// {
	//
	// @Override
	// public void onClick(View v)
	// {
	// // TODO Auto-generated method stub
	// Facebooklogin();
	//
	// }
	// };

	private OnClickListener login_1 = new OnClickListener()
	{

		@Override
		public void onClick(View v)
		{
			
			final Dialog dialog = new Dialog(LoginPage.this, R.style.dialog);
			View myView = LayoutInflater.from(LoginPage.this).inflate(R.layout.dialog_delayenjoy, null);
			dialog.setContentView(myView);
			mButton_yes = (Button) myView.findViewById(R.id.btn_yes);
			mButton_no = (Button) myView.findViewById(R.id.btn_no);
			
			mButton_yes.setOnClickListener(new OnClickListener()
			{
				
				@Override
				public void onClick(View v)
				{
					Intent intent = new Intent();
					intent.setClass(LoginPage.this, Activity_about.class);
					startActivity(intent);
					finish();
				}
			});
			
			mButton_no.setOnClickListener(new OnClickListener()
			{
				
				@Override
				public void onClick(View v)
				{
					Facebooklogin();
					dialog.cancel();
				}
			});
			dialog.show();
		
			
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}

	private boolean isNetworkConnected()
	{
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null)
		{
			return false;
		}
		else
		{
			return true;
		}

	}
	
	private void food_show()
	{
		ClassAPI API_Funtion_1 = new ClassAPI(LoginPage.this);
		try
		{
			// ----------------------------------------//
			API_Funtion_1.food_show(88122841, mToken, API_Funtion_1.GetTimestamp(), new JsonHttpResponseHandler()
			{
				@SuppressLint("NewApi")
				public void onSuccess(JSONObject response)
				{
					try
					{
						// Log.e("*************", "qq1");
						String meals_null_breakfast = response.get("breakfast").toString();
						String meals_null_lunch = response.get("lunch").toString();
						String meals_null_dinner = response.get("dinner").toString();
						Log.e("*************", "ok_1");
						Log.e("*************", "OK_2");
						Log.e("*************", "OK_3");
					}
					catch (JSONException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					super.onSuccess(response);
				}

				@Override
				public void onFailure(Throwable e, JSONObject errorResponse)
				{
					Log.e("api1", "error");
					super.onFailure(e, errorResponse);
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable e)
				{
					Log.e("1", ""+statusCode);
					Log.e("2", ""+headers.toString());
					Log.e("3", ""+responseBody);
					Log.e("4", ""+e.toString());
					super.onFailure(statusCode, headers, responseBody, e);
				}
				
			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
