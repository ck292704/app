package getinfoPage;
import com.example.hellochart.R;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ViewSwitcher.ViewFactory;


public class setWorkpower extends Activity {
	private RadioButton mRadioButtonStrong,mRadioButtonMed,mRadioButtonWeak;
	private RadioGroup mRadioGroupWorkpower;	
	private Button mButtonNext2;	
	private ImageView mImageViewStrong;
	private TextView mTextViewStrong;
	private TextView mTextViewStrongtitle;
	private View mViewLine1;
	private ImageView mImageViewMed;
	private TextView mTextViewMed;
	private TextView mTextViewMedtitle;
	private View mViewLine2;
	private ImageView mImageViewWeak;
	private TextView mTextViewWeak;
	private TextView mTextViewWeaktitle;
	private View mViewLine3;
	String mPowerlevel="2";
	String mName,mGender,mUid,mToken;
	int mAge;
	int[] mpic={R.drawable.test1,R.drawable.test2,R.drawable.test3};	
	int index=0;
	double mHeight,mWeight;
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_workpower);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#df3a5f8a"));
		getActionBar().setBackgroundDrawable(actionbar_color);
		iniView();	
		bundle();
		mRadioGroupWorkpower.setOnCheckedChangeListener(workpower);
		
	}

	private void iniView() {
		
		mRadioGroupWorkpower=(RadioGroup)findViewById(R.id.radioGroup1);
		mRadioButtonStrong=(RadioButton)findViewById(R.id.radio_strong);
		mRadioButtonMed=(RadioButton)findViewById(R.id.radio_medium);
		mRadioButtonWeak=(RadioButton)findViewById(R.id.radio_weak);
		mButtonNext2=(Button)findViewById(R.id.btn_next2);
		mImageViewStrong=(ImageView)findViewById(R.id.imageViews);
		mTextViewStrong=(TextView)findViewById(R.id.textViewStrongs);
		mTextViewStrongtitle=(TextView)findViewById(R.id.textViewStrongtitles);
		mViewLine1=(View)findViewById(R.id.viewlines);		
		mImageViewMed=(ImageView)findViewById(R.id.imageViewm);
		mTextViewMed=(TextView)findViewById(R.id.textViewMedm);
		mTextViewMedtitle=(TextView)findViewById(R.id.textViewMediumtitlem);
		mViewLine2=(View)findViewById(R.id.viewlinem);	
		mImageViewWeak=(ImageView)findViewById(R.id.imageVieww);
		mTextViewWeak=(TextView)findViewById(R.id.textViewLightl);
		mTextViewWeaktitle=(TextView)findViewById(R.id.textViewLighttitlel);
		mViewLine3=(View)findViewById(R.id.viewlinel);
		mImageViewStrong.setBackgroundResource(R.drawable.info);
		mImageViewMed.setBackgroundResource(R.drawable.info);
		mImageViewWeak.setBackgroundResource(R.drawable.info);
		mButtonNext2.setOnClickListener(input2);
	}

	private void bundle() {
		Bundle bundle=this.getIntent().getExtras();
		mName=bundle.getString("key_Name");
		mGender=bundle.getString("key_Gender");
		mToken=bundle.getString("key_token");
		mUid=bundle.getString("key_Uid");
		mAge=bundle.getInt("key_Age");
		mHeight=bundle.getDouble("key_Height");
		mWeight=bundle.getDouble("key_Weight");
	}
	private RadioGroup.OnCheckedChangeListener workpower=new OnCheckedChangeListener() {
		
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			
			if(checkedId==R.id.radio_weak){
				
				mImageViewWeak.setAlpha((float) 0.8);
				mTextViewWeak.setAlpha((float) 0.8);
				mTextViewWeaktitle.setAlpha((float) 0.8);
				mViewLine3.setAlpha((float) 0.8);		
				mImageViewMed.setAlpha((float) 0.1);
				mTextViewMed.setAlpha((float) 0.1);
				mTextViewMedtitle.setAlpha((float) 0.1);
				mViewLine2.setAlpha((float) 0.1);			
				mImageViewStrong.setAlpha((float) 0.1);
				mTextViewStrong.setAlpha((float) 0.1);
				mTextViewStrongtitle.setAlpha((float) 0.1);
				mViewLine1.setAlpha((float) 0.1);
				mPowerlevel="0";}
			else if(checkedId==R.id.radio_medium){
				mImageViewStrong.setAlpha((float) 0.1);
				mTextViewStrong.setAlpha((float) 0.1);
				mTextViewStrongtitle.setAlpha((float) 0.1);
				mViewLine1.setAlpha((float) 0.1);
				
				mImageViewMed.setAlpha((float) 0.8);
				mTextViewMed.setAlpha((float) 0.8);
				mTextViewMedtitle.setAlpha((float) 0.8);
				mViewLine2.setAlpha((float) 0.8);
				
				mImageViewWeak.setAlpha((float) 0.1);
				mTextViewWeak.setAlpha((float) 0.1);
				mTextViewWeaktitle.setAlpha((float) 0.1);
				mViewLine3.setAlpha((float) 0.1);
				mPowerlevel="1";}
			else{
				mImageViewStrong.setAlpha((float) 0.8);
				mTextViewStrong.setAlpha((float) 0.8);
				mTextViewStrongtitle.setAlpha((float) 0.8);
				mViewLine1.setAlpha((float) 0.8);
				
				mImageViewMed.setAlpha((float) 0.1);
				mTextViewMed.setAlpha((float) 0.1);
				mTextViewMedtitle.setAlpha((float) 0.1);
				mViewLine2.setAlpha((float) 0.1);
				
				mImageViewWeak.setAlpha((float) 0.1);
				mTextViewWeak.setAlpha((float) 0.1);
				mTextViewWeaktitle.setAlpha((float) 0.1);
				mViewLine3.setAlpha((float) 0.1);
				mPowerlevel="2";}	
		}
		
	};
	
	private OnClickListener input2=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			//Toast.makeText(setWorkpower.this," "+mToken, Toast.LENGTH_SHORT).show();
			//level upload
			Intent intent=new Intent();
			intent.setClass(setWorkpower.this,getTime.class);
			Bundle bundle=new Bundle();
			bundle.putString("key_Level", mPowerlevel);
			bundle.putString("key_Name", mName);
			bundle.putString("key_Gender",mGender);
			bundle.putString("key_Uid",mUid);
			bundle.putString("key_token",mToken);
			bundle.putInt("key_Age",mAge);
			bundle.putDouble("key_Height", mHeight);
			bundle.putDouble("key_Weight", mWeight);
			intent.putExtras(bundle); 
			startActivity(intent);
			setWorkpower.this.finish();
			
		}
	};
}
