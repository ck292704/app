package news_page;

import index_page.Activity_index_page;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.hellochart.R;
import com.example.informationPage.Activity_MealsPage;
import com.loopj.android.http.JsonHttpResponseHandler;

import API_Funtion.ClassAPI;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Activity_news_page extends Activity
{
	private WebView mWebViewNews;
	private ClassAPI API_Funtion = new ClassAPI(Activity_news_page.this);
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news_page);
		final ProgressDialog ringProgressDialog = ProgressDialog.show(Activity_news_page.this, "Please wait ...", "Loading ...", true);
		ringProgressDialog.setCancelable(true);
		try
		{
			API_Funtion.mission_finish(88122841, gettoken(), API_Funtion.GetTimestamp(), 4, new JsonHttpResponseHandler()
			{

				@Override
				public void onSuccess(JSONObject response)
				{
					Log.e("log", "mission5_okk!");
					super.onSuccess(response);
				}
				
			});
		}
		catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ActionBar bar = getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		ColorDrawable actionbar_color = new ColorDrawable(Color.parseColor("#3a5f8a"));
		bar.setBackgroundDrawable(actionbar_color);
		
		String mUrl= "http://www.healthrecord.lomrt.com/cjcu/WP_NEWS/index.php";  
	 	mWebViewNews=(WebView)findViewById(R.id.webView1);
	 	WebSettings websetting=mWebViewNews.getSettings();
	 	websetting.setSupportZoom(true);
	 	websetting.setBuiltInZoomControls(false);  
	 	websetting.setJavaScriptEnabled(true); 
	 	mWebViewNews.setWebViewClient(new WebViewClient());
	 	mWebViewNews.loadUrl(mUrl);
	 	mWebViewNews.setWebViewClient(new WebViewClient()
	 	{

			@Override
			public void onPageFinished(WebView view, String url)
			{
				super.onPageFinished(view, url);
				ringProgressDialog.dismiss();
			}
	 		
	 	});
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			Intent intent = new Intent();
			intent.setClass(Activity_news_page.this, Activity_index_page.class);
			startActivity(intent);
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{    
	   switch (item.getItemId()) 
	   {        
	      case android.R.id.home:            
	         Intent intent = new Intent(this, Activity_index_page.class);            
	         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
	         startActivity(intent);
	         Activity_news_page.this.finish();
	         return true;        
	      default:            
	         return super.onOptionsItemSelected(item);    
	   }
	}
	
	private String gettoken() // 取得token
	{
		String fileName = "Token";
		int readed; // 已讀取的位元數
		String content = ""; // 內容
		byte[] buff = new byte[256]; // input stream buffer
		// Input stream
		try
		{
			FileInputStream reader = Activity_news_page.this.openFileInput(fileName);
			while ((readed = reader.read(buff)) != -1)
			{
				content += new String(buff).trim();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}

}
